# indicateurs-data

Mise à disposition des indicateurs (GeoJSON et markdown) à fréquence quotidienne

## Alimentation

Ce dépôt est alimenté par un pipeline déclaré dans le projet [indicateurs](https://git.opendatafrance.net/observatoire/indicateurs).