# Odservatoire - Organisations

*599 organisations recensées dont:*
- *51 autres groupements de collectivités territoriales*
- *81 communautés d'agglomération*
- *26 communautés de communes*
- *269 communes*
- *4 communautés urbaines*
- *49 départements*
- *28 délégataires de service public*
- *18 métropoles*
- *61 organismes associés de collectivité territoriale*
- *12 régions*

## ACIGNE

*Commune*

### Source **OpenDataSoft**
- URL : https://data.rennesmetropole.fr/explore/?sort=modified&refine.publisher=Commune+d%27Acigné
- Volumétrie : 3 jeux de données et 455 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 551
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 22/05/2019 à 15h58
- Date de dernière mise à jour le 11/09/2019 à 07h27

## AGEN

*Commune*

### Source **OpenDataSoft**
- URL : https://data.agen.fr/explore/?refine.publisher=Mairie+d%27Agen
- Volumétrie : 5 jeux de données et 697 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1473
- Date de création : le 23/04/2015 à 02h23
- Date de dernière mise à jour le 29/01/2018 à 14h16


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bec29278b4c412a07c7ee5f/
- Volumétrie : 70 jeux de données et 219 ressources
- Nombre de vues (tous jeux de données confondus) : 140
- Nombre de téléchargements (toutes ressources confondues) : 8
- Types de ressources disponibles : `csv`, `json`, `shp`, `obj`
- Types de licences utilisées : `License Not Specified`, `Other (Public Domain)`, `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 14/11/2018 à 16h55
- Date de dernière mise à jour le 01/08/2018 à 12h53

## AGENCE D'URBANISME DE L'AGGLOMERATION ORLEANAISE

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://data.orleans-metropole.fr/explore/?sort=modified&refine.publisher=Agence+d%E2%80%99Urbanisme+de+l%E2%80%99Agglom%C3%A9ration+Orl%C3%A9anaise+(AUAO)
- Volumétrie : 17 jeux de données et 22099 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 7160
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 13/09/2018 à 09h13
- Date de dernière mise à jour le 07/02/2019 à 10h53

## AGENCE DE DEV. TOURISTIQUE DE L'ARDECHE

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://opendata.ardeche.fr/group/agence-de-développement-touristique-de-lardèche
- Volumétrie : 3 jeux de données

## AGENCE DE TOURISME DE LA CORSE

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://www.data.corsica/explore/?refine.publisher=Agence+de+Tourisme+de+la+Corse
- Volumétrie : 14 jeux de données et 3264 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 11546
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 12/06/2017 à 12h51
- Date de dernière mise à jour le 06/09/2019 à 03h57

## AGENCE DES ESPACES VERTS ILE DE FRANCE

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://data.iledefrance.fr/explore/?sort=modified&refine.publisher=Agence+des+espaces+verts+d%27%C3%8Ele+de+France+(AEV)
- Volumétrie : 23 jeux de données et 17250 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 29564
- Types de licences utilisées : `ODbL`, `Open Database License (ODbL)`, `Licence Ouverte v2.0 (Etalab)`, `Licence ouverte`
- Date de création : le 10/06/2013 à 07h54
- Date de dernière mise à jour le 20/05/2019 à 09h04

## AGENCE PUBLIQUE DE GESTION LOCALE

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c94a888634f415e8c85de26/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 19/07/2019 à 10h24
- Date de dernière mise à jour le 19/07/2019 à 10h25

## AIGLUN

*Commune*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/commune-daiglun-04
- Volumétrie : 4 jeux de données

## AIN

*Département*

### Source **Plateforme territoriale**
- URL : http://departement-ain.opendata.arcgis.com/datasets?source=D%C3%A9partement%20de%20l%27Ain
- Volumétrie : 2 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5851009688ee3870fbc65bb3
- Volumétrie : 12 jeux de données et 59 ressources
- Nombre de vues (tous jeux de données confondus) : 28
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `json`, `shp`, `zip`, `document`, `csv`, `image`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 28/02/2017 à 11h30
- Date de dernière mise à jour le 25/05/2019 à 06h48

## AIR BREIZH

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : http://data.airbreizh.asso.fr/geonetwork/srv/fre/catalog.search#/search?facet.q=type%2Fdataset
- Volumétrie : 13 jeux de données

## AIR PAYS DE LA LOIRE

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data.airpl.org/geonetwork/srv/fre/catalog.search#/search?facet.q=type%2Fdataset
- Volumétrie : 28 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff53a3a7292c64a77cd7/
- Volumétrie : 10 jeux de données et 29 ressources
- Nombre de vues (tous jeux de données confondus) : 28
- Nombre de téléchargements (toutes ressources confondues) : 85
- Types de ressources disponibles : `csv`, `xls`, `json`, `xml`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 07/05/2014 à 04h13
- Date de dernière mise à jour le 10/12/2013 à 14h49

## AIRE SUR L'ADOUR

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b9a1371634f417f35dc4a7c/
- Volumétrie : 1 jeux de données et 48 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`, `json`, `shp`, `dbf`, `document`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 08/02/2019 à 06h40
- Date de dernière mise à jour le 08/02/2019 à 06h40

## AIRPARIF

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data-airparif-asso.opendata.arcgis.com/datasets
- Volumétrie : 92 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a4381adc751df74bae4627d/
- Volumétrie : 14 jeux de données et 34 ressources
- Nombre de vues (tous jeux de données confondus) : 75
- Nombre de téléchargements (toutes ressources confondues) : 41
- Types de ressources disponibles : `xml`, `geojson`, `pdf`, `postman_collection.json`, `wmts`, `web page`, `csv`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence`
- Date de création : le 29/12/2017 à 15h33
- Date de dernière mise à jour le 28/03/2019 à 14h28

## AIX LES BAINS

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ab9e75188ee387964e77c2b/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 27/03/2018 à 08h58
- Date de dernière mise à jour le 27/03/2018 à 08h58

## AIX-EN-PROVENCE

*Commune*

### Source **Plateforme territoriale**
- URL : http://www.aixenprovence.fr/Open-Data
- Volumétrie : 5 jeux de données

## ALLAIRE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a1d2901c751df5e50300f1c
- Volumétrie : 1 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 4
- Types de ressources disponibles : `csv`, `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 28/11/2017 à 12h19
- Date de dernière mise à jour le 23/08/2018 à 16h49

## ALPES DE HAUTE PROVENCE

*Département*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/conseil-departemental-des-alpes-de-haute-provence
- Volumétrie : 21 jeux de données

## ALPI 40

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59b2610dc751df4ea9075aea/
- Volumétrie : 6 jeux de données et 11 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `xml`, `csv`, `geojson`, `shp`, `json`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 21/09/2017 à 11h32
- Date de dernière mise à jour le 04/10/2019 à 00h00

## ANDUZE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c49b5498b4c4139d1714d76/
- Volumétrie : 3 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xlsx`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `License Not Specified`
- Date de création : le 24/01/2019 à 15h21
- Date de dernière mise à jour le 29/01/2019 à 12h02

## ANGERS

*Commune*

### Source **OpenDataSoft**
- URL : https://data.angers.fr/explore/?refine.publisher=Ville+d%27Angers
- Volumétrie : 42 jeux de données et 39551 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 31498
- Types de licences utilisées : `Open Database License (ODbL)`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 28/07/2016 à 08h55
- Date de dernière mise à jour le 06/10/2019 à 10h12

## ANGERS LOIRE METROPOLE

*Communauté urbaine*

### Source **OpenDataSoft**
- URL : https://data.angers.fr/explore/?refine.publisher=Angers+Loire+M%C3%A9tropole
- Volumétrie : 34 jeux de données et 167770 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 238896
- Types de licences utilisées : `Open Database License (ODbL)`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 21/07/2016 à 13h03
- Date de dernière mise à jour le 06/10/2019 à 23h11


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/538346d6a3a72906c7ec5c36
- Volumétrie : 119 jeux de données et 688 ressources
- Nombre de vues (tous jeux de données confondus) : 2127
- Nombre de téléchargements (toutes ressources confondues) : 65
- Types de ressources disponibles : `csv`, `json`, `shp`, `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`
- Date de création : le 14/03/2018 à 08h27
- Date de dernière mise à jour le 11/09/2019 à 08h51

## ANGLET

*Commune*

### Source **OpenDataSoft**
- URL : https://anglet-opendatapaysbasque.opendatasoft.com/explore/?sort=modified&refine.publisher=Ville+d%27Anglet
- Volumétrie : 15 jeux de données et 15974 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 3302
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 23/11/2018 à 14h19
- Date de dernière mise à jour le 25/06/2019 à 14h28


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b17dbeb88ee3869a59ebdca/
- Volumétrie : 17 jeux de données et 64 ressources
- Nombre de vues (tous jeux de données confondus) : 12
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`, `json`, `shp`, `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 09/04/2019 à 10h52
- Date de dernière mise à jour le 25/06/2019 à 14h28

## ANTIBES JUAN LES PINS

*Commune*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/ville-dantibes-juan-les-pins
- Volumétrie : 71 jeux de données

## APUR

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : http://opendata.apur.org/datasets
- Volumétrie : 94 jeux de données

## ARCADE

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/arcade
- Volumétrie : 4 jeux de données

## ARCADI ILE DE FRANCE

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://data.iledefrance.fr/explore/?refine.publisher=Arcadi
- Volumétrie : 10 jeux de données et 2377 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 38273
- Types de licences utilisées : `ODbL`, `Licence ouverte`
- Date de création : le 27/01/2014 à 09h34
- Date de dernière mise à jour le 16/03/2019 à 17h26

## ARDECHE

*Département*

### Source **Plateforme territoriale**
- URL : https://opendata.ardeche.fr/search/type/dataset
- Volumétrie : 11 jeux de données

## ARDTA

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : http://opendata.auvergnerhonealpes.eu/7-les-donnees.htm?facet.provider%5B%5D=Agence+r%C3%A9gionale+de+d%C3%A9veloppement+des+territoires+d%27Auvergne+%28ARDTA%29
- Volumétrie : 14 jeux de données

## ARIEGE

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a90060ec751df1402f350d8/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 23/02/2018 à 14h09
- Date de dernière mise à jour le 23/02/2018 à 14h15

## ARL PACA

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/agence-regionale-du-livre-provence-alpes-cote-dazur
- Volumétrie : 4 jeux de données

## ARMENTIERES

*Commune*

### Source **OpenDataSoft**
- URL : https://opendata.lillemetropole.fr/explore/?refine.publisher=Ville+d%27ARMENTIERES
- Volumétrie : 2 jeux de données et 2420 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1729
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 19/10/2017 à 06h12
- Date de dernière mise à jour le 03/04/2019 à 13h26

## ASNIERES SUR SEINE

*Commune*

### Source **OpenDataSoft**
- URL : https://opendata.hauts-de-seine.fr/explore/?refine.publisher=Ville+d%27Asnières-sur-Seine
- Volumétrie : 2 jeux de données et 107 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1355
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 20/08/2018 à 09h25
- Date de dernière mise à jour le 24/09/2019 à 14h13


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a61b7a7c751df3fd36ac81a
- Volumétrie : 2 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 28/03/2018 à 09h05
- Date de dernière mise à jour le 28/03/2018 à 13h31

## ATD 36

*Organisme associé de collectivité territoriale*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5aa95464c751df7c5ded72f6/
- Volumétrie : 2 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `pdf`, `03.15-ago-ndeg-4-budget-2019.pdf`, `03.16 ago_n°4 budget 2018.pdf`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 01/06/2018 à 17h57
- Date de dernière mise à jour le 09/04/2019 à 16h41

## ATMO AUVERGNE RHONE ALPES

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data-atmoaura.opendata.arcgis.com/datasets
- Volumétrie : 200 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59a538b688ee386b121578b9/
- Volumétrie : 6 jeux de données et 30 ressources
- Nombre de vues (tous jeux de données confondus) : 6
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`, `json`, `shp`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 30/09/2019 à 06h40
- Date de dernière mise à jour le 05/10/2019 à 06h41

## ATMO BOURGOGNE FRANCHE COMTE

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://www.atmo-bfc.org/opendata
- Volumétrie : 20 jeux de données

## ATMO GRAND EST

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data-atmograndest.opendata.arcgis.com/datasets
- Volumétrie : 122 jeux de données

## ATMO GUYANE

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data-atmo-guyane.opendata.arcgis.com/datasets
- Volumétrie : 20 jeux de données

## ATMO HAUTS DE FRANCE

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data-atmo-hdf.opendata.arcgis.com/datasets
- Volumétrie : 46 jeux de données

## ATMO NORMANDIE

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://datas-atmonormandie.opendata.arcgis.com/datasets
- Volumétrie : 39 jeux de données

## ATMO NOUVELLE AQUITAINE

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data-atmo-na.opendata.arcgis.com/datasets
- Volumétrie : 349 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff53a3a7292c64a77cd9/
- Volumétrie : 7 jeux de données et 13 ressources
- Nombre de vues (tous jeux de données confondus) : 11
- Nombre de téléchargements (toutes ressources confondues) : 19
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 07/05/2014 à 04h57
- Date de dernière mise à jour le 18/09/2013 à 08h47

## ATMO OCCITANIE

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data-atmo-occitanie.opendata.arcgis.com/datasets
- Volumétrie : 39 jeux de données

## ATMO REUNION

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data-atmoreunion.opendata.arcgis.com/datasets
- Volumétrie : 9 jeux de données

## ATMO SUD

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/atmosud
- Volumétrie : 17 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b9fb3678b4c416ff2277bb5/
- Volumétrie : 17 jeux de données et 245 ressources
- Nombre de vues (tous jeux de données confondus) : 8
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `csv`, `geojson`, `wfs`, `pdf`, `shp`, `zip`, `geotiff`, `wms`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 21/06/2019 à 10h01
- Date de dernière mise à jour le 05/10/2019 à 00h00

## ATOUMOD

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5cc844dd634f41078d8e2c8e/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 8
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 23/07/2019 à 05h36
- Date de dernière mise à jour le 11/09/2019 à 14h13

## AUBE

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a216283c751df6333108428/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 4
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 13/12/2017 à 16h12
- Date de dernière mise à jour le 13/12/2017 à 16h12

## AUBE EN CHAMPAGNE TOURISME

*Organisme associé de collectivité territoriale*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/54880126c751df081fa3fc15/
- Volumétrie : 1 jeux de données et 11 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 33
- Types de ressources disponibles : `xls`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 10/12/2014 à 10h05
- Date de dernière mise à jour le 10/12/2014 à 16h58

## AUCAMVILLE

*Commune*

### Source **OpenDataSoft**
- URL : https://data.toulouse-metropole.fr/explore/?refine.publisher=Mairie+d%27Aucamville
- Volumétrie : 4 jeux de données et 19 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 2743
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 23/03/2015 à 13h39
- Date de dernière mise à jour le 31/12/2017 à 17h48

## AUTOLIB VELIB METROPOLE

*Autre groupement de collectivités territoriales*

### Source **OpenDataSoft**
- URL : https://opendata.paris.fr/explore/?sort=modified&refine.publisher=Autolib+Velib+M%C3%A9tropole+
- Volumétrie : 2 jeux de données
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 06/10/2019 à 21h59
- Date de dernière mise à jour le 07/10/2019 à 04h31


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/55b10bbcc751df773d10ccc3/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 74
- Nombre de téléchargements (toutes ressources confondues) : 51
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 08/02/2016 à 16h27
- Date de dernière mise à jour le 08/02/2016 à 16h27

## AUTUN

*Commune*

### Source **Plateforme territoriale**
- URL : https://www.databfc.fr/bfc/portail
- Volumétrie : 4 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bb47c2a634f416334276b4f/
- Volumétrie : 4 jeux de données et 12 ressources
- Nombre de vues (tous jeux de données confondus) : 183
- Nombre de téléchargements (toutes ressources confondues) : 22
- Types de ressources disponibles : `csv`, `xls`, `shx`, `shp`, `prj`, `kml`, `dbf`, `geojson`, `cpg`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 03/10/2018 à 10h28
- Date de dernière mise à jour le 05/10/2018 à 16h08

## AUVERGNE RHONE ALPES

*Région*

### Source **Plateforme territoriale**
- URL : http://opendata.auvergnerhonealpes.eu/7-les-donnees.htm?facet.provider[]=R%C3%A9gion%20Auvergne%20Rh%C3%B4ne-Alpes&from=0
- Volumétrie : 71 jeux de données

## AUVERGNE RHONE ALPES TOURISME

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : http://opendata.auvergnerhonealpes.eu/7-les-donnees.htm?facet.provider%5B%5D=Comit%C3%A9+R%C3%A9gional+de+D%C3%A9veloppement+touristique+d%E2%80%99Auvergne+%28CRDTA%29
- Volumétrie : 12 jeux de données

## AVIGNON

*Commune*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/avignon
- Volumétrie : 38 jeux de données

## AYHERRE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5cfa61b1634f411763ec4dff/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `json`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 07/06/2019 à 15h22
- Date de dernière mise à jour le 07/06/2019 à 15h46

## BAILLARGUES

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/baillargues
- Volumétrie : 27 jeux de données

## BAINS SUR OUST

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a1d64a3c751df51a9440eae
- Volumétrie : 9 jeux de données et 15 ressources
- Nombre de vues (tous jeux de données confondus) : 12
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `xml`, `pdf`, `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 28/11/2017 à 14h44
- Date de dernière mise à jour le 29/05/2019 à 16h57

## BALMA

*Commune*

### Source **OpenDataSoft**
- URL : https://data.toulouse-metropole.fr/explore/?refine.publisher=Mairie+de+Balma
- Volumétrie : 4 jeux de données et 140 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 4900
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 14/02/2013 à 23h00
- Date de dernière mise à jour le 31/12/2017 à 22h45

## BANDOL

*Commune*

### Source **Plateforme territoriale**
- URL : https://opendata.sictiam.fr/fr/organization/mairie-de-bandol
- Volumétrie : 1 jeux de données

## BAR LE DUC

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/56b89c46c751df22549f9dea
- Volumétrie : 30 jeux de données et 71 ressources
- Nombre de vues (tous jeux de données confondus) : 35
- Nombre de téléchargements (toutes ressources confondues) : 45
- Types de ressources disponibles : `csv`, `pdf`, `rss`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 02/02/2016 à 16h20
- Date de dernière mise à jour le 18/07/2019 à 16h24

## BARAQUEVILLE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59bf94cec751df629f10513d/
- Volumétrie : 1 jeux de données et 23 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`, `json`, `shp`, `dbf`, `document`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 08/02/2019 à 06h44
- Date de dernière mise à jour le 08/02/2019 à 06h44

## BAS RHIN

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5abcf3c688ee386e18f7f79c/
- Volumétrie : 78 jeux de données et 347 ressources
- Nombre de vues (tous jeux de données confondus) : 111
- Nombre de téléchargements (toutes ressources confondues) : 9
- Types de ressources disponibles : `document`, `json`, `shp`, `zip`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`
- Date de création : le 10/12/2018 à 18h32
- Date de dernière mise à jour le 09/08/2019 à 00h38

## BAYONNE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/58d53c4688ee380aeea5e932
- Volumétrie : 50 jeux de données et 157 ressources
- Nombre de vues (tous jeux de données confondus) : 101
- Nombre de téléchargements (toutes ressources confondues) : 7
- Types de ressources disponibles : `zip`, `json`, `shp`, `jpeg2000`, `csv/utf8`, `xlsx`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 01/10/2018 à 10h18
- Date de dernière mise à jour le 05/10/2019 à 06h41

## BEAULIEU

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/beaulieu
- Volumétrie : 19 jeux de données

## BEAUVAIS

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/587cebc488ee3805cb9b81a4
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 13/03/2017 à 14h09
- Date de dernière mise à jour le 20/03/2017 à 13h52

## BEGANNE

*Commune*

### Source **OpenDataSoft**
- URL : https://www.opendata56.fr/explore/?refine.publisher=Ville+de+B%C3%A9ganne
- Volumétrie : 2 jeux de données et 218 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 10
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 18/10/2018 à 18h16
- Date de dernière mise à jour le 17/11/2018 à 08h30

## BEGARD

*Commune*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 3 jeux de données

## BESANCON

*Commune*

### Source **Plateforme territoriale**
- URL : https://www.databfc.fr/bfc/portail
- Volumétrie : 2 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/539a5b76a3a7293bc272836c/
- Volumétrie : 2 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 11
- Nombre de téléchargements (toutes ressources confondues) : 11
- Types de ressources disponibles : `csv`, `word`
- Types de licences utilisées : `License Not Specified`, `Other (Public Domain)`
- Date de création : le 07/07/2014 à 09h30
- Date de dernière mise à jour le 22/03/2019 à 16h21

## BESNE

*Commune*

### Source **OpenDataSoft**
- URL : https://data.agglo-carene.fr/explore/?refine.publisher=Besné
- Volumétrie : 1 jeux de données et 81 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 118
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 30/07/2019 à 13h30
- Date de dernière mise à jour le 30/07/2019 à 13h30

## BLAGNAC

*Commune*

### Source **OpenDataSoft**
- URL : https://data.toulouse-metropole.fr/explore/?sort=modified&refine.publisher=Ville+de+Blagnac
- Volumétrie : 76 jeux de données et 12349 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 7900
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 11/04/2019 à 10h32
- Date de dernière mise à jour le 26/08/2019 à 21h36

## BOE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/596ca1ecc751df20cc2b38df
- Volumétrie : 25 jeux de données et 75 ressources
- Nombre de vues (tous jeux de données confondus) : 30
- Nombre de téléchargements (toutes ressources confondues) : 36
- Types de ressources disponibles : `json`, `xls`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 17/07/2017 à 17h33
- Date de dernière mise à jour le 02/09/2019 à 09h13

## BOISSY SAINT LEGER

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.ville-boissy.fr/portail
- Volumétrie : 4 jeux de données

## BORDEAUX

*Commune*

### Source **OpenDataSoft**
- URL : https://opendata.bordeaux-metropole.fr/explore/?disjunctive.publisher&disjunctive.frequence&disjunctive.territoire&sort=title&refine.publisher=Ville+de+Bordeaux
- Volumétrie : 76 jeux de données et 361188 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 2205
- Types de licences utilisées : `Licence Ouverte`
- Date de création : le 01/01/2009 à 11h00
- Date de dernière mise à jour le 07/10/2019 à 04h08

## BORDEAUX METROPOLE

*Métropole*

### Source **OpenDataSoft**
- URL : https://opendata.bordeaux-metropole.fr/explore/?disjunctive.publisher&disjunctive.frequence&disjunctive.territoire&sort=title&refine.publisher=Bordeaux+Métropole
- Volumétrie : 167 jeux de données et 2934506 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 6197
- Types de licences utilisées : `Licence Ouverte`
- Date de création : le 03/04/2019 à 15h45
- Date de dernière mise à jour le 07/10/2019 à 04h32


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b0d11f688ee382af4b18c95/
- Volumétrie : 86 jeux de données et 779 ressources
- Nombre de vues (tous jeux de données confondus) : 255
- Nombre de téléchargements (toutes ressources confondues) : 5
- Types de ressources disponibles : `gtfs.zip`, `json`, `shp`, `csv`, `zip`, `dwg`, `image`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 20/09/2016 à 11h02
- Date de dernière mise à jour le 04/10/2019 à 06h40

## BOULOGNE BILLANCOURT

*Commune*

### Source **OpenDataSoft**
- URL : https://boulognebillancourt-seineouest.opendatasoft.com/explore/?sort=title&refine.publisher=Ville+de+Boulogne-Billancourt
- Volumétrie : 11 jeux de données et 1413 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 2355
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 18/04/2019 à 09h09
- Date de dernière mise à jour le 23/08/2019 à 12h21


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/54d8f2a4c751df1df7467389
- Volumétrie : 19 jeux de données et 42 ressources
- Nombre de vues (tous jeux de données confondus) : 91
- Nombre de téléchargements (toutes ressources confondues) : 25
- Types de ressources disponibles : `csv`, `json`, `shp`, `kmz`, `kml`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 17/02/2015 à 17h17
- Date de dernière mise à jour le 23/08/2019 à 12h21

## BOURGOGNE FRANCHE COMTE

*Région*

### Source **Plateforme territoriale**
- URL : https://www.databfc.fr/bfc/portail
- Volumétrie : 20 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5835cdb988ee383e0bc65bb3
- Volumétrie : 21 jeux de données et 37 ressources
- Nombre de vues (tous jeux de données confondus) : 43
- Nombre de téléchargements (toutes ressources confondues) : 19
- Types de ressources disponibles : `json`, `shp`, `csv`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 17/11/2017 à 16h29
- Date de dernière mise à jour le 05/10/2019 à 06h54

## BREST METROPOLE

*Métropole*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff5ba3a7292c64a77d19
- Volumétrie : 7 jeux de données et 5 ressources
- Nombre de vues (tous jeux de données confondus) : 78
- Nombre de téléchargements (toutes ressources confondues) : 50
- Types de ressources disponibles : `wms`, `gtfs`, `jpeg`, `txt`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `License Not Specified`, `Other (Open)`
- Date de création : le 07/05/2014 à 04h27
- Date de dernière mise à jour le 17/09/2019 à 11h29

## BRETAGNE

*Région*

### Source **OpenDataSoft**
- URL : https://data.bretagne.bzh/explore/?sort=modified&refine.publisher=R%C3%A9gion+Bretagne
- Volumétrie : 108 jeux de données et 625224 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 6377
- Types de licences utilisées : `Licence ouverte : https://www.etalab.gouv.fr/licence-ouverte-open-licence`, `Licence ouverte : http://wiki.data.gouv.fr/wiki/Licence_Ouverte_/_Open_Li
cence`, `Licence Ouverte v2.0 (Etalab)`, `Licence ODBL: https://opendatacommons.org/licenses/odbl/summary/`, `Licence Créative Common : http://creativecommons.org/licenses/by/4.0/`, `Open Database License (ODbL)`
- Date de création : le 05/12/2014 à 00h00
- Date de dernière mise à jour le 06/10/2019 à 21h29


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/559a4970c751df46a9a453ba
- Volumétrie : 145 jeux de données et 486 ressources
- Nombre de vues (tous jeux de données confondus) : 611
- Nombre de téléchargements (toutes ressources confondues) : 25
- Types de ressources disponibles : `csv`, `json`, `shp`, `doc`, `xlsx`, `zip`, `image`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 08/02/2019 à 06h39
- Date de dernière mise à jour le 04/10/2019 à 06h53

## BRETAGNE ENVIRONNEMENT

*Organisme associé de collectivité territoriale*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/56c44eb7c751df689e6c6de0/
- Volumétrie : 74 jeux de données et 192 ressources
- Nombre de vues (tous jeux de données confondus) : 113
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `json`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 24/10/2018 à 12h19
- Date de dernière mise à jour le 25/09/2019 à 06h46

## BRETEIL

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b236d75c751df4eaa3a9b5e/
- Volumétrie : 6 jeux de données et 6 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 28/06/2018 à 10h40
- Date de dernière mise à jour le 02/09/2019 à 10h04

## BRIE COMTE ROBERT

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b71455e8b4c41290a9c0450/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `xls`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 17/08/2018 à 15h15
- Date de dernière mise à jour le 17/08/2018 à 15h15

## BROCAS LES FORGES

*Commune*

### Source **OpenDataSoft**
- URL : https://numerique.brocaslesforges.fr/explore/?sort=title&refine.publisher=Commune+de+Brocas
- Volumétrie : 15 jeux de données et 713 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1433
- Types de licences utilisées : `Open Database License (ODbL)`, `Domaine public`
- Date de création : le 15/12/2018 à 22h10
- Date de dernière mise à jour le 15/09/2019 à 04h32


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff65a3a7292c64a77d76
- Volumétrie : 109 jeux de données et 327 ressources
- Nombre de vues (tous jeux de données confondus) : 61
- Nombre de téléchargements (toutes ressources confondues) : 16
- Types de ressources disponibles : `csv`, `json`, `shp`, `doc`, `pdf`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`, `Other (Public Domain)`
- Date de création : le 05/05/2019 à 00h43
- Date de dernière mise à jour le 14/09/2019 à 04h57

## CA ANNEMASSE LES VOIRONS AGGLOMERATION

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ca4bacb634f4118ab6dea0f/
- Volumétrie : 3 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 6
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 24/04/2019 à 08h46
- Date de dernière mise à jour le 24/04/2019 à 08h47

## CA ARLES CRAU CAMARGUE MONTAGNETTE

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/communaute-dagglomeration-arles-crau-camargue-montagnette
- Volumétrie : 6 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff55a3a7292c64a77cde/
- Volumétrie : 6 jeux de données et 6 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 10/05/2019 à 19h34
- Date de dernière mise à jour le 09/05/2019 à 00h00

## CA BAR LE DUC SUD MEUSE

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/56b8a4ffc751df02a29f9deb
- Volumétrie : 51 jeux de données et 252 ressources
- Nombre de vues (tous jeux de données confondus) : 30
- Nombre de téléchargements (toutes ressources confondues) : 48
- Types de ressources disponibles : `csv`, `pdf`, `rss`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 02/02/2016 à 16h19
- Date de dernière mise à jour le 24/04/2019 à 15h21

## CA BEAUNE COTE ET SUD

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bec28758b4c412a07c7ee5e/
- Volumétrie : 1 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 21/11/2018 à 10h26
- Date de dernière mise à jour le 25/09/2019 à 16h31

## CA CANNES PAYS DE LERINS

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b3f6542c751df03439fad72/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 29/01/2019 à 15h46
- Date de dernière mise à jour le 29/01/2019 à 15h46

## CA CAUX VALLEE DE SEINE

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : http://sgi-cvs.opendata.arcgis.com/datasets?sort_by=relevance
- Volumétrie : 57 jeux de données

## CA CHATEAUROUX METROPOLE

*Communauté d'agglomération*

### Source **OpenDataSoft**
- URL : https://data.chateauroux-metropole.fr/explore/?sort=title&refine.publisher=CHATEAUROUX+METROPOLE
- Volumétrie : 51 jeux de données et 5296 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 9061
- Date de création : le 28/12/2018 à 15h27
- Date de dernière mise à jour le 12/09/2019 à 08h58


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bf67a998b4c417eae692920/
- Volumétrie : 67 jeux de données et 210 ressources
- Nombre de vues (tous jeux de données confondus) : 43
- Nombre de téléchargements (toutes ressources confondues) : 11
- Types de ressources disponibles : `csv`, `json`, `zip`, `shp`, `ksh`, `pdf`, `xlsx`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 08/01/2019 à 12h53
- Date de dernière mise à jour le 12/09/2019 à 08h58

## CA COEUR D'ESSONNE AGGLOMERATION

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a5f18afc751df7bae61b648/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 7
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 30/01/2018 à 13h55
- Date de dernière mise à jour le 30/01/2018 à 13h55

## CA DE BEZIERS MEDITERRANEE

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b6c2e4b8b4c41287663df35/
- Volumétrie : 2 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 13
- Nombre de téléchargements (toutes ressources confondues) : 5
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 14/01/2019 à 14h56
- Date de dernière mise à jour le 09/09/2019 à 08h30

## CA DE CASTRES MAZAMET

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c9398018b4c412692ef9cf7/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 21/03/2019 à 15h06
- Date de dernière mise à jour le 27/06/2019 à 16h58

## CA DE CHALONS EN CHAMPAGNE

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5badf1f68b4c412c38533e4e/
- Volumétrie : 22 jeux de données et 145 ressources
- Nombre de vues (tous jeux de données confondus) : 15
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`, `geojson`, `esri rest`, `web page`, `zip`, `kml`, `ogc wms`, `ogc wfs`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 10/11/2018 à 01h00
- Date de dernière mise à jour le 10/08/2019 à 02h35

## CA DE HAGUENAU

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c0f8dfa634f4122be181e6a/
- Volumétrie : 1 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 12/12/2018 à 16h29
- Date de dernière mise à jour le 14/08/2019 à 04h58

## CA DE L'ALBIGEOIS

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5be0257c634f415cecdbb5ff/
- Volumétrie : 3 jeux de données et 12 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`, `prj`, `dbf`, `shx`, `shp`, `sbn`, `sbx`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 30/11/2018 à 13h51
- Date de dernière mise à jour le 29/08/2019 à 11h40

## CA DE L'AUXERROIS

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bec4aea8b4c416395a5e99e/
- Volumétrie : 1 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `gtfsrt.pb`, `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 07/01/2019 à 12h03
- Date de dernière mise à jour le 18/09/2019 à 13h51

## CA DE LA PRESQU'ILE DE GUERANDE ATLANTIQUE

*Communauté d'agglomération*

### Source **OpenDataSoft**
- URL : https://capatlantique-loireatlantique.opendatasoft.com/explore/?refine.publisher=Cap+Atlantique
- Volumétrie : 14 jeux de données et 63153 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1892
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 06/08/2018 à 14h54
- Date de dernière mise à jour le 04/10/2019 à 04h00


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5cb0688e8b4c410c0750754c/
- Volumétrie : 32 jeux de données et 91 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `csv`, `json`, `shp`, `pdf`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 15/04/2019 à 16h06
- Date de dernière mise à jour le 04/10/2019 à 04h00

## CA DE LA REGION DIEPPOISE

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c1bb6bf634f416b25009c88/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 20/12/2018 à 16h50
- Date de dernière mise à jour le 20/12/2018 à 16h50

## CA DE LA REGION NAZAIRIENNE ET DE L'ESTUAIRE

*Communauté d'agglomération*

### Source **OpenDataSoft**
- URL : https://data.agglo-carene.fr/explore/?refine.publisher=CARENE
- Volumétrie : 12 jeux de données et 82010 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1918
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`, `Open Database License (ODbL)`
- Date de création : le 24/07/2018 à 13h11
- Date de dernière mise à jour le 06/10/2019 à 14h05

## CA DE LA RIVIERA FRANCAISE

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bb48e40634f410f9101c8e3/
- Volumétrie : 1 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 7
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 03/10/2018 à 11h50
- Date de dernière mise à jour le 04/10/2019 à 10h18

## CA DE LAVAL

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bc4921f8b4c416a683d003f/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `11.zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 15/10/2018 à 15h19
- Date de dernière mise à jour le 15/10/2018 à 15h19

## CA DE NEVERS

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://www.databfc.fr/bfc/portail
- Volumétrie : 9 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5943987b88ee381f30d196f7
- Volumétrie : 9 jeux de données et 28 ressources
- Nombre de vues (tous jeux de données confondus) : 15
- Nombre de téléchargements (toutes ressources confondues) : 14
- Types de ressources disponibles : `xlsx`, `csv`, `geojson`, `zip`, `pdf`, `json`, `url`, `kmz`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 14/09/2017 à 14h01
- Date de dernière mise à jour le 30/08/2019 à 16h12

## CA DE RAMBOUILLET TERRITOIRES

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/580e1e1388ee385e8e13e4cb
- Volumétrie : 1 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `xlsx`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 25/10/2016 à 13h40
- Date de dernière mise à jour le 11/05/2018 à 09h35

## CA DE VESOUL

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://www.databfc.fr/bfc/portail
- Volumétrie : 1 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5cd5862c8b4c412132ddb84c/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 14/05/2019 à 15h20
- Date de dernière mise à jour le 14/05/2019 à 15h20

## CA DINAN AGGLOMERATION

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 10 jeux de données

## CA DRACENIE PROVENCE VERDON AGGLOMERATION

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/dracenie-provence-verdon-agglomeration
- Volumétrie : 3 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5baa4a978b4c412a0da6114c/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 26/09/2018 à 09h13
- Date de dernière mise à jour le 26/09/2018 à 09h14

## CA DU BASSIN DE BRIVE

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c9e2c5d634f412729e9e460/
- Volumétrie : 2 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`, `json`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 01/04/2019 à 11h26
- Date de dernière mise à jour le 19/09/2019 à 06h38

## CA DU BOULONNAIS

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5970a76d88ee38216d112aa1/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 9
- Nombre de téléchargements (toutes ressources confondues) : 19
- Types de ressources disponibles : `zip`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 17/11/2017 à 10h12
- Date de dernière mise à jour le 09/09/2019 à 10h34

## CA DU COTENTIN

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://www.opendata-manche.fr/web/guest/donnees
- Volumétrie : 27 jeux de données

## CA DU DOUAISIS

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59ce086088ee3865cbc4328f/
- Volumétrie : 2 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 10
- Nombre de téléchargements (toutes ressources confondues) : 15
- Types de ressources disponibles : `zip`, `pdf`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 05/12/2017 à 18h19
- Date de dernière mise à jour le 29/03/2018 à 10h36

## CA DU GRAND BESANCON

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://www.databfc.fr/bfc/portail
- Volumétrie : 8 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59196e9088ee38471c78dac6
- Volumétrie : 19 jeux de données et 69 ressources
- Nombre de vues (tous jeux de données confondus) : 135
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`, `json`, `shp`, `zip`, `gtfs`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 27/07/2018 à 14h01
- Date de dernière mise à jour le 05/10/2019 à 06h41

## CA DU GRAND CAHORS

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/589d6e22c751df0ff0ae0a65
- Volumétrie : 23 jeux de données et 27 ressources
- Nombre de vues (tous jeux de données confondus) : 32
- Nombre de téléchargements (toutes ressources confondues) : 31
- Types de ressources disponibles : `csv`, `txt`, `xlsx`, `zip`, `pdf`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence`
- Date de création : le 09/02/2017 à 15h03
- Date de dernière mise à jour le 02/07/2019 à 15h22

## CA DU PAYS AJACCIEN

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://opendata.ca-ajaccien.corsica/ouverture-des-donnees-publiques-du-pays-ajaccien/donnees/
- Volumétrie : 106 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5540eb53c751df7dafe96a3b
- Volumétrie : 14 jeux de données et 40 ressources
- Nombre de vues (tous jeux de données confondus) : 24
- Nombre de téléchargements (toutes ressources confondues) : 20
- Types de ressources disponibles : `zip`, `wms`, `wfs`, `json`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 12/05/2015 à 10h18
- Date de dernière mise à jour le 26/03/2019 à 15h10

## CA DU PAYS DE GRASSE

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://opendata.sictiam.fr/fr/organization/ca-du-pays-de-grasse
- Volumétrie : 1 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5afae4b088ee3854d32fa3d9/
- Volumétrie : 2 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 32
- Nombre de téléchargements (toutes ressources confondues) : 50
- Types de ressources disponibles : `zip`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 18/07/2018 à 11h39
- Date de dernière mise à jour le 16/01/2019 à 14h51

## CA DU PAYS DE SAINT MALO

*Communauté d'agglomération*

### Source **OpenDataSoft**
- URL : https://data.stmalo-agglomeration.fr/explore/?refine.publisher=SAINT-MALO+AGGLOMERATION
- Volumétrie : 26 jeux de données et 2122 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1147
- Types de licences utilisées : `Licence Ouverte (Etalab)`, `Licence Ouverte v2.0 (Etalab)`, `odc-odbl`, `Open Database License (ODbL)`
- Date de création : le 19/10/2015 à 12h09
- Date de dernière mise à jour le 14/05/2019 à 15h53


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5492db21c751df7a9c04805a
- Volumétrie : 8 jeux de données et 8 ressources
- Nombre de vues (tous jeux de données confondus) : 37
- Nombre de téléchargements (toutes ressources confondues) : 38
- Types de ressources disponibles : `csv`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 16/08/2016 à 09h46
- Date de dernière mise à jour le 25/08/2016 à 09h50

## CA DU PUY EN VELAY

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : http://opendata.auvergnerhonealpes.eu/7-les-donnees.htm?facet.provider[]=Communaut%C3%A9%20de%20Communes%20de%20l%27Emblavez&from=0
- Volumétrie : 1 jeux de données

## CA DU SAINT QUENTINOIS

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : http://opendata.agglo-saintquentinois.fr/datasets
- Volumétrie : 523 jeux de données

## CA DURANCE LUBERON VERDON

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/communaute-dagglomeration-durance-luberon-verdon
- Volumétrie : 3 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/es/organizations/5911d508c751df168e15a89f/
- Volumétrie : 1 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `neptune.zip`, `gtfs.zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 09/10/2018 à 10h26
- Date de dernière mise à jour le 09/10/2018 à 10h27

## CA EVREUX PORTES DE NORMANDIE

*Communauté d'agglomération*

### Source **OpenDataSoft**
- URL : https://epn-agglo.opendatasoft.com/explore/?sort=modified&refine.publisher=Evreux+Portes+de+Normandie+
- Volumétrie : 1 jeux de données
- Date de création : le 22/03/2019 à 10h10
- Date de dernière mise à jour le 06/04/2019 à 16h02

## CA FECAMP CAUX LITTORAL AGGLOMERATION

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://data.estuairedelaseine.fr/
- Volumétrie : 6 jeux de données

## CA GAP TALLARD DURANCE

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/communaute-dagglomeration-de-gap-tallard-durance
- Volumétrie : 1 jeux de données

## CA GRAND AVIGNON

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a7ad2b1c751df27e7d1190c/
- Volumétrie : 2 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 6
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `.json`, `gtfs`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 19/07/2019 à 10h12
- Date de dernière mise à jour le 19/07/2019 à 12h13

## CA GRAND BELFORT

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://www.databfc.fr/bfc/portail
- Volumétrie : 9 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5afd7ce888ee387d9f4e136f/
- Volumétrie : 28 jeux de données et 117 ressources
- Nombre de vues (tous jeux de données confondus) : 20
- Nombre de téléchargements (toutes ressources confondues) : 4
- Types de ressources disponibles : `esri rest`, `zip`, `csv`, `kml`, `geojson`, `web page`, `xls`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 28/12/2018 à 13h34
- Date de dernière mise à jour le 05/10/2019 à 06h38

## CA GRAND CHALON

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://www.databfc.fr/bfc/portail
- Volumétrie : 3 jeux de données

## CA GRAND CHAMBERY AGGLOMERATION

*Communauté d'agglomération*

### Source **OpenDataSoft**
- URL : https://donnees.grandchambery.fr/explore/?refine.publisher=Grand+Chamb%C3%A9ry
- Volumétrie : 11 jeux de données et 13727 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 6783
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 17/04/2018 à 11h48
- Date de dernière mise à jour le 30/09/2019 à 07h25


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ba24dc9634f41611dc498cc/
- Volumétrie : 30 jeux de données et 110 ressources
- Nombre de vues (tous jeux de données confondus) : 94
- Nombre de téléchargements (toutes ressources confondues) : 25
- Types de ressources disponibles : `csv`, `json`, `shp`, `pdf`, `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`
- Date de création : le 28/09/2018 à 22h16
- Date de dernière mise à jour le 03/10/2019 à 01h27

## CA GRAND GUERET

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5cc0c79b8b4c414c13d12d0a/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 24/06/2019 à 03h53
- Date de dernière mise à jour le 26/09/2019 à 13h38

## CA GRAND PARIS SUD SEINE ESSONNE SENART

*Communauté d'agglomération*

### Source **OpenDataSoft**
- URL : https://data.grandparissud.fr/explore/?refine.publisher=Grand+Paris+Sud
- Volumétrie : 15 jeux de données et 119957 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 243
- Types de licences utilisées : `Licence Ouverte (Etalab)`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 03/10/2017 à 08h07
- Date de dernière mise à jour le 30/09/2019 à 21h03


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59133d14c751df2fa69bfb86/
- Volumétrie : 2 jeux de données et 7 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 4
- Types de ressources disponibles : `geojson`, `kml`, `csv`, `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 29/05/2017 à 17h35
- Date de dernière mise à jour le 18/04/2019 à 18h57

## CA HAVRAISE

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://data.agglo-lehavre.fr
- Volumétrie : 129 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59bfa079c751df6dad38dc0d/
- Volumétrie : 49 jeux de données et 245 ressources
- Nombre de vues (tous jeux de données confondus) : 62
- Nombre de téléchargements (toutes ressources confondues) : 22
- Types de ressources disponibles : `wfs`, `kmz`, `json`, `dxf`, `shp`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 26/07/2018 à 09h19
- Date de dernière mise à jour le 26/07/2018 à 09h19

## CA HERAULT MEDITERRANEE

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bac83c9634f4106bc21d00b/
- Volumétrie : 1 jeux de données et 5 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 27/09/2018 à 11h37
- Date de dernière mise à jour le 11/09/2019 à 16h24

## CA LA ROCHE SUR YON AGGLOMERATION

*Communauté d'agglomération*

### Source **OpenDataSoft**
- URL : https://data.larochesuryon.fr/explore/?refine.publisher=La+Roche-sur-Yon+Agglom%C3%A9ration
- Volumétrie : 16 jeux de données et 3008 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1106566
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 04/09/2018 à 10h03
- Date de dernière mise à jour le 05/10/2019 à 22h02

## CA LA ROCHELLE

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ae6b61b88ee382152cd61a9/
- Volumétrie : 2 jeux de données et 10 ressources
- Nombre de vues (tous jeux de données confondus) : 16
- Nombre de téléchargements (toutes ressources confondues) : 4
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 23/08/2018 à 09h17
- Date de dernière mise à jour le 30/09/2019 à 14h24

## CA LANNION TREGOR COMMUNAUTE

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 8 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/595254f888ee384d9d265130
- Volumétrie : 9 jeux de données et 48 ressources
- Nombre de vues (tous jeux de données confondus) : 12
- Nombre de téléchargements (toutes ressources confondues) : 12
- Types de ressources disponibles : `xml`, `html`, `odata`, `csv`, `json`, `geojson`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 28/06/2017 à 09h33
- Date de dernière mise à jour le 23/11/2018 à 23h13

## CA LISIEUX NORMANDIE

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://data.estuairedelaseine.fr
- Volumétrie : 19 jeux de données

## CA LOIRE FOREZ AGGLOMERATION

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bed6ebf8b4c41329a5f7d1f/
- Volumétrie : 2 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 13/02/2019 à 08h44
- Date de dernière mise à jour le 21/03/2019 à 13h50

## CA LORIENT AGGLOMERATION

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/55b8c85288ee3827203ca288
- Volumétrie : 117 jeux de données et 303 ressources
- Nombre de vues (tous jeux de données confondus) : 251
- Nombre de téléchargements (toutes ressources confondues) : 207
- Types de ressources disponibles : `zip`, `json`, `shp`, `dwg`, `xls`, `document`, `csv`, `ecw`, `image`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 03/11/2015 à 15h25
- Date de dernière mise à jour le 05/10/2019 à 06h49

## CA MACONNAIS BEAUJOLAIS 

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c0f89d4634f41198d4a53e8/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 11/12/2018 à 11h08
- Date de dernière mise à jour le 27/08/2019 à 09h02

## CA MONT SAINT MICHEL NORMANDIE

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://www.opendata-manche.fr/web/guest/donnees
- Volumétrie : 11 jeux de données

## CA MONTARGOISE ET RIVES DU LOING 

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bfd11e4634f4170344a961b/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 31/01/2019 à 09h01
- Date de dernière mise à jour le 09/09/2019 à 14h47

## CA MOULINS COMMUNAUTE

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : http://opendata.auvergnerhonealpes.eu/7-les-donnees.htm?facet.provider[]=Communaut%C3%A9%20de%20Communes%20du%20Pays%20de%20Chevagnes%20en%20Sologne&from=0
- Volumétrie : 8 jeux de données

## CA MULHOUSE ALSACE AGGLOMERATION

*Communauté d'agglomération*

### Source **OpenDataSoft**
- URL : https://data.mulhouse-alsace.fr/explore/?refine.publisher=Mulhouse+Alsace+Agglom%C3%A9ration
- Volumétrie : 35 jeux de données et 168115 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 10157
- Types de licences utilisées : `Licence Ouverte (Etalab)`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 19/10/2017 à 14h56
- Date de dernière mise à jour le 02/10/2019 à 13h35


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5be06c41634f4158c956b300/
- Volumétrie : 120 jeux de données et 444 ressources
- Nombre de vues (tous jeux de données confondus) : 60
- Nombre de téléchargements (toutes ressources confondues) : 12
- Types de ressources disponibles : `csv`, `json`, `shp`, `gtfs-current.zip`, `zip`, `pdf`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 09/11/2018 à 11h25
- Date de dernière mise à jour le 24/09/2019 à 13h23

## CA NIME METROPOLE

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c63eb3b634f414aa0d998d4/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 9
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 09/09/2019 à 15h08
- Date de dernière mise à jour le 09/09/2019 à 15h08

## CA PARIS SACLAY

*Communauté d'agglomération*

### Source **OpenDataSoft**
- URL : https://opendata.paris-saclay.com/explore/?refine.publisher=Communaut%C3%A9+d%27agglom%C3%A9ration+Paris-Saclay
- Volumétrie : 60 jeux de données et 14580 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 2134
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`, `Open Database License (ODbL)`
- Date de création : le 13/01/2017 à 10h11
- Date de dernière mise à jour le 06/10/2019 à 10h01

## CA PARIS VALLEE DE LA MARNE

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ba1142b8b4c4139be113bca/
- Volumétrie : 5 jeux de données et 5 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 18/09/2018 à 17h26
- Date de dernière mise à jour le 08/03/2019 à 14h59

## CA PAU BEARN PYRENEES

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://opendata.agglo-pau.fr/catal2.html
- Volumétrie : 33 jeux de données

## CA PAYS FOIX VARILHES

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bd9cd77634f413406c077fd/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 31/10/2018 à 16h58
- Date de dernière mise à jour le 31/10/2018 à 17h00

## CA PORTE DE L'ISERE

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c516739634f4121096513af/
- Volumétrie : 2 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `gtfsrt.json`, `gtfsrt.pb`, `gtfs`, `neptune.zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 30/01/2019 à 10h16
- Date de dernière mise à jour le 07/02/2019 à 17h35

## CA PRIVAS CENTRE ARDECHE 

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5cadb863634f41514da8fa0c/
- Volumétrie : 3 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `License Not Specified`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 15/04/2019 à 09h00
- Date de dernière mise à jour le 11/07/2019 à 16h00

## CA PROVENCE ALPES AGGLOMERATION

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/provence-alpes-agglomeration
- Volumétrie : 2 jeux de données

## CA QUIMPER BRETAGNE OCCIDENTALE

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a82e90b88ee387c99662abb/
- Volumétrie : 7 jeux de données et 26 ressources
- Nombre de vues (tous jeux de données confondus) : 5
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `json`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 08/02/2019 à 06h39
- Date de dernière mise à jour le 08/02/2019 à 06h39

## CA REDON AGGLOMERATION

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59679365c751df12bcc44bba
- Volumétrie : 7 jeux de données et 11 ressources
- Nombre de vues (tous jeux de données confondus) : 5
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`, `xml`, `json`, `shp`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 05/10/2017 à 09h41
- Date de dernière mise à jour le 21/05/2019 à 14h29

## CA ROANNAIS AGGLOMERATION

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ae0518e88ee3874bffdf173/
- Volumétrie : 5 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv + carte`, `csv`, `xls`
- Types de licences utilisées : `License Not Specified`, `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence`
- Date de création : le 25/04/2018 à 12h20
- Date de dernière mise à jour le 13/06/2018 à 12h01

## CA RODEZ AGGLOMERATION

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bf2d970634f414ab5643cee/
- Volumétrie : 1 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 08/07/2019 à 09h43
- Date de dernière mise à jour le 04/09/2019 à 03h01

## CA ROYAN ATLANTIQUE

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://data.agglo-royan.fr/dataset
- Volumétrie : 6 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/56ab6daa88ee3859ece82d6e
- Volumétrie : 7 jeux de données et 43 ressources
- Nombre de vues (tous jeux de données confondus) : 6
- Nombre de téléchargements (toutes ressources confondues) : 10
- Types de ressources disponibles : `zip`, `csv`, `ods`, `xlsx`, `shape`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `License Not Specified`
- Date de création : le 16/03/2018 à 19h35
- Date de dernière mise à jour le 05/10/2019 à 00h00

## CA SAINT BRIEUC ARMOR AGGLOMERATION

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 2 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5acb2f2088ee3813a476714d/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 04/10/2019 à 18h09
- Date de dernière mise à jour le 04/10/2019 à 18h10

## CA SAINT LOUIS AGGLOMERATION

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a7c19f5c751df3e55b0c321
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 08/02/2018 à 11h35
- Date de dernière mise à jour le 12/09/2018 à 13h49

## CA SETE AGGLOPOLE MEDITERRANEE

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : http://opendata.agglopole.fr
- Volumétrie : 20 jeux de données

## CA SICOVAL

*Communauté d'agglomération*

### Source **OpenDataSoft**
- URL : https://data.sicoval.fr/explore/?refine.publisher=Sicoval+-+communaut%C3%A9+d%27agglom%C3%A9ration+du+Sud-Est+toulousain
- Volumétrie : 43 jeux de données et 73437 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 689
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`, `odc-odbl`, `Licence Ouverte (Etalab)`
- Date de création : le 30/06/2017 à 13h36
- Date de dernière mise à jour le 07/10/2019 à 01h20


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/586b79b3c751df75b32b7154
- Volumétrie : 26 jeux de données et 26 ressources
- Nombre de vues (tous jeux de données confondus) : 45
- Nombre de téléchargements (toutes ressources confondues) : 45
- Types de ressources disponibles : `xls`, `xlsx`, `zip`, `pdf`, `json`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 11/01/2017 à 16h17
- Date de dernière mise à jour le 14/03/2017 à 17h13

## CA SOPHIA ANTIPOLIS

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/communaute-dagglomeration-sophia-antipolis
- Volumétrie : 3 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a71cf5fc751df19bcdb5d11/
- Volumétrie : 4 jeux de données et 30 ressources
- Nombre de vues (tous jeux de données confondus) : 43
- Nombre de téléchargements (toutes ressources confondues) : 5
- Types de ressources disponibles : `gtfs`, `json`, `shapfile`, `xml`, `ecw`, `csv`, `pdf`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 21/06/2018 à 16h54
- Date de dernière mise à jour le 20/11/2018 à 11h16

## CA TERRITOIRES VENDOMOIS

*Communauté d'agglomération*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c067f178b4c410e40e88183/
- Volumétrie : 1 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 04/12/2018 à 16h12
- Date de dernière mise à jour le 04/12/2018 à 16h15

## CA VAR ESTEREL MEDITERRANEE

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/communaute-dagglomeration-var-esterel-mediterranee
- Volumétrie : 1 jeux de données

## CA VENTOUX COMTAT VENAISSIN

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/communaute-dagglomeration-ventoux-comtat-venaissin
- Volumétrie : 39 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c62d28e8b4c414299ae8286/
- Volumétrie : 42 jeux de données et 331 ressources
- Nombre de vues (tous jeux de données confondus) : 12
- Nombre de téléchargements (toutes ressources confondues) : 12
- Types de ressources disponibles : `csv`, `shp`, `geojson`, `gtfs`, `html`, `xml`, `gpx`, `kml`, `pdf`, `tab`, `json`, `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 25/06/2019 à 10h36
- Date de dernière mise à jour le 05/10/2019 à 00h00

## CA VERSAILLES GRAND PARC

*Communauté d'agglomération*

### Source **Plateforme territoriale**
- URL : http://www-cavgp.opendata.arcgis.com/datasets
- Volumétrie : 39 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/58c5a537c751df2fb0fde3c9/
- Volumétrie : 39 jeux de données et 229 ressources
- Nombre de vues (tous jeux de données confondus) : 22
- Nombre de téléchargements (toutes ressources confondues) : 10
- Types de ressources disponibles : `web page`, `geojson`, `kml`, `csv`, `esri rest`, `zip`
- Date de création : le 05/09/2017 à 04h48
- Date de dernière mise à jour le 05/09/2017 à 04h48

## CAMOEL

*Commune*

### Source **OpenDataSoft**
- URL : https://capatlantique-loireatlantique.opendatasoft.com/explore/?refine.publisher=Camoël
- Volumétrie : 2 jeux de données et 148 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 191
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 26/07/2018 à 14h34
- Date de dernière mise à jour le 04/10/2019 à 04h00

## CANDE

*Commune*

### Source **Plateforme territoriale**
- URL : https://www.opendata49.fr/index.php?id=38
- Volumétrie : 3 jeux de données

## CANTAL

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff69a3a7292c64a77d94
- Volumétrie : 6 jeux de données et 13 ressources
- Nombre de vues (tous jeux de données confondus) : 632
- Nombre de téléchargements (toutes ressources confondues) : 162
- Types de ressources disponibles : `excel`, `xls`, `pdf`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `License Not Specified`
- Date de création : le 07/05/2014 à 03h47
- Date de dernière mise à jour le 05/11/2014 à 01h37

## CASTELNAU LE LEZ

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/castelnau-le-lez
- Volumétrie : 33 jeux de données

## CASTELNAUDARY

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/58d2805b88ee3830ab40dd02
- Volumétrie : 12 jeux de données et 38 ressources
- Nombre de vues (tous jeux de données confondus) : 2404
- Nombre de téléchargements (toutes ressources confondues) : 32
- Types de ressources disponibles : `geojson`, `csv`, `kmz`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 22/03/2017 à 15h05
- Date de dernière mise à jour le 27/02/2019 à 17h16

## CASTRIES

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/castries
- Volumétrie : 27 jeux de données

## CC BAUGEOIS VALLEE

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/communaute-de-communes-vallee-des-baux-alpilles/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 17/01/2019 à 14h12
- Date de dernière mise à jour le 20/03/2019 à 15h54

## CC CAUSSES ET VALLEE DE LA DORDOGNE

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bb5cf6f634f413b3b296999/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 04/10/2018 à 10h53
- Date de dernière mise à jour le 27/05/2019 à 13h44

## CC CAUX ESTUAIRE

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : https://data.estuairedelaseine.fr
- Volumétrie : 23 jeux de données

## CC COEUR COTE FLEURIE

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : http://data.coeurcotefleurie.org/dataset
- Volumétrie : 51 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59f846d588ee380840aa2865/
- Volumétrie : 53 jeux de données et 251 ressources
- Nombre de vues (tous jeux de données confondus) : 366
- Nombre de téléchargements (toutes ressources confondues) : 43
- Types de ressources disponibles : `geojson`, `kml`, `shp`, `wms`, `csv`, `ods`, `json`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 16/11/2017 à 15h35
- Date de dernière mise à jour le 05/10/2019 à 02h15

## CC COEUR D'OSTREVENT

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c937ac98b4c41415cbe68c8/
- Volumétrie : 1 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xls`, `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 21/03/2019 à 13h17
- Date de dernière mise à jour le 21/03/2019 à 13h18

## CC COEUR HAUTE LANDE

*Communauté de communes*

### Source **OpenDataSoft**
- URL : https://numerique.brocaslesforges.fr/explore/?sort=title&refine.publisher=Communaut%C3%A9+de+Communes+Coeur+Haute+Lande
- Volumétrie : 3 jeux de données et 10 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 319
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 15/06/2019 à 01h18
- Date de dernière mise à jour le 15/06/2019 à 08h03

## CC COMBRAILLES SIOULE ET MORGE

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : http://opendata.auvergnerhonealpes.eu/7-les-donnees.htm?facet.provider[]=Communaut%C3%A9%20de%20Communes%20Manzat%20Communaut%C3%A9&from=0
- Volumétrie : 3 jeux de données

## CC DU BASSIN DE PONT A MOUSSON 

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c18eb5a634f41110ca05145/
- Volumétrie : 1 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 18/12/2018 à 13h55
- Date de dernière mise à jour le 18/12/2018 à 15h18

## CC DU BRIANCONNAIS

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/communaute-de-communes-du-brianconnais
- Volumétrie : 1 jeux de données

## CC DU FRONTONNAIS

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/583ef3d9c751df6abcc0bb7e
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 30/11/2016 à 17h21
- Date de dernière mise à jour le 30/11/2016 à 17h36

## CC DU GRAND AUTUNOIS MORVAN

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c46ef148b4c416a35762335/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 22/01/2019 à 11h34
- Date de dernière mise à jour le 22/01/2019 à 11h35

## CC DU PAYS DE LAPALISSE

*Communauté de communes*

### Source **OpenDataSoft**
- URL : http://opendata.auvergnerhonealpes.eu/7-les-donnees.htm?facet.provider[]=Communaut%C3%A9%20de%20Communes%20du%20Pays%20de%20Lapalisse&from=0
- Volumétrie : aucun jeu de données

## CC DU PAYS DE SAINT ELOY

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : http://opendata.auvergnerhonealpes.eu/7-les-donnees.htm?facet.provider[]=Communaut%C3%A9%20de%20Communes%20Coeur%20de%20Combrailles&from=0
- Volumétrie : 2 jeux de données

## CC DU PAYS DE SALERS

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : http://opendata.auvergnerhonealpes.eu/7-les-donnees.htm?facet.provider[]=Communaut%C3%A9%20de%20Communes%20du%20Pays%20de%20Salers&from=0
- Volumétrie : 7 jeux de données

## CC DU PAYS MORNANTAIS

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bbdad47634f410fb7e37ca7/
- Volumétrie : 7 jeux de données et 6 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`, `xlsx`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 10/10/2018 à 09h51
- Date de dernière mise à jour le 25/04/2019 à 12h38

## CC DU PAYS REUNI D'ORANGE

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/communaute-de-communes-du-pays-reuni-dorange
- Volumétrie : 22 jeux de données

## CC DU PLATEAU PICARD

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/596c880888ee387e51f2c2f6/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 24/07/2018 à 16h20
- Date de dernière mise à jour le 24/07/2018 à 16h20

## CC DU VAL D'ILLE AUBIGNE

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/57daa20388ee385c305ff491
- Volumétrie : 41 jeux de données et 653 ressources
- Nombre de vues (tous jeux de données confondus) : 17
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `json`, `shp`, `zip`, `document`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 29/07/2018 à 08h53
- Date de dernière mise à jour le 08/02/2019 à 06h57

## CC HAUTES TERRES COMMUNAUTE

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : http://opendata.auvergnerhonealpes.eu/7-les-donnees.htm?facet.provider[]=Communaut%C3%A9%20de%20Communes%20du%20C%C3%A9zallier&from=0
- Volumétrie : 6 jeux de données

## CC LEFF ARMOR

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 5 jeux de données

## CC MONFORT COMMUNAUTE

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b18fb70c751df1da631a2da/
- Volumétrie : 4 jeux de données et 5 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 18/09/2018 à 12h15
- Date de dernière mise à jour le 04/01/2019 à 10h17

## CC PLAINE LIMAGNE

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : http://opendata.auvergnerhonealpes.eu/7-les-donnees.htm?facet.provider[]=Communaut%C3%A9%20de%20Communes%20Limagne%20Bords%20Allier&from=0
- Volumétrie : 1 jeux de données

## CC POHER COMMUNAUTE

*Communauté de communes*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 1 jeux de données

## CC SARREBOURG MOSELLE SUD

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bd9653a634f417672a9dd46/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 31/10/2018 à 14h57
- Date de dernière mise à jour le 31/10/2018 à 14h57

## CC VALLEE DES BAUX ALPILLES

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bfbf1548b4c412a0cdc6c70/
- Volumétrie : 5 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `csv`, `kml`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 29/11/2018 à 11h26
- Date de dernière mise à jour le 11/09/2019 à 16h11

## CC VITRY CHAMPAGNE ET DER

*Communauté de communes*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a32371888ee387affdaa88d/
- Volumétrie : 1 jeux de données et 17 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `jp2`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 14/12/2017 à 16h47
- Date de dernière mise à jour le 18/04/2018 à 13h51

## CENTRE VAL DE LOIRE

*Région*

### Source **OpenDataSoft**
- URL : https://data.centrevaldeloire.fr/explore/?refine.publisher=R%C3%A9gion+Centre-Val+de+Loire
- Volumétrie : 67 jeux de données
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`, `Licence Ouverte (Etalab)`, `Open Database License (ODbL)`
- Date de création : le 30/10/2017 à 14h37
- Date de dernière mise à jour le 07/10/2019 à 00h22

## CESSON

*Commune*

### Source **OpenDataSoft**
- URL : https://data.ville-cesson.fr/explore/?refine.publisher=Commune+de+Cesson
- Volumétrie : 1 jeux de données
- Date de création : le 26/10/2018 à 13h46
- Date de dernière mise à jour le 26/10/2018 à 13h52

## CESSON SEVIGNE

*Commune*

### Source **OpenDataSoft**
- URL : https://data.rennesmetropole.fr/explore/?refine.publisher=Ville+de+Cesson-S%C3%A9vign%C3%A9
- Volumétrie : 9 jeux de données et 1763 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 5276
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 15/01/2018 à 13h29
- Date de dernière mise à jour le 03/12/2018 à 15h22

## CHALONS EN CHAMPAGNE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5badf0508b4c412b0839e0f4/
- Volumétrie : 15 jeux de données et 86 ressources
- Nombre de vues (tous jeux de données confondus) : 28
- Nombre de téléchargements (toutes ressources confondues) : 15
- Types de ressources disponibles : `geojson`, `kml`, `zip`, `web page`, `csv`, `esri rest`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 01/10/2018 à 17h11
- Date de dernière mise à jour le 01/10/2018 à 17h11

## CHAMBERY

*Commune*

### Source **OpenDataSoft**
- URL : https://donnees.grandchambery.fr/explore/?refine.publisher=Ville+de+Chamb%C3%A9ry
- Volumétrie : 7 jeux de données et 2998 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1994
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 23/08/2018 à 15h06
- Date de dernière mise à jour le 15/09/2019 à 16h55

## CHAMBERY GRAND LAC ECONOMIE

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bcf2a798b4c412f2198fab2/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 12/02/2019 à 10h59
- Date de dernière mise à jour le 12/02/2019 à 11h00

## CHAMPAGNE SUR SEINE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5846e4ae88ee3811d6c65bb3/
- Volumétrie : 5 jeux de données et 25 ressources
- Nombre de vues (tous jeux de données confondus) : 17
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `pdf`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 16/03/2017 à 11h56
- Date de dernière mise à jour le 11/04/2019 à 10h49

## CHARENTE

*Département*

### Source **Plateforme territoriale**
- URL : https://data16.lacharente.fr/donnees/recherche/
- Volumétrie : 57 jeux de données

## CHARENTE MARITIME

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59b6449a88ee38073588ac9d
- Volumétrie : 1 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 45
- Types de ressources disponibles : `pdf`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 25/10/2017 à 17h57
- Date de dernière mise à jour le 16/01/2018 à 17h26

## CHARENTE TOURISME

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data16.lacharente.fr/donnees/recherche/
- Volumétrie : 16 jeux de données

## CHARTRES METROPOLE TRANSPORTS 

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c35c8518b4c416fe6cb42c9/
- Volumétrie : 7 jeux de données et 14 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 4
- Types de ressources disponibles : `zip`, `txt`
- Types de licences utilisées : `License Not Specified`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 24/01/2019 à 12h19
- Date de dernière mise à jour le 19/07/2019 à 10h51

## CHASSIEU

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.beta.grandlyon.com/fr/recherche
- Volumétrie : 10 jeux de données

## CHATEAUROUX

*Commune*

### Source **OpenDataSoft**
- URL : https://data.chateauroux-metropole.fr/explore/?sort=title&refine.publisher=VILLE+DE+CHATEAUROUX
- Volumétrie : 7 jeux de données et 2801 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1741
- Date de création : le 22/01/2019 à 15h24
- Date de dernière mise à jour le 01/04/2019 à 10h27

## CHATOU

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c9e2bfb634f41255724d4ca/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 29/03/2019 à 15h53
- Date de dernière mise à jour le 29/03/2019 à 15h53

## CHAVILLE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5437e8c288ee387cb68f5e87
- Volumétrie : 14 jeux de données et 19 ressources
- Nombre de vues (tous jeux de données confondus) : 74
- Nombre de téléchargements (toutes ressources confondues) : 112
- Types de ressources disponibles : `csv`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 09/01/2015 à 09h26
- Date de dernière mise à jour le 09/06/2015 à 16h34

## CHEMERE

*Commune*

### Source **OpenDataSoft**
- URL : https://data.loire-atlantique.fr/explore/?refine.publisher=Ville+de+Chéméré
- Volumétrie : 4 jeux de données et 332 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 434
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 27/09/2015 à 00h00
- Date de dernière mise à jour le 01/06/2018 à 11h58

## CHENNEVIERES SUR MARNE

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.chennevieres.com/portail
- Volumétrie : 5 jeux de données

## CITEDIA

*Délégataire de service public*

### Source **OpenDataSoft**
- URL : https://data.rennesmetropole.fr/explore/?refine.publisher=Cit%C3%A9dia
- Volumétrie : 2 jeux de données et 10 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 550047
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 07/09/2016 à 09h53
- Date de dernière mise à jour le 07/10/2019 à 04h20

## CLAPIERS

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/clapiers
- Volumétrie : 37 jeux de données

## COGOLIN

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bac7fb68b4c414fe45d8cee/
- Volumétrie : 3 jeux de données et 6 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 27/09/2018 à 09h38
- Date de dernière mise à jour le 27/09/2018 à 14h26

## COLOMIERS

*Commune*

### Source **OpenDataSoft**
- URL : https://data.toulouse-metropole.fr/explore/?refine.publisher=Mairie+de+Colomiers
- Volumétrie : 1 jeux de données et 37 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1738
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 04/06/2015 à 00h12
- Date de dernière mise à jour le 31/12/2017 à 17h47

## COMBS LA VILLE

*Commune*

### Source **OpenDataSoft**
- URL : https://combslaville-grandparissud.opendatasoft.com/explore/?refine.publisher=Mairie+de+Combs-la-Ville
- Volumétrie : 7 jeux de données et 2104 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 152
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 26/09/2018 à 09h20
- Date de dernière mise à jour le 23/09/2019 à 15h54

## COMINES

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/593fb6b788ee3810680f7775
- Volumétrie : 3 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 7
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 02/08/2018 à 16h27
- Date de dernière mise à jour le 06/02/2019 à 10h04

## COMITE REGIONAL DU TOURISME ILE DE FRANCE

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://data.iledefrance.fr/explore/?refine.publisher=Comit%C3%A9+r%C3%A9gional+du+tourisme
- Volumétrie : 7 jeux de données et 1894 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 18093
- Types de licences utilisées : `Licence ouverte`
- Date de création : le 27/02/2014 à 10h40
- Date de dernière mise à jour le 16/03/2019 à 18h08

## COMPAGNIE DES TRANSPORTS STRASBOURGEOIS

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ae16ff988ee3848de337bb3/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 7
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 26/04/2018 à 08h28
- Date de dernière mise à jour le 26/04/2018 à 08h28

## CONFLANS SAINTE HONORINE

*Commune*

### Source **OpenDataSoft**
- URL : https://data.ville-conflans.fr/explore/?refine.publisher=Ville+de+Conflans-Sainte-Honorine
- Volumétrie : aucun jeu de données

## CORSE

*Région*

### Source **OpenDataSoft**
- URL : https://www.data.corsica/explore/?refine.publisher=Collectivit%C3%A9+Territoriale+de+Corse
- Volumétrie : 117 jeux de données et 31734 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 47595
- Types de licences utilisées : `Licence Ouverte (Etalab)`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 27/01/2014 à 13h41
- Date de dernière mise à jour le 29/04/2019 à 15h18


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/537d58eea3a72973a2dc026c
- Volumétrie : 536 jeux de données et 1738 ressources
- Nombre de vues (tous jeux de données confondus) : 1931
- Nombre de téléchargements (toutes ressources confondues) : 1476
- Types de ressources disponibles : `csv`, `json`, `obj`, `shp`, `zip`, `pdf`, `xlsx`, `xlb`, `xls`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`, `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence`
- Date de création : le 24/05/2014 à 12h12
- Date de dernière mise à jour le 03/10/2019 à 01h25

## CORSE COMPETENCES

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://www.data.corsica/explore/?refine.publisher=GIP+Corse+Comp%C3%A9tences
- Volumétrie : 37 jeux de données et 12382 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 70484
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 27/01/2014 à 13h04
- Date de dernière mise à jour le 07/01/2018 à 13h39

## CORSICA STATISTICA

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://www.data.corsica/explore/?refine.publisher=Corsica+Statistica
- Volumétrie : 20 jeux de données et 633 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 2908
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 08/03/2017 à 10h51
- Date de dernière mise à jour le 07/01/2018 à 18h26

## COTES D'ARMOR

*Département*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 124 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/54aff788c751df39e138127d
- Volumétrie : 123 jeux de données et 668 ressources
- Nombre de vues (tous jeux de données confondus) : 149
- Nombre de téléchargements (toutes ressources confondues) : 54
- Types de ressources disponibles : `html`, `odata`, `csv`, `xml`, `json`, `geojson`, `fr/le-departement/fonctionnement-et-organisation/les-maisons-du-departement`, `com/planifier/sports-loisirs/randonnees/les-grands-itineraires/ev1-la-velodyssee`, `fr/data-presentation-ux/#/cg22/datasets/assmat/views/grid?primary-bg-color=@046d8b&primary-font-color=@fff&hidecontrolpanel=true`, `aspx`, `fr/`, `fr`, `pdf`, `fr/politiques-ministerielles/livre-et-lecture/bibliotheques/observatoire-de-la-lecture-publique/repondre-a-l-enquete/enquete-sur-les-bibliotheques-municipales`, `php?page=collections-photographiques`, `php?page=cartes-postales`, `org/wiki/classification_d%c3%a9cimale_de_dewey`, `fr/data-presentation-ux/#/cg22/datasets/assistant_maternel/views/grid`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 27/01/2016 à 10h26
- Date de dernière mise à jour le 16/09/2019 à 05h44

## COULOMMIERS

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff6ba3a7292c64a77da2
- Volumétrie : 26 jeux de données et 26 ressources
- Nombre de vues (tous jeux de données confondus) : 44
- Nombre de téléchargements (toutes ressources confondues) : 199
- Types de ressources disponibles : `xlsx`, `doc`, `xls`, `txt`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 07/05/2014 à 03h42
- Date de dernière mise à jour le 28/07/2015 à 18h19

## COURCUIRE

*Commune*

### Source **Plateforme territoriale**
- URL : https://www.databfc.fr/bfc/portail
- Volumétrie : 1 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c937c53634f414c0c84251d/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 21/03/2019 à 15h47
- Date de dernière mise à jour le 21/03/2019 à 15h48

## COURNONSEC

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/cournonsec
- Volumétrie : 24 jeux de données

## COURNONTERRAL

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/cournonterral
- Volumétrie : 31 jeux de données

## CU DE DUNKERQUE

*Communauté urbaine*

### Source **OpenDataSoft**
- URL : https://data.dunkerque-agglo.fr/explore/?sort=modified&refine.publisher=Communauté+urbaine+de+Dunkerque
- Volumétrie : 65 jeux de données
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 08/11/2018 à 14h44
- Date de dernière mise à jour le 07/10/2019 à 00h16


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c123930634f4179b683b998/
- Volumétrie : 1 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 01/07/2019 à 03h44
- Date de dernière mise à jour le 25/09/2019 à 11h47

## CU DU GRAND POITIERS

*Communauté urbaine*

### Source **OpenDataSoft**
- URL : https://data.grandpoitiers.fr/explore/?refine.publisher=Grand+Poitiers+
- Volumétrie : 91 jeux de données
- Types de licences utilisées : `Licence Ouverte (Etalab)`, `Licence Ouverte v2.0 (Etalab)`
- Date de dernière mise à jour le 07/10/2019 à 04h32


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/54da1f52c751df59f7467389
- Volumétrie : 244 jeux de données et 813 ressources
- Nombre de vues (tous jeux de données confondus) : 804
- Nombre de téléchargements (toutes ressources confondues) : 102
- Types de ressources disponibles : `csv`, `json`, `pdf`, `shp`, `obj`, `xlsx`, `xls`, `document`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`, `Licence Ouverte / Open Licence`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 03/07/2015 à 14h11
- Date de dernière mise à jour le 05/10/2019 à 04h04

## CU GRAND REIMS

*Communauté urbaine*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5aabc0ecc751df25cba8382b/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 14
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 03/10/2018 à 12h07
- Date de dernière mise à jour le 25/09/2019 à 16h28

## CUGNAUX

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/58d27ed688ee383022f3994b/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `geojson`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 22/03/2017 à 15h36
- Date de dernière mise à jour le 22/03/2017 à 15h36

## DEUX SEVRES

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59ba4600c751df6bbdb72c11
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xls`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 30/03/2018 à 11h43
- Date de dernière mise à jour le 08/11/2018 à 11h35

## DIGNE LES BAINS

*Commune*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/ville-de-digne-les-bains
- Volumétrie : 94 jeux de données

## DOLUS D'OLERON

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c8a2eaa8b4c412414de233d/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 14/03/2019 à 12h16
- Date de dernière mise à jour le 14/03/2019 à 12h17

## DONGES

*Commune*

### Source **OpenDataSoft**
- URL : https://data.agglo-carene.fr/explore/?refine.publisher=Donges
- Volumétrie : 2 jeux de données et 317 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 230
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 23/04/2019 à 09h05
- Date de dernière mise à jour le 06/10/2019 à 09h30

## DOUBS

*Département*

### Source **OpenDataSoft**
- URL : https://opendata.doubs.fr/explore/?refine.publisher=CD25
- Volumétrie : 30 jeux de données et 154518 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 13751
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 20/03/2018 à 15h58
- Date de dernière mise à jour le 07/10/2019 à 04h10

## DOUBS TRES HAUT DEBIT

*Autre groupement de collectivités territoriales*

### Source **OpenDataSoft**
- URL : https://opendata.doubs.fr/explore/?refine.publisher=Syndicat+Mixte+Doubs+Tr%C3%A8s+Haut+Debit
- Volumétrie : 3 jeux de données et 1319 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1890
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 23/04/2018 à 12h15
- Date de dernière mise à jour le 06/10/2019 à 20h01

## DRAGUIGNAN

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bfd0fce634f416c97b0b0a9/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `*.pdf`, `.pdf`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 19/12/2018 à 09h28
- Date de dernière mise à jour le 19/12/2018 à 09h39

## DRAGUIGNAN

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bfd0fce634f416c97b0b0a9/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `*.pdf`, `.pdf`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 19/12/2018 à 09h28
- Date de dernière mise à jour le 19/12/2018 à 09h39

## DREUX

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a82b75a88ee3827fadf1afb/
- Volumétrie : 1 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 11/01/2019 à 10h23
- Date de dernière mise à jour le 31/07/2019 à 05h31

## EGUISHEIM

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c6ac6e58b4c417067851df5/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 18/02/2019 à 16h06
- Date de dernière mise à jour le 19/03/2019 à 08h06

## ENNETIERES EN WEPPES

*Commune*

### Source **OpenDataSoft**
- URL : https://opendata.lillemetropole.fr/explore/?&refine.publisher=commune+Enneti%C3%A8res+en+Weppes
- Volumétrie : 6 jeux de données et 58 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 5748
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 21/06/2017 à 13h47
- Date de dernière mise à jour le 27/09/2019 à 13h34

## EURE

*Département*

### Source **Plateforme territoriale**
- URL : http://www.opendata-27-76.fr
- Volumétrie : 73 jeux de données

## EURE ET LOIR

*Département*

### Source **OpenDataSoft**
- URL : https://data.eurelien.fr/explore/?refine.publisher=CD28
- Volumétrie : 30 jeux de données et 21510 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 15757
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 26/07/2017 à 14h05
- Date de dernière mise à jour le 06/10/2019 à 22h02


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/58b3e7e588ee3814c598d150
- Volumétrie : 31 jeux de données et 116 ressources
- Nombre de vues (tous jeux de données confondus) : 77
- Nombre de téléchargements (toutes ressources confondues) : 8
- Types de ressources disponibles : `csv`, `json`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 14/03/2018 à 01h01
- Date de dernière mise à jour le 04/10/2019 à 22h02

## EURE ET LOIR NUMERIQUE

*Autre groupement de collectivités territoriales*

### Source **OpenDataSoft**
- URL : https://data.eurelien.fr/explore/?refine.publisher=Eure-et-Loir+Numérique
- Volumétrie : 2 jeux de données et 321 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 5
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 01/09/2017 à 15h02
- Date de dernière mise à jour le 14/12/2018 à 07h51

## EUROMETROPOLE DE STRASBOURG

*Métropole*

### Source **OpenDataSoft**
- URL : https://data.strasbourg.eu/explore/?sort=modified&refine.publisher=Eurom%C3%A9tropole+de+Strasbourg
- Volumétrie : 64 jeux de données et 482554 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 2488
- Types de licences utilisées : `Licence Ouverte (Etalab)`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 23/10/2018 à 13h27
- Date de dernière mise à jour le 07/10/2019 à 04h28


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59d2b2dc88ee3826a73899be
- Volumétrie : 3 jeux de données et 7 ressources
- Nombre de vues (tous jeux de données confondus) : 26
- Nombre de téléchargements (toutes ressources confondues) : 21
- Types de ressources disponibles : `csv`, `1_0.pdf`, `zip`, `pdf`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 08/10/2017 à 20h42
- Date de dernière mise à jour le 13/08/2019 à 08h55

## EZE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c388d5a8b4c415b415729d7/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `pdf`
- Types de licences utilisées : `Open Data Commons Public Domain Dedication and Licence (PDDL)`
- Date de création : le 11/01/2019 à 15h14
- Date de dernière mise à jour le 11/01/2019 à 15h15

## FABREGUES

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/fabr%C3%A8gues
- Volumétrie : 26 jeux de données

## FAUGERES

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/57ad996ac751df5e2c97bae5/
- Volumétrie : 5 jeux de données et 61 ressources
- Nombre de vues (tous jeux de données confondus) : 28
- Nombre de téléchargements (toutes ressources confondues) : 21
- Types de ressources disponibles : `html`, `pdf`
- Types de licences utilisées : `Creative Commons Attribution Share-Alike`
- Date de création : le 12/08/2016 à 12h59
- Date de dernière mise à jour le 15/08/2016 à 20h20

## FEGREAC

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bb35a75634f410c19c29451/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 02/10/2018 à 13h49
- Date de dernière mise à jour le 02/10/2018 à 13h50

## FEREL

*Commune*

### Source **OpenDataSoft**
- URL : https://capatlantique-loireatlantique.opendatasoft.com/explore/?refine.publisher=Férel
- Volumétrie : 2 jeux de données et 90 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 209
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 07/08/2018 à 07h26
- Date de dernière mise à jour le 04/10/2019 à 04h00

## FINISTERE

*Département*

### Source **OpenDataSoft**
- URL : https://opendata.finistere.fr/explore/?refine.publisher=D%C3%A9partement+du+Finist%C3%A8re
- Volumétrie : 39 jeux de données et 42529 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 10973
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 15/10/2018 à 17h36
- Date de dernière mise à jour le 18/09/2019 à 09h39


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a86b30588ee38553adbaa6e/
- Volumétrie : 41 jeux de données et 119 ressources
- Nombre de vues (tous jeux de données confondus) : 26
- Nombre de téléchargements (toutes ressources confondues) : 5
- Types de ressources disponibles : `csv`, `json`, `zip`, `shp`, `xls`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`
- Date de création : le 27/11/2018 à 15h36
- Date de dernière mise à jour le 18/09/2019 à 09h39

## FIRMINY

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5d15b4d8634f4151ec5b1550/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `-a-23k-ville-de-firminy.xlsx`
- Types de licences utilisées : `Other (Public Domain)`
- Date de création : le 28/06/2019 à 03h39
- Date de dernière mise à jour le 28/06/2019 à 03h39

## FLEURY SUR ORNE

*Commune*

### Source **OpenDataSoft**
- URL : https://data.fleurysurorne.fr/explore/?refine.publisher=Ville+de+Fleury-sur-Orne
- Volumétrie : 42 jeux de données et 2488 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 78
- Types de licences utilisées : `Licence Ouverte (Etalab)`, `Open Database License (ODbL)`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 29/05/2018 à 10h06
- Date de dernière mise à jour le 06/10/2019 à 08h02

## FLOURENS

*Commune*

### Source **OpenDataSoft**
- URL : https://data.toulouse-metropole.fr/explore/?refine.publisher=Mairie+de+Flourens
- Volumétrie : 1 jeux de données et 24 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 2729
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 26/11/2015 à 09h23
- Date de dernière mise à jour le 31/12/2017 à 17h47

## FUCLEM

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b34a85fc751df441182f347/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 03/09/2019 à 12h49
- Date de dernière mise à jour le 03/09/2019 à 12h50

## GANGES

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c45a5c18b4c4104123ed6d5/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 31/01/2019 à 10h43
- Date de dernière mise à jour le 31/01/2019 à 12h12

## GEOVENDEE MAISON DES COMMUNES

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : https://data-vendee-fr-paysdelaloire.opendatasoft.com/explore/?refine.publisher=Géo+Vendée
- Volumétrie : 2 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c18fe498b4c4115a1a7ee80/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 11
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 18/12/2018 à 18h01
- Date de dernière mise à jour le 13/06/2019 à 14h26

## GERS

*Département*

### Source **OpenDataSoft**
- URL : https://data.gers.fr/explore/?disjunctive.publisher&sort=modified&refine.publisher=Département+du+Gers
- Volumétrie : 16 jeux de données et 195013 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 2132
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 01/02/2015 à 15h14
- Date de dernière mise à jour le 05/08/2019 à 08h53


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/56f3b0a6c751df13e653faef
- Volumétrie : aucun jeu de données

## GIP ALFA CENTRE

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://data.centrevaldeloire.fr/explore/?refine.publisher=GIP+Alfa+Centre
- Volumétrie : aucun jeu de données

## GIP TERRITOIRES NUMERIQUES BFC

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://www.databfc.fr/bfc/portail
- Volumétrie : 3 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b4f5364c751df0bb1c28756/
- Volumétrie : 5 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 32
- Nombre de téléchargements (toutes ressources confondues) : 11
- Types de ressources disponibles : `csv`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `License Not Specified`
- Date de création : le 19/07/2018 à 11h16
- Date de dernière mise à jour le 28/03/2019 à 08h18

## GIRONDE

*Département*

### Source **Plateforme territoriale**
- URL : https://www.datalocale.fr/dataset?organization=cd33
- Volumétrie : 94 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff67a3a7292c64a77d83/
- Volumétrie : 87 jeux de données et 207 ressources
- Nombre de vues (tous jeux de données confondus) : 745
- Nombre de téléchargements (toutes ressources confondues) : 1452
- Types de ressources disponibles : `shape`, `csv`, `geojson`, `html`, `kml`, `wms`, `xls`, `zip`, `kmz`, `ods`, `json`, `rdf`, `xml`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence`
- Date de création : le 07/05/2014 à 03h38
- Date de dernière mise à jour le 18/09/2013 à 08h48

## GIRONDE TOURISME

*Organisme associé de collectivité territoriale*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff62a3a7292c64a77d5b/
- Volumétrie : 30 jeux de données et 59 ressources
- Nombre de vues (tous jeux de données confondus) : 176
- Nombre de téléchargements (toutes ressources confondues) : 1260
- Types de ressources disponibles : `csv`, `html`, `ods`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence`
- Date de création : le 07/05/2014 à 03h47
- Date de dernière mise à jour le 18/09/2013 à 08h48

## GRABELS

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/grabels
- Volumétrie : 24 jeux de données

## GRAND EST

*Région*

### Source **Plateforme territoriale**
- URL : http://data.grandest.fr/donnees
- Volumétrie : 39 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/55df259f88ee386e57a46ec2
- Volumétrie : 27 jeux de données et 52 ressources
- Nombre de vues (tous jeux de données confondus) : 66
- Nombre de téléchargements (toutes ressources confondues) : 64
- Types de ressources disponibles : `document`, `json`, `shp`, `gml`, `shapefile`, `wfs`, `xml`, `1.docx`, `txt`, `xlsx`, `kml`, `xls`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`, `License Not Specified`
- Date de création : le 25/09/2015 à 11h13
- Date de dernière mise à jour le 10/06/2019 à 06h38

## GRAND PARIS SEINE OUEST

*Autre groupement de collectivités territoriales*

### Source **OpenDataSoft**
- URL : https://data.seineouest.fr/explore/?sort=records_count&refine.publisher=Seine+Ouest
- Volumétrie : 77 jeux de données et 71940 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 21723
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 13/02/2019 à 10h35
- Date de dernière mise à jour le 06/10/2019 à 23h09


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/543e77eb88ee380c0d999949
- Volumétrie : 82 jeux de données et 288 ressources
- Nombre de vues (tous jeux de données confondus) : 85
- Nombre de téléchargements (toutes ressources confondues) : 5
- Types de ressources disponibles : `csv`, `json`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 16/04/2019 à 16h53
- Date de dernière mise à jour le 04/10/2019 à 23h07

## GRAND PORT MARITIME DE ROUEN

*Délégataire de service public*

### Source **Plateforme territoriale**
- URL : https://data.estuairedelaseine.fr
- Volumétrie : 3 jeux de données

## GRAND PORT MARITIME DU HAVRE

*Délégataire de service public*

### Source **Plateforme territoriale**
- URL : https://data.estuairedelaseine.fr
- Volumétrie : 22 jeux de données

## GRANVILLE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c6ab1d98b4c41356c6fa89a/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 18/02/2019 à 14h32
- Date de dernière mise à jour le 18/02/2019 à 14h32

## GRENOBLE

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.metropolegrenoble.fr/ckan/dataset?organization=grenoble
- Volumétrie : 50 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5733038988ee38472ed1b934/
- Volumétrie : 52 jeux de données et 218 ressources
- Nombre de vues (tous jeux de données confondus) : 122
- Nombre de téléchargements (toutes ressources confondues) : 68
- Types de ressources disponibles : `csv`, `kml`, `geojson`, `dxf`, `json`, `kmz`, `shp`
- Types de licences utilisées : `License Not Specified`, `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 14/06/2016 à 16h15
- Date de dernière mise à jour le 05/10/2019 à 02h26

## GRENOBLE ALPES METROPOLE

*Métropole*

### Source **Plateforme territoriale**
- URL : http://data.metropolegrenoble.fr/ckan/dataset?organization=grenoble-alpes-metropole
- Volumétrie : 12 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5732ff7788ee382b08d1b934
- Volumétrie : 16 jeux de données et 35 ressources
- Nombre de vues (tous jeux de données confondus) : 19
- Nombre de téléchargements (toutes ressources confondues) : 8
- Types de ressources disponibles : `geojson`, `json`, `csv`, `kml`, `kmz`, `xls`, `ods`, `shp`
- Types de licences utilisées : `License Not Specified`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 14/06/2016 à 15h49
- Date de dernière mise à jour le 05/10/2019 à 02h26

## GUEMENE PENFAO

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b8fe384634f414b25ea1d30/
- Volumétrie : 1 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 05/09/2018 à 17h25
- Date de dernière mise à jour le 05/09/2018 à 17h30

## GUERANDE

*Commune*

### Source **OpenDataSoft**
- URL : https://capatlantique-loireatlantique.opendatasoft.com/explore/?refine.publisher=Guérande
- Volumétrie : 2 jeux de données et 291 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 252
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 05/10/2018 à 14h35
- Date de dernière mise à jour le 04/10/2019 à 04h00

## GUIDEL

*Commune*

### Source **OpenDataSoft**
- URL : https://www.opendata56.fr/explore/?refine.publisher=Ville+de+Guidel
- Volumétrie : 1 jeux de données et 19 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 4
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 19/10/2018 à 07h31
- Date de dernière mise à jour le 17/11/2018 à 08h30

## GUINGUAMP

*Commune*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 11 jeux de données

## GWAD'AIR

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data-gwadair.opendata.arcgis.com/datasets
- Volumétrie : 42 jeux de données

## HAUTE GARONNE

*Département*

### Source **OpenDataSoft**
- URL : https://data.haute-garonne.fr/explore/?refine.publisher=Conseil+départemental+de+la+Haute-Garonne
- Volumétrie : 54 jeux de données et 88814 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 54554
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`, `Open Database License (ODbL)`, `Licence Ouverte (Etalab)`
- Date de création : le 20/11/2015 à 15h41
- Date de dernière mise à jour le 07/10/2019 à 04h02


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/582a045088ee3875cec65bb4
- Volumétrie : 102 jeux de données et 392 ressources
- Nombre de vues (tous jeux de données confondus) : 349
- Nombre de téléchargements (toutes ressources confondues) : 50
- Types de ressources disponibles : `csv`, `json`, `shp`, `pdf`, `zip`, `jpe`, `obj`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`, `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 17/11/2016 à 10h44
- Date de dernière mise à jour le 05/10/2019 à 03h01

## HAUTE GARONNE TOURISME 

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://data.haute-garonne.fr/explore/?refine.publisher=Comit%C3%A9+D%C3%A9partemental+du+Tourisme+de+la+Haute-Garonne+(CDT31)
- Volumétrie : 2 jeux de données et 463 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1784
- Types de licences utilisées : `Licence Ouverte (Etalab)`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 08/01/2016 à 16h13
- Date de dernière mise à jour le 02/10/2019 à 15h01

## HAUTES ALPES

*Département*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/conseil-departemental-des-hautes-alpes
- Volumétrie : 8 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a98083188ee387ee20c3cdb/
- Volumétrie : 55 jeux de données et 278 ressources
- Nombre de vues (tous jeux de données confondus) : 76
- Nombre de téléchargements (toutes ressources confondues) : 43
- Types de ressources disponibles : `zip`, `json`, `shp`, `document`, `image`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 08/02/2019 à 06h39
- Date de dernière mise à jour le 08/02/2019 à 06h40

## HAUTES PYRENEES

*Département*

### Source **OpenDataSoft**
- URL : https://opendata.ha-py.fr/explore/?refine.publisher=D%C3%A9partement+des+Hautes-Pyr%C3%A9n%C3%A9es
- Volumétrie : 33 jeux de données
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 21/03/2018 à 15h47
- Date de dernière mise à jour le 06/10/2019 à 05h30


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c3f1ca2634f411d2aef4ddb/
- Volumétrie : 33 jeux de données et 122 ressources
- Nombre de vues (tous jeux de données confondus) : 6
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`, `json`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 25/01/2019 à 11h57
- Date de dernière mise à jour le 04/10/2019 à 05h30

## HAUTS DE FRANCE

*Région*

### Source **Plateforme territoriale**
- URL : http://opendata.hautsdefrance.fr/dataset
- Volumétrie : 21 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff6aa3a7292c64a77d96
- Volumétrie : 52 jeux de données et 84 ressources
- Nombre de vues (tous jeux de données confondus) : 252
- Nombre de téléchargements (toutes ressources confondues) : 130
- Types de ressources disponibles : `json`, `shp`, `rtf`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence`
- Date de création : le 07/05/2014 à 03h37
- Date de dernière mise à jour le 05/10/2019 à 06h38

## HAUTS DE SEINE

*Département*

### Source **OpenDataSoft**
- URL : https://opendata.hauts-de-seine.fr/explore/?refine.publisher=D%C3%A9partement+des+Hauts-de-Seine
- Volumétrie : 132 jeux de données et 1013362 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 229941
- Types de licences utilisées : `Licence Ouverte (Etalab)`, `Licence d'utilisation des reproductions numériques du fonds des Archives de la Planète`
- Date de création : le 16/12/2012 à 23h00
- Date de dernière mise à jour le 04/10/2019 à 01h00


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/544e1b0888ee38289ae1ed05
- Volumétrie : 173 jeux de données et 627 ressources
- Nombre de vues (tous jeux de données confondus) : 23988
- Nombre de téléchargements (toutes ressources confondues) : 1751
- Types de ressources disponibles : `csv`, `json`, `shp`, `pdf`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`, `Licence Ouverte / Open Licence`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 06/04/2017 à 06h02
- Date de dernière mise à jour le 19/09/2019 à 11h26

## HAWA MAYOTTE

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data-hawa-mayotte.opendata.arcgis.com/datasets
- Volumétrie : 6 jeux de données

## IAU ILE DE FRANCE

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : http://data.iau-idf.fr/datasets
- Volumétrie : 114 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff7da3a7292c64a77e23/
- Volumétrie : 53 jeux de données et 159 ressources
- Nombre de vues (tous jeux de données confondus) : 765
- Nombre de téléchargements (toutes ressources confondues) : 770
- Types de ressources disponibles : `html`, `csv`, `json`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 07/05/2014 à 03h41
- Date de dernière mise à jour le 14/09/2013 à 19h42

## IFFENDIC

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b32535988ee380dd6e257de/
- Volumétrie : 6 jeux de données et 6 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 28/06/2018 à 10h45
- Date de dernière mise à jour le 23/09/2019 à 14h50

## ILE DE FRANCE

*Région*

### Source **OpenDataSoft**
- URL : https://data.iledefrance.fr/explore/?refine.publisher=R%C3%A9gion+%C3%8Ele-de-France
- Volumétrie : 284 jeux de données et 718246 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 533815
- Types de licences utilisées : `Licence ouverte`, `Licence Ouverte v2.0 (Etalab)`, `Licence Ouverte (Etalab)`, `Licence ouverte (Etalab)`, `CC BY-NC-ND`
- Date de création : le 30/05/2013 à 01h41
- Date de dernière mise à jour le 07/10/2019 à 04h00


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fffa8a3a7292c64a780c8
- Volumétrie : 1206 jeux de données et 6258 ressources
- Nombre de vues (tous jeux de données confondus) : 55514
- Nombre de téléchargements (toutes ressources confondues) : 25452
- Types de ressources disponibles : `csv`, `json`, `shp`, `pdf`, `zip`, `doc`, `xlb`, `docx`, `xlsx`, `obj`, `png`, `xls`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`, `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence`, `Other (Open)`
- Date de création : le 07/05/2014 à 03h37
- Date de dernière mise à jour le 05/10/2019 à 04h00

## ILE DE FRANCE MOBILITES

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://opendata.stif.info/explore/?refine.publisher=Ile-de-France+Mobilit%C3%A9s
- Volumétrie : 40 jeux de données et 4435558 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 728808
- Types de licences utilisées : `Licence Ouverte (Etalab)`, `Licence ODbL Version Française`, `CC BY-NC-ND 3.0 FR`, `Open Database License (ODbL)`, `CC BY-NC-ND 4.0`
- Date de création : le 08/07/2015 à 07h45
- Date de dernière mise à jour le 06/10/2019 à 15h00


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/568e5e9488ee38033aaf0bf4/
- Volumétrie : 49 jeux de données et 293 ressources
- Nombre de vues (tous jeux de données confondus) : 88
- Nombre de téléchargements (toutes ressources confondues) : 26
- Types de ressources disponibles : `csv`, `json`, `pdf`, `shp`, `zip`, `png`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 14/03/2018 à 06h58
- Date de dernière mise à jour le 04/10/2019 à 10h01

## INDRE

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a82f34188ee380c10423e48/
- Volumétrie : 6 jeux de données et 68 ressources
- Nombre de vues (tous jeux de données confondus) : 6
- Nombre de téléchargements (toutes ressources confondues) : 12
- Types de ressources disponibles : `csv`, `zip`, `pdf`, `d.i.pdf`, `06.2017.pdf`, `11.17.pdf`, `01.2008.pdf`, `05.2018.pdf`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 13/02/2018 à 16h06
- Date de dernière mise à jour le 20/08/2019 à 03h27

## INDRE ET LOIRE

*Département*

### Source **Plateforme territoriale**
- URL : https://data-cd37.opendata.arcgis.com/search
- Volumétrie : 4 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5733002788ee384aabd1b934/
- Volumétrie : 8 jeux de données et 50 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `web page`, `esri rest`, `geojson`, `kml`, `zip`, `ogc wms`, `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 05/05/2019 à 00h43
- Date de dernière mise à jour le 05/10/2019 à 02h14

## INFOCOM'94

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : https://data.infocom94.fr/portail
- Volumétrie : 17 jeux de données

## ISERE

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bb1c7e4634f412a95dff6b9/
- Volumétrie : 2 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `gtfsrt.pb`, `gtfsrt.json`, `neptune.zip`, `gtfs`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 01/10/2018 à 09h26
- Date de dernière mise à jour le 30/01/2019 à 10h16

## ISSY LES MOULINEAUX

*Commune*

### Source **OpenDataSoft**
- URL : https://data.issy.com/explore/?refine.publisher=Ville+d%27Issy-les-Moulineaux
- Volumétrie : 91 jeux de données et 51896 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 88284
- Types de licences utilisées : `Licence Ouverte (Etalab)`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 05/12/2014 à 17h03
- Date de dernière mise à jour le 07/10/2019 à 04h08


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fffb7a3a7292c64a78145
- Volumétrie : 288 jeux de données et 1408 ressources
- Nombre de vues (tous jeux de données confondus) : 1320
- Nombre de téléchargements (toutes ressources confondues) : 1408
- Types de ressources disponibles : `csv`, `json`, `shp`, `jpe`, `zip`, `xlb`, `pdf`, `csv`, ` json`, ` excel`, ` shp`, `xml`, `rss`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`, `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence`
- Date de création : le 07/05/2014 à 03h40
- Date de dernière mise à jour le 05/10/2019 à 05h05

## ISTRES

*Commune*

### Source **Plateforme territoriale**
- URL : https://www.istres.fr/index.php?id=618
- Volumétrie : 27 jeux de données

## JACOU

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/jacou
- Volumétrie : 27 jeux de données

## JUVIGNAC

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/juvignac
- Volumétrie : 30 jeux de données

## KEOLIS AGEN

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bfe67b8634f4121765f58f3/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 28/11/2018 à 15h01
- Date de dernière mise à jour le 28/11/2018 à 15h03

## KEOLIS BUS VERTS

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5d13756e8b4c415ad792a83e/
- Volumétrie : 1 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 26/06/2019 à 15h46
- Date de dernière mise à jour le 02/08/2019 à 05h08

## KEOLIS CAEN

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c09189f8b4c414a8d02298e/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 16
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 26/08/2019 à 17h26
- Date de dernière mise à jour le 10/09/2019 à 12h11

## KEOLIS CHATEAUROUX

*Délégataire de service public*

### Source **OpenDataSoft**
- URL : https://data.chateauroux-metropole.fr/explore/?sort=title&refine.publisher=KEOLIS
- Volumétrie : 2 jeux de données et 2 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 646
- Date de création : le 14/06/2019 à 15h57
- Date de dernière mise à jour le 07/08/2019 à 09h13

## KEOLIS CHAUMONT

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c5063ec8b4c412d63742601/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 29/01/2019 à 15h58
- Date de dernière mise à jour le 29/01/2019 à 15h58

## KEOLIS RENNES

*Délégataire de service public*

### Source **OpenDataSoft**
- URL : https://data.explore.star.fr/explore/?refine.publisher=STAR
- Volumétrie : 32 jeux de données et 37375 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 8728410
- Types de licences utilisées : `Open Database License (ODbL)`, `CC BY-ND`
- Date de création : le 14/11/2014 à 10h12
- Date de dernière mise à jour le 07/10/2019 à 04h24

## KICEO CTGMVA

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a6f24a4c751df613f36c422/
- Volumétrie : 2 jeux de données et 5 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 8
- Types de ressources disponibles : `gbfs`, `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 29/01/2018 à 15h10
- Date de dernière mise à jour le 19/09/2019 à 10h25

## LA BAULE ESCOUBLAC

*Commune*

### Source **OpenDataSoft**
- URL : https://capatlantique-loireatlantique.opendatasoft.com/explore/?refine.publisher=La+Baule-Escoublac
- Volumétrie : 2 jeux de données et 221 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 185
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 04/09/2018 à 07h48
- Date de dernière mise à jour le 04/10/2019 à 04h00

## LA CHAPELLE D'ARMENTIERES

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b518bb188ee3863608f3fac/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 20/07/2018 à 09h25
- Date de dernière mise à jour le 20/07/2018 à 09h25

## LA CHAPELLE DES MARAIS

*Commune*

### Source **OpenDataSoft**
- URL : https://data.agglo-carene.fr/explore/?refine.publisher=La+Chapelle+des+Marais
- Volumétrie : 2 jeux de données et 152 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 43
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 16/04/2019 à 15h11
- Date de dernière mise à jour le 06/10/2019 à 06h45

## LA GAUDE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ba9f0bb634f416762d56c0c/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 08/10/2018 à 14h56
- Date de dernière mise à jour le 12/10/2018 à 17h13

## LA NOUAYE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b3495d188ee382f97297bdc/
- Volumétrie : 4 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 28/06/2018 à 10h34
- Date de dernière mise à jour le 21/05/2019 à 11h34

## LA ROCHE SUR YON

*Commune*

### Source **OpenDataSoft**
- URL : https://data.larochesuryon.fr/explore/?refine.publisher=Ville+de+la+Roche-sur-Yon
- Volumétrie : 39 jeux de données et 15642 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 863
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 22/07/2016 à 00h00
- Date de dernière mise à jour le 05/10/2019 à 22h02

## LA ROCHELLE

*Commune*

### Source **Plateforme territoriale**
- URL : https://opendata.larochelle.fr/dataset/?metakey=dataset_property&metaval=140
- Volumétrie : 303 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5821930b88ee386862c65bb3/
- Volumétrie : 373 jeux de données et 1933 ressources
- Nombre de vues (tous jeux de données confondus) : 235
- Nombre de téléchargements (toutes ressources confondues) : 162
- Types de ressources disponibles : `csv`, `pdf`, `api`, `xlsx`, `xml`, `json`, `zip`, `kml`, `xls`, `geojson`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 05/10/2018 à 02h44
- Date de dernière mise à jour le 05/10/2019 à 02h22

## LA SEYNE SUR MER

*Commune*

### Source **Plateforme territoriale**
- URL : https://opendata.sictiam.fr/fr/organization/mairie-de-la-seyne-sur-mer
- Volumétrie : 1 jeux de données

## LA TREMBLADE 

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c8a2ec6634f411c5580a72b/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 14/03/2019 à 12h13
- Date de dernière mise à jour le 14/03/2019 à 12h13

## LA VICOMTE SUR RANCE

*Commune*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 2 jeux de données

## LANCIEUX

*Commune*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/donnees
- Volumétrie : 1 jeux de données

## LANESTER

*Commune*

### Source **OpenDataSoft**
- URL : https://www.opendata56.fr/explore/?refine.publisher=Ville+de+Lanester
- Volumétrie : 5 jeux de données et 7593 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 65
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 19/10/2018 à 07h35
- Date de dernière mise à jour le 07/06/2019 à 13h38


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59d6526ac751df274a175d35/
- Volumétrie : 2 jeux de données et 10 ressources
- Nombre de vues (tous jeux de données confondus) : 11
- Nombre de téléchargements (toutes ressources confondues) : 9
- Types de ressources disponibles : `html`, `odata`, `csv`, `xml`, `json`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 21/12/2017 à 14h30
- Date de dernière mise à jour le 29/12/2017 à 14h30

## LANNION

*Commune*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 7 jeux de données

## LATTES

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/lattes
- Volumétrie : 28 jeux de données

## LAVERUNE

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/lav%C3%A9rune
- Volumétrie : 32 jeux de données

## LE COUDRAY MONTCEAUX

*Commune*

### Source **OpenDataSoft**
- URL : https://data.coudray-montceaux.fr/explore/?refine.publisher=Mairie+du+Coudray-Montceaux
- Volumétrie : 7 jeux de données et 65 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 255
- Date de création : le 05/10/2018 à 09h33
- Date de dernière mise à jour le 08/10/2018 à 09h17

## LE CRES

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/le-cr%C3%A8s
- Volumétrie : 30 jeux de données

## LE CROISIC

*Commune*

### Source **OpenDataSoft**
- URL : https://capatlantique-loireatlantique.opendatasoft.com/explore/?refine.publisher=Le+Croisic
- Volumétrie : 1 jeux de données et 100 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 76
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 14/06/2019 à 04h00
- Date de dernière mise à jour le 04/10/2019 à 04h00

## LE HAVRE

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.agglo-lehavre.fr
- Volumétrie : 78 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a3a7cd8c751df3092aea45a/
- Volumétrie : 61 jeux de données et 305 ressources
- Nombre de vues (tous jeux de données confondus) : 78
- Nombre de téléchargements (toutes ressources confondues) : 34
- Types de ressources disponibles : `wfs`, `kmz`, `json`, `dxf`, `shp`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 26/07/2018 à 09h19
- Date de dernière mise à jour le 26/07/2018 à 09h19

## LE MANS

*Commune*

### Source **Plateforme territoriale**
- URL : http://www.lemans.fr/citoyen/la-collectivite/lopen-data/les-contenus-en-open-data/
- Volumétrie : 24 jeux de données

## LE MOTIF - OBSERVATOIRE DU LIVRE ET DE L'ECRIT

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://data.iledefrance.fr/explore/?refine.publisher=MOTif
- Volumétrie : 8 jeux de données et 4615 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 15242
- Types de licences utilisées : `ODbL`, `Licence ouverte`
- Date de création : le 18/03/2014 à 09h10
- Date de dernière mise à jour le 16/03/2019 à 17h45

## LE PASSAGE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ab24b9188ee385d43b29616/
- Volumétrie : 1 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `pdf`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 21/03/2018 à 13h19
- Date de dernière mise à jour le 21/03/2018 à 14h00

## LE PELLERIN

*Commune*

### Source **OpenDataSoft**
- URL : https://data.nantesmetropole.fr/explore/?refine.publisher=Le+Pellerin
- Volumétrie : 4 jeux de données et 2183 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1381
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 27/03/2017 à 00h00
- Date de dernière mise à jour le 01/07/2019 à 09h38

## LE PERREUX SUR MARNE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff85a3a7292c64a77ea9
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 6
- Nombre de téléchargements (toutes ressources confondues) : 19
- Types de ressources disponibles : `ods`, `xls`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 07/05/2014 à 03h47
- Date de dernière mise à jour le 07/01/2014 à 14h54

## LE POULIGUEN

*Commune*

### Source **OpenDataSoft**
- URL : https://capatlantique-loireatlantique.opendatasoft.com/explore/?refine.publisher=Le+Pouliguen
- Volumétrie : 1 jeux de données et 64 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 78
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 11/06/2019 à 13h16
- Date de dernière mise à jour le 04/10/2019 à 04h00

## LE PRADET

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/58f5d64788ee3847b91e4f32
- Volumétrie : 1 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xlsx`, `ods`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 18/04/2017 à 11h36
- Date de dernière mise à jour le 18/04/2017 à 11h36

## LES LILAS

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff86a3a7292c64a77eab
- Volumétrie : 71 jeux de données et 1767 ressources
- Nombre de vues (tous jeux de données confondus) : 1779
- Nombre de téléchargements (toutes ressources confondues) : 586
- Types de ressources disponibles : `pdf`, `01-au-1.03.pdf`, `02-au-18.02.pdf`, `02.pdf`, `7.19.pdf`, `-marechal-juin-30-et-31.7.19-1-.pdf`, `8.19.pdf`, `7-au-2.8.19.pdf`, `02-au-6.03.pdf`, `02-au-18.03.pdf`, `-17.02-.pdf`, `02-au-30.04.pdf`, `02-.pdf`, `02-au-15.03.pdf`, `02-au-06.02.pdf`, `-rolland-15.2.19.pdf`, `-demanagement-rue-romain-rolland-15.02.pdf`, `01-au-8.2.19.pdf`, `01.pdf`, `02-au-15.02.pdf`, `01-au-01.02-.pdf`, `02-au-6.03-.pdf`, `01-au-08.02.pdf`, `02-au-29.02.pdf`, `01-au-8.03-.pdf`, `01-au-3.05.pdf`, `01-.pdf`, `000-memoire-vive-19.01-.pdf`, `01-au-25.01-.pdf`, `csv`, `xls`, `txt`, `12-au-18.01.19-.pdf`, `-salez-22-au-27.7.19.pdf`, `-barbusse-29.7-au-9.8.19.pdf`, `-w.rousseau-2.7-au-30.8.19-deschamps.pdf`, `-de-kock-2.7-au-30.9.19-deschamps.pdf`, `5-au-15.7.19.pdf`, `6.19.pdf`, `19.pdf`, `6-au-31.7.19.pdf`, `5-au-7.6.19.pdf`, `519-au-16.5.20.pdf`, `5.19.pdf`, `5-au-20.11.2019.pdf`, `1-au-14.6.19.pdf`, `-mal-lattre-de-tassigny-29.4-au-29.5.19.pdf`, `4-au-23.7.19.pdf`, `3-au-12.4.19.pdf`, `3-au-5.4.19-.pdf`, `3.19.pdf`, `3-au-5.4.19.pdf`, `2-au-5.8.19.pdf`, `-rolland-4-au-20.3.19.pdf`, `-poulmarch-4-au-20.3.19.pdf`, `2-au-11.3.19.pdf`, `12.18-au-21.2.19.pdf`, `01-au-01.03.pdf`, `-langevin-25.2-au-7.3.19.pdf`, `02-au-5.04.pdf`, `02-au-8.03.pdf`, `02-au-18.04.pdf`, `17.pdf`, `6-au-31.12.19.pdf`, `01-au-31.12.pdf`, `-de-kock.pdf`, `.pdf`, `-meslin-1er-adjoint.pdf`, `benferroudj.pdf`, `-tarifs-activites-peri-et-extra-scolaires-2019-2020.pdf`, `-reglement-interieur-activites-peri-et-extra-scolaires-2019-2020.pdf`, `-convention-organisation-etudes-surveillees.pdf`, `-convention-occupation-et-dusage-le-rucher-des-lilas.pdf`, `-convention-organisation-pause-meridienne.pdf`, `-lancement-procedure-aoo-location-vehicules-transport-en-commun.pdf`, `-allongement-convention-reservation-ssdh-sentes.pdf`, `-document-cadre-dorientations-logement-eptee.pdf`, `-bilan-politique-fonciere-2018.pdf`, `-tarifs-services-culturels.pdf`, `-demande-agrement-clas-caf.pdf`, `-fixation-divers-tarifs-municipaux.pdf`, `-designation-representant-cig-conseil-de-discipline.pdf`, `-reiteration-garantie-demprunt-ratp-habitat-logis-transport.pdf`, `-creation-2-emplois-non-permanents-saisonniers-espaces-verts.pdf`, `-actualisation-tableau-des-effectifs.pdf`, `-reiteration-garantie-demprunt-ssdh.pdf`, `-convention-adhesion-payfip-titre.pdf`, `-convention-depenses-recettes-eptee-2019.pdf`, `-convention-mise-a-dispo-services-eptee-2019.pdf`, `-designation-delegue-comite-local-pld-eptee.pdf`, `-designation-delegue-syncom.pdf`, `-designation-delegue-sequano-amenagement.pdf`, `-designation-suppleant-mve.pdf`, `-designation-representants-conseils-decoles.pdf`, `-designation-representantion-ccspl.pdf`, `-designation-representants-cao-com-ad-hoc-ccdsp.pdf`, `-designation-membres-commissions-municipales-1-et-4.pdf`, `-election-conseiller-de-territoire-eptee.pdf`, `-election-1er-adjoint.pdf`, `-convention-syctom.pdf`, `-tarifs-sejours-ete-2019.pdf`, `-convention-electrons-solaires-toiture-w.-rousseau.pdf`, `csu-fipd-definition-montant-previsionnel.pdf`, `-tgc-renouvellement-licences-spectacles.pdf`, `-concession-dsp-approbation-choix-ent.pdf`, `-subvention-centre-monuments-nationaux-cathedrale.pdf`, `-convention-moi-jeune-citoyen.pdf`, `compte-gestion-bp-2018.pdf`, `-lancement-ao-fourniture-materiel-pour-les-ateliers.pdf`, `-lancement-ao-marche-anti-intrusion-et-telesurveillance.pdf`, `-lancement-ao-marche-securite-incendie.pdf`, `-immo-mdb.pdf`, `-convention-caf-ave-aide-aux-vacances-enfants.pdf`, `-convention-caf-accueil-enfant-handicapes.pdf`, `-avenant-convention-alsh.pdf`, `-avenant-convention-charte-qualite-plan-mercredi.pdf`, `-rousseau-et.pdf`, `-convention-reservation-logt-16-18-et-26-30-rue-bruyeres-france-habitation.pdf`, `-convention-reservation-logt-46-rue-paris-france-habitation.pdf`, `-convention-reseration-logt-40-rue-paris-france-habitation.pdf`, `-convention-reservation-logt-rochefoucauld-france-habitation.pdf`, `-convention-smac-le-triton-2019.pdf`, `-convention-colline-bleue-2019-a-2021.pdf`, `-convention-clas-avec-caf.pdf`, `-convention-projets-ete-avec-caf-sejours-ete-2018.pdf`, `pasteur..pdf`, `-dassurances..pdf`, `contentieux-sci-fromont..pdf`, `-energie-partagee.pdf`, `-avenant-bail-a-construction-logis-transports.pdf`, `-convention-reservation-osica-12-bernard.pdf`, `-avenant-socotec.pdf`, `-engagement-sda-adap-points-darret.pdf`, `-dp-facade-sur-rue-centre-loisirs.pdf`, `-dp-menuiseries-cms.pdf`, `-pc-exutoires-tgc.pdf`, `-dp-menuiseries-r.-rolland.pdf`, `-subvention-sipperec-eclairage-gymnase-j.-jaures.pdf`, `-subvention-sipperec-menuiseries-r.-rolland.pdf`, `docx`, `-convention-mandat-plu.pdf`, `-clect-metropole.pdf`, `-designation-membres-pour-commissions-municipales-suite-a-demissio.pdf`, `-designation-commission-consultative-services-publics-locaux.pdf`, `constitutive-g.de-commandes.pdf`, `-dun-accompte-ogec-notre-dame.pdf`, `locale-de-la-lyr.pdf`, `publics.pdf`, `plh.pdf`, `eitbf.pdf`, `-le-triton.pdf`, `sfil.pdf`, `-contrat-cdc.pdf`, `conv-est.e-ligne-11.pdf`, `-2016-u.m.l.pdf`, `travaux-dangereux.pdf`, `-sifurep-statuts.pdf`, `-vente-20-24-bruyeres-masseron.pdf`, `-adoption-schema-tvb.pdf`, `650eur.pdf`, `906eur.pdf`, `256.008eur.pdf`, `es-ecvf.pdf`, `-copie.pdf`, `-parcelle-i3-adhesion-quittance.pdf`, `-dp-creation-apport-dair-laverie-cui.pdf`, `-convention-relogement-ept-copie.pdf`, `-61bis-65-av.-pasteur-cession-droits-a-construire-sci-4g.pdf`, `-convention-reservation-france-hab.-42-bruyeres.pdf`, `-3-anglemont-cession-a-vilogia.pdf`, `-revalorisation-et-fixation-de-divers-tarifs-municipaux..pdf`, `-sequano-acquisition-actions-les-lilas.pdf`, `374-euros.pdf`, `107-euros.pdf`, `312euros.pdf`, `-groupement-commande-sigeif-sdesm-sey78.pdf`, `-dp-rehabilitation-vestiaires-stade.pdf`, `-dp-rehabilitation-jean-poulmarch.pdf`, `-convention-potager-des-lilas.pdf`, `-agis-t-jeune-novembre-2018.pdf`, `-convention-france-mediation-3-.pdf`, `-marches-communaux-fixation-droits-de-place.pdf`, `-ouverture-de-cre-dits.pdf`, `-rapport-clect-mgp.pdf`, `-participation-familiale-qf-classes-decouv.-et-sejours-thematiques.pdf`, `-avenant-1-epfif.pdf`, `-representants-comite-suivi-geothermie.pdf`, `-representants-mve.pdf`, `-renouvellement-adhesion-mve.pdf`, `-approbation-revision-plu.pdf`, `-resiliation-bail-commercial.pdf`, `-acquisition-fonds-de-commerce.pdf`, `-modif-reglement-lilart.pdf`, `-developpement-videoprotection.pdf`, `-kiosque-resto-du-coeur-agis-t-jeune.pdf`, `-kiosque-rucher-des-lilas-agis-t-jeune.pdf`, `-kiosque-courgette-solidaire-agis-t-jeune.pdf`, `-solde-subvention-ogec.pdf`, `-kiosque-armee-du-salut-agis-t-jeune-et-acte.pdf`, `-convention-eptee-dispositif-iti.pdf`, `-convention-mediakiosk.pdf`, `-approbation-lancement-dsp.pdf`, `-tarifs-stages-photos-j.-cocteau.pdf`, `-conv-romainville-saint-germain.pdf`, `-convention-eup.pdf`, `-avis-pmhh.pdf`, `-principe-de-creation-dun-perimetre-de-sauvegarde.pdf`, `-adhesion-sippnco.pdf`, `-convention-fse-2017-2020.pdf`, `-conv-caf-mediation-sociale-partagee.pdf`, `-autorisation-consultation-ccspl.pdf`, `-rapport-annuel-semaco-2017-dsp-marche.pdf`, `-dm2-bp-ville.pdf`, `-convention-tripartite-compostage-ville-ssdh-eptee.pdf`, `-convention-villes-ept-sipperec.pdf`, `-benevolat-juin-2018.pdf`, `-permis-de-vegetaliser-aodp.pdf`, `-convention-potager-liberte-mad-parc-s.-veil.pdf`, `-designation-representant-mve-suite-modification-de-statuts.pdf`, `-modification-statuts-mve.pdf`, `-agis-t-jeune-mai-2018.pdf`, `pass-jeunes-2018.pdf`, `-fixation-nombre-representants-chsct.pdf`, `-fixation-nombre-representants-ct.pdf`, `-lancement-ao-marche-securite-incendie-2019-2023.pdf`, `-lancement-ao-marche-chauffage-ventilation-climatisation-2018-2022.pdf`, `-acam-ssi-liberte.pdf`, `2019.csv`, `2019.pdf`, `jpg`, `xml`, `-dsucs-et-fsrif-2018-note.csv`, `-dsucs-et-fsrif-2018-note.pdf`, `02.2019-2.pdf`, `html`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `License Not Specified`, `Licence Ouverte / Open Licence`, `Open Data Commons Public Domain Dedication and Licence (PDDL)`
- Date de création : le 07/05/2014 à 03h45
- Date de dernière mise à jour le 23/07/2019 à 15h10

## LIEUSAINT

*Commune*

### Source **OpenDataSoft**
- URL : https://data.ville-lieusaint.fr/explore/?sort=title&refine.publisher=Mairie+de+Lieusaint
- Volumétrie : 2 jeux de données et 68 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 8
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 27/09/2018 à 10h21
- Date de dernière mise à jour le 16/01/2019 à 15h12

## LIG'AIR

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data-ligair.opendata.arcgis.com/datasets
- Volumétrie : 66 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5be949a9634f415deb46af0e/
- Volumétrie : 117 jeux de données et 470 ressources
- Nombre de vues (tous jeux de données confondus) : 10
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `esri rest`, `web page`, `zip`, `csv`, `kml`, `geojson`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 18/11/2018 à 23h49
- Date de dernière mise à jour le 05/10/2019 à 02h12

## LILA PRESQU'ILE

*Autre groupement de collectivités territoriales*

### Source **OpenDataSoft**
- URL : https://capatlantique-loireatlantique.opendatasoft.com/explore/?refine.publisher=Lila+Presqu%27ile
- Volumétrie : 3 jeux de données et 707 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 163
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 10/09/2019 à 14h07
- Date de dernière mise à jour le 10/09/2019 à 14h09

## LILLE

*Commune*

### Source **OpenDataSoft**
- URL : https://opendata.lillemetropole.fr/explore/?refine.publisher=Ville+de+Lille
- Volumétrie : 3 jeux de données et 2137 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 4781
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 06/11/2017 à 16h06
- Date de dernière mise à jour le 30/09/2019 à 13h42

## LIMOGES

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/54acf7fcc751df4851de6536
- Volumétrie : 5 jeux de données et 5 ressources
- Nombre de vues (tous jeux de données confondus) : 36
- Nombre de téléchargements (toutes ressources confondues) : 88
- Types de ressources disponibles : `zip`, `ods`
- Types de licences utilisées : `License Not Specified`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 13/01/2015 à 15h31
- Date de dernière mise à jour le 09/08/2018 à 17h46

## LISIEUX

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/57835411c751df7f2ef3855e
- Volumétrie : 13 jeux de données et 33 ressources
- Nombre de vues (tous jeux de données confondus) : 11
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `pdf`, `xml`, `xlsx`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 01/02/2019 à 10h31
- Date de dernière mise à jour le 06/09/2019 à 16h36

## LOIR ET CHER

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff68a3a7292c64a77d8a
- Volumétrie : 70 jeux de données et 70 ressources
- Nombre de vues (tous jeux de données confondus) : 226
- Nombre de téléchargements (toutes ressources confondues) : 818
- Types de ressources disponibles : `csv`, `shp`, `xls`, `json`, `autre`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 07/05/2014 à 03h38
- Date de dernière mise à jour le 22/01/2015 à 16h29

## LOIRE ATLANTIQUE

*Département*

### Source **OpenDataSoft**
- URL : https://data.loire-atlantique.fr/explore/?refine.publisher=Département+de+Loire-Atlantique
- Volumétrie : 278 jeux de données et 876732 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 73500
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 13/01/2009 à 00h00
- Date de dernière mise à jour le 07/10/2019 à 04h35


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff6fa3a7292c64a77dc1/
- Volumétrie : 284 jeux de données et 961 ressources
- Nombre de vues (tous jeux de données confondus) : 58
- Nombre de téléchargements (toutes ressources confondues) : 6
- Types de ressources disponibles : `csv`, `json`, `shp`, `zip`, `pdf`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`
- Date de création : le 31/07/2018 à 15h25
- Date de dernière mise à jour le 01/10/2019 à 09h58

## LOIRE ATLANTIQUE DEVELOPPEMENT

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://data.loire-atlantique.fr/explore/?refine.publisher=Loire-Atlantique+Développement
- Volumétrie : 17 jeux de données et 606 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 2415
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 31/10/2012 à 00h00
- Date de dernière mise à jour le 29/05/2018 à 16h50


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff87a3a7292c64a77eb0/
- Volumétrie : 20 jeux de données et 80 ressources
- Nombre de vues (tous jeux de données confondus) : 46
- Nombre de téléchargements (toutes ressources confondues) : 266
- Types de ressources disponibles : `json`, `csv`, `xml`, `xls`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 07/05/2014 à 03h37
- Date de dernière mise à jour le 10/12/2013 à 14h47

## LOIRE ATLANTIQUE TOURISME

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://data.loire-atlantique.fr/explore/?refine.publisher=e-SPRIT
- Volumétrie : 26 jeux de données et 40189 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 8868
- Types de licences utilisées : `Open Database License (ODbL)`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 16/11/2016 à 00h00
- Date de dernière mise à jour le 07/10/2019 à 01h12

## LOIRET

*Département*

### Source **Plateforme territoriale**
- URL : https://open-loiret.opendata.arcgis.com/datasets?source=Conseil%20d%C3%A9partemental%20du%20Loiret&t=donn%C3%A9es%20ouvertes
- Volumétrie : 43 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/57d90ef4c751df44fb79df72
- Volumétrie : 182 jeux de données et 773 ressources
- Nombre de vues (tous jeux de données confondus) : 147
- Nombre de téléchargements (toutes ressources confondues) : 21
- Types de ressources disponibles : `csv`, `esri rest`, `web page`, `zip`, `kml`, `geojson`, `.json`, `json`, `shp`, `xls`, `document`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`, `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 17/09/2018 à 08h47
- Date de dernière mise à jour le 31/08/2019 à 06h41

## LONGJUMEAU

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff88a3a7292c64a77eb2
- Volumétrie : 16 jeux de données et 14 ressources
- Nombre de vues (tous jeux de données confondus) : 11
- Nombre de téléchargements (toutes ressources confondues) : 76
- Types de ressources disponibles : `csv`, `rtf`, `autre`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 07/05/2014 à 03h49
- Date de dernière mise à jour le 04/01/2019 à 09h02

## LORIENT

*Commune*

### Source **OpenDataSoft**
- URL : https://www.opendata56.fr/explore/?disjunctive.publisher&sort=modified&refine.publisher=Ville+de+Lorient
- Volumétrie : 15 jeux de données et 74547 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 6812
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 28/01/2019 à 13h39
- Date de dernière mise à jour le 01/10/2019 à 06h02


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59e05c6588ee386ef4f4f86d/
- Volumétrie : 17 jeux de données et 60 ressources
- Nombre de vues (tous jeux de données confondus) : 70
- Nombre de téléchargements (toutes ressources confondues) : 8
- Types de ressources disponibles : `csv`, `json`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`
- Date de création : le 16/02/2019 à 16h41
- Date de dernière mise à jour le 01/10/2019 à 06h02

## LOT

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ac5c34488ee386e5ad2bb40/
- Volumétrie : 8 jeux de données et 7 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 12/04/2018 à 16h05
- Date de dernière mise à jour le 23/08/2019 à 04h45

## LOUVIERS

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/53806fe2a3a7297e4d35d6c6
- Volumétrie : 3 jeux de données et 8 ressources
- Nombre de vues (tous jeux de données confondus) : 120
- Nombre de téléchargements (toutes ressources confondues) : 12
- Types de ressources disponibles : `html`, `xls`, `csv`, `json`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 24/05/2014 à 13h08
- Date de dernière mise à jour le 24/05/2014 à 13h55

## LYON

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.beta.grandlyon.com/fr/recherche
- Volumétrie : 13 jeux de données

## MADININAIR

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data-madininair.opendata.arcgis.com/datasets
- Volumétrie : 31 jeux de données

## MAHINA

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/552d6a09c751df1ed737dddb
- Volumétrie : 2 jeux de données et 22 ressources
- Nombre de vues (tous jeux de données confondus) : 13
- Nombre de téléchargements (toutes ressources confondues) : 5
- Types de ressources disponibles : `kml`, `geojson`, `csv`, `pdf`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 23/07/2015 à 23h35
- Date de dernière mise à jour le 25/07/2015 à 05h43

## MAINE ET LOIRE

*Département*

### Source **Plateforme territoriale**
- URL : https://www.opendata49.fr/index.php?id=38
- Volumétrie : 79 jeux de données

## MAISON DE L'ESTUAIRE DE LA SEINE

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data.maisondelestuaire.org
- Volumétrie : 19 jeux de données

## MANCHE

*Département*

### Source **Plateforme territoriale**
- URL : https://www.opendata-manche.fr/web/guest/donnees
- Volumétrie : 46 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff67a3a7292c64a77d87
- Volumétrie : 35 jeux de données et 69 ressources
- Nombre de vues (tous jeux de données confondus) : 146
- Nombre de téléchargements (toutes ressources confondues) : 74
- Types de ressources disponibles : `html`, `odata`, `fr/tjeune/`, `pdf`, `fr/accueil`, `aspx#recherche`, `fr`, `csv`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 07/05/2014 à 03h43
- Date de dernière mise à jour le 22/07/2016 à 11h22

## MANCHE NUMERIQUE

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : https://www.opendata-manche.fr/web/guest/donnees
- Volumétrie : 10 jeux de données

## MARNES LA COQUETTE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/55094e4cc751df601a882846
- Volumétrie : 9 jeux de données et 9 ressources
- Nombre de vues (tous jeux de données confondus) : 11
- Nombre de téléchargements (toutes ressources confondues) : 15
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 18/03/2015 à 11h10
- Date de dernière mise à jour le 18/03/2015 à 11h22

## MARSEILLE

*Commune*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/ville-de-marseille
- Volumétrie : 30 jeux de données

## MARTIGUES

*Commune*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/ville-de-martigues
- Volumétrie : 9 jeux de données

## MAUGES COMMUNAUTE

*Communauté d'agglomération*

### Source **OpenDataSoft**
- URL : https://data.paysdelaloire.fr/explore/?disjunctive.publisher&sort=modified&refine.gestionnaire=Mauges+Communauté+
- Volumétrie : aucun jeu de données

## MAYENNE

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5aeab860c751df3bca98b623/
- Volumétrie : 293 jeux de données et 853 ressources
- Nombre de vues (tous jeux de données confondus) : 35
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `gouv-cd53-20-05-2019.xlsx`, `document`, `json`, `shp`, `gouv-cd53-21-12-2018.xlsx`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 23/07/2018 à 10h10
- Date de dernière mise à jour le 20/05/2019 à 14h21

## MAZINGARBE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bb345d18b4c414c173fc6a5/
- Volumétrie : 8 jeux de données et 10 ressources
- Nombre de vues (tous jeux de données confondus) : 6
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `pdf`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 09/10/2018 à 10h25
- Date de dernière mise à jour le 26/10/2018 à 13h07

## MENTON

*Commune*

### Source **Plateforme territoriale**
- URL : https://opendata.sictiam.fr/fr/organization/commune-de-menton
- Volumétrie : 1 jeux de données

## METROPOLE AIX MARSEILLE PROVENCE

*Métropole*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/metropole-daix-marseille-provence
- Volumétrie : 17 jeux de données

## METROPOLE DE LYON

*Métropole*

### Source **Plateforme territoriale**
- URL : https://data.beta.grandlyon.com/fr/recherche
- Volumétrie : 559 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff7aa3a7292c64a77e0c
- Volumétrie : 767 jeux de données et 2043 ressources
- Nombre de vues (tous jeux de données confondus) : 746
- Nombre de téléchargements (toutes ressources confondues) : 51
- Types de ressources disponibles : `csv`, `document`, `ecw`, `json`, `shp`, `zip`, `xls`, `image`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 17/04/2019 à 06h47
- Date de dernière mise à jour le 08/09/2019 à 00h40

## METROPOLE DU GRAND NANCY

*Métropole*

### Source **Plateforme territoriale**
- URL : http://opendata.grandnancy.eu/jeux-de-donnees/rechercher-un-jeu-de-donnees/
- Volumétrie : 48 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff65a3a7292c64a77d73
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 6
- Nombre de téléchargements (toutes ressources confondues) : 5
- Types de ressources disponibles : `gtfs`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 14/03/2018 à 17h13
- Date de dernière mise à jour le 11/09/2019 à 04h58

## METROPOLE EUROPEENNE DE LILLE

*Métropole*

### Source **OpenDataSoft**
- URL : https://opendata.lillemetropole.fr/explore/?refine.publisher=MEL
- Volumétrie : 153 jeux de données et 4129172 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 3147600
- Types de licences utilisées : `Licence Ouverte (Etalab)`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 20/06/2016 à 09h06
- Date de dernière mise à jour le 07/10/2019 à 04h28


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/58b69c62c751df4b175dd7a4
- Volumétrie : 289 jeux de données et 1601 ressources
- Nombre de vues (tous jeux de données confondus) : 7017
- Nombre de téléchargements (toutes ressources confondues) : 904
- Types de ressources disponibles : `csv`, `json`, `shp`, `doc`, `pdf`, `zip`, `jpe`, `png`, `xlb`, `xls`, `xlsx`, `docx`, `bmp`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`, `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 03/03/2017 à 05h00
- Date de dernière mise à jour le 27/09/2019 à 13h48

## METROPOLE NICE COTE D'AZUR

*Métropole*

### Source **Plateforme territoriale**
- URL : http://opendata.nicecotedazur.org/data/organization/metropole-nice-cote-d-azur
- Volumétrie : 92 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59aea226c751df38f3f6834b/
- Volumétrie : 187 jeux de données et 1737 ressources
- Nombre de vues (tous jeux de données confondus) : 229
- Nombre de téléchargements (toutes ressources confondues) : 64
- Types de ressources disponibles : `geojson`, `csv`, `json`, `kmz`, `zip`, `shp`, `pdf`, `cpg`, `dbf`, `prj`, `sbn`, `sbx`, `xml`, `dwg`, `ods`, `xls`, `xlsx`, `jpg`, `ecw`, `gpx`, `docx`, `odt`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 05/09/2017 à 15h32
- Date de dernière mise à jour le 05/10/2019 à 05h01

## METZ METROPOLE

*Métropole*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/53fd1263a3a729390ba568a4
- Volumétrie : 54 jeux de données et 102 ressources
- Nombre de vues (tous jeux de données confondus) : 3094
- Nombre de téléchargements (toutes ressources confondues) : 2207
- Types de ressources disponibles : `gtfs`, `shape`, `json`, `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence`, `Other (Attribution)`
- Date de création : le 29/09/2014 à 11h48
- Date de dernière mise à jour le 14/11/2018 à 15h01

## MEUDON

*Commune*

### Source **OpenDataSoft**
- URL : https://data.meudon.fr/explore/?sort=title&refine.publisher=Ville+de+Meudon
- Volumétrie : 11 jeux de données et 760 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 260
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 03/04/2019 à 14h10
- Date de dernière mise à jour le 21/05/2019 à 10h29


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/54d89442c751df1245467389
- Volumétrie : 190 jeux de données et 185 ressources
- Nombre de vues (tous jeux de données confondus) : 749
- Nombre de téléchargements (toutes ressources confondues) : 273
- Types de ressources disponibles : `csv`, `xlsx`, `pdf`, `xls`, `tva_section_dinvestissement.csv`, `tva_section_de_fonctionnement.csv`, `_publiques_locales.csv`, `kmz`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `License Not Specified`
- Date de création : le 09/02/2015 à 12h17
- Date de dernière mise à jour le 20/11/2017 à 18h07

## MEURTHE ET MOSELLE

*Département*

### Source **Plateforme territoriale**
- URL : http://catalogue.infogeo54.fr/geonetwork/srv/fre/catalog.search#/home
- Volumétrie : 15 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bc74634634f4144ed36741c/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 4
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 21/10/2018 à 21h41
- Date de dernière mise à jour le 21/10/2018 à 21h42

## MEZE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5be01ff4634f4154a89ac27d/
- Volumétrie : 4 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`, `-a-23k.csv`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 09/11/2018 à 15h53
- Date de dernière mise à jour le 08/08/2019 à 17h06

## MOGNENEINS

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5662c125c751df42e605bd61
- Volumétrie : 5 jeux de données et 5 ressources
- Nombre de vues (tous jeux de données confondus) : 5
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 31/01/2017 à 20h43
- Date de dernière mise à jour le 06/08/2019 à 11h10

## MONACIA D'AULLENE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/53ae7fc3a3a729709f56d4f4
- Volumétrie : 12 jeux de données et 68 ressources
- Nombre de vues (tous jeux de données confondus) : 88
- Nombre de téléchargements (toutes ressources confondues) : 132
- Types de ressources disponibles : `xls`, `zip`, `pdf`, `csv`, `kmz`, `csv`, ` api`, `kml`, `gtfs`, `shp (wgs84)`, `geojson`, `osm`, `xml`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `Creative Commons CCZero`
- Date de création : le 28/06/2014 à 10h49
- Date de dernière mise à jour le 19/04/2017 à 17h15

## MONTAUD

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/montaud
- Volumétrie : 14 jeux de données

## MONTBELIARD

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c00089a8b4c414c75aed250/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 14/01/2019 à 14h41
- Date de dernière mise à jour le 09/04/2019 à 15h05

## MONTENDRE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c8a2ea6634f411541dbc634/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 14/03/2019 à 12h17
- Date de dernière mise à jour le 14/03/2019 à 12h17

## MONTFERRIER SUR LEZ

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/montferrier-sur-lez
- Volumétrie : 24 jeux de données

## MONTOIR DE BRETAGNE

*Commune*

### Source **OpenDataSoft**
- URL : https://data.agglo-carene.fr/explore/?refine.publisher=Montoir+de+Bretagne
- Volumétrie : 2 jeux de données et 265 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 236
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 16/07/2018 à 08h22
- Date de dernière mise à jour le 06/10/2019 à 13h00

## MONTPELLIER

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/montpellier
- Volumétrie : 109 jeux de données

## MONTPELLIER MEDITERRANEE METROPOLE

*Métropole*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/montpellier-mediterran%C3%A9e-m%C3%A9tropole
- Volumétrie : 129 jeux de données

## MONTREUIL

*Commune*

### Source **OpenDataSoft**
- URL : https://data.montreuil.fr/explore/?refine.publisher=Ville+de+Montreuil
- Volumétrie : 68 jeux de données et 481808 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 686
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`, `Open Database License (ODbL)`
- Date de création : le 25/04/2019 à 18h55
- Date de dernière mise à jour le 07/10/2019 à 04h06

## MORBIHAN

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c924c1c8b4c41159d78d937/
- Volumétrie : 17 jeux de données et 18 ressources
- Nombre de vues (tous jeux de données confondus) : 15
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`, `txt`, `shape`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 03/04/2019 à 13h56
- Date de dernière mise à jour le 13/09/2019 à 16h50

## MORBIHAN ENERGIES

*Autre groupement de collectivités territoriales*

### Source **OpenDataSoft**
- URL : https://www.opendata56.fr/explore/?refine.publisher=Morbihan+%C3%A9nergies
- Volumétrie : 6 jeux de données et 1357 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 2626
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 19/10/2018 à 08h15
- Date de dernière mise à jour le 23/07/2019 à 09h41


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/551d4b6cc751df4f1a0cd89f/
- Volumétrie : 7 jeux de données et 28 ressources
- Nombre de vues (tous jeux de données confondus) : 27
- Nombre de téléchargements (toutes ressources confondues) : 9
- Types de ressources disponibles : `csv`, `json`, `shp`, `html`, `odata`, `xml`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 23/06/2017 à 16h19
- Date de dernière mise à jour le 10/04/2019 à 15h38

## MORBIHAN TOURISME

*Organisme associé de collectivité territoriale*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/56aa3aac88ee387ea98ffbc6/
- Volumétrie : 5 jeux de données et 8 ressources
- Nombre de vues (tous jeux de données confondus) : 6
- Nombre de téléchargements (toutes ressources confondues) : 17
- Types de ressources disponibles : `html`, `odata`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence`
- Date de création : le 29/04/2016 à 10h40
- Date de dernière mise à jour le 29/04/2016 à 10h45

## MOSELLE

*Département*

### Source **Plateforme territoriale**
- URL : http://www.moselleopendata.fr/datas
- Volumétrie : 20 jeux de données

## MULHOUSE

*Commune*

### Source **OpenDataSoft**
- URL : https://data.mulhouse-alsace.fr/explore/?refine.publisher=Ville+de+Mulhouse
- Volumétrie : 41 jeux de données et 168278 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 10673
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`, `Licence Ouverte (Etalab)`
- Date de création : le 07/07/2017 à 09h02
- Date de dernière mise à jour le 12/09/2019 à 21h49

## MURVIEL LES MONTPELLIER

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/murviel-les-montpellier
- Volumétrie : 19 jeux de données

## MUSIQUE ET DANSE EN LOIRE ATLANTIQUE

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://data.loire-atlantique.fr/explore/?refine.publisher=Musique+et+Danse+en+Loire-Atlantique
- Volumétrie : 5 jeux de données et 413 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 542
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 04/12/2012 à 00h00
- Date de dernière mise à jour le 29/05/2018 à 21h52


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff98a3a7292c64a78011/
- Volumétrie : 5 jeux de données et 20 ressources
- Nombre de vues (tous jeux de données confondus) : 18
- Nombre de téléchargements (toutes ressources confondues) : 231
- Types de ressources disponibles : `json`, `csv`, `xml`, `xls`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 07/05/2014 à 03h42
- Date de dernière mise à jour le 10/12/2013 à 14h47

## NANCY

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a210bc888ee382bed1f31db/
- Volumétrie : 5 jeux de données et 10 ressources
- Nombre de vues (tous jeux de données confondus) : 11
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 18/05/2018 à 09h35
- Date de dernière mise à jour le 04/10/2019 à 08h31

## NANDY

*Commune*

### Source **OpenDataSoft**
- URL : https://nandy-grandparissud.opendatasoft.com/explore/?refine.publisher=Mairie+de+Nandy
- Volumétrie : 1 jeux de données et 1 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 36
- Date de création : le 10/10/2018 à 14h02
- Date de dernière mise à jour le 16/10/2018 à 14h12

## NANTERRE

*Commune*

### Source **Plateforme territoriale**
- URL : http://www.nanterre.fr/1166-donnees-a-telecharger.htm
- Volumétrie : 7 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/58ecb30388ee3855493567c6/
- Volumétrie : 8 jeux de données et 18 ressources
- Nombre de vues (tous jeux de données confondus) : 11
- Nombre de téléchargements (toutes ressources confondues) : 13
- Types de ressources disponibles : `zip`, `csv`, `pdf`, `png`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 18/04/2018 à 17h03
- Date de dernière mise à jour le 02/05/2019 à 11h55

## NANTES

*Commune*

### Source **OpenDataSoft**
- URL : https://data.nantesmetropole.fr/explore/?refine.publisher=Ville+de+Nantes
- Volumétrie : 140 jeux de données et 154678 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 38221
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 13/12/2012 à 00h00
- Date de dernière mise à jour le 04/10/2019 à 14h51


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fffb9a3a7292c64a78159
- Volumétrie : 140 jeux de données et 323 ressources
- Nombre de vues (tous jeux de données confondus) : 15
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `csv`, `json`, `shp`, `pdf`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 18/07/2018 à 09h10
- Date de dernière mise à jour le 18/09/2019 à 09h52

## NANTES METROPOLE

*Métropole*

### Source **OpenDataSoft**
- URL : https://data.nantesmetropole.fr/explore/?refine.publisher=Nantes+Métropole
- Volumétrie : 127 jeux de données et 3040460 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1220890
- Types de licences utilisées : `Open Database License (ODbL)`, `Licence ouverte v2.0 (Etalab)`
- Date de création : le 27/01/2014 à 00h00
- Date de dernière mise à jour le 07/10/2019 à 04h36


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff98a3a7292c64a78012
- Volumétrie : 131 jeux de données et 429 ressources
- Nombre de vues (tous jeux de données confondus) : 92
- Nombre de téléchargements (toutes ressources confondues) : 5
- Types de ressources disponibles : `csv`, `json`, `pdf`, `shp`, `gtfs`, `zip`, `kml`, `kmz`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 18/07/2018 à 09h10
- Date de dernière mise à jour le 18/09/2019 à 09h55

## NEVERS

*Commune*

### Source **Plateforme territoriale**
- URL : https://www.databfc.fr/bfc/portail
- Volumétrie : 11 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/594be11888ee38743d67051b
- Volumétrie : 12 jeux de données et 15 ressources
- Nombre de vues (tous jeux de données confondus) : 23
- Nombre de téléchargements (toutes ressources confondues) : 15
- Types de ressources disponibles : `csv`, `zip`, `json`, `pdf`, `ods`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 23/06/2017 à 09h04
- Date de dernière mise à jour le 17/10/2018 à 08h54

## NICE

*Commune*

### Source **Plateforme territoriale**
- URL : http://opendata.nicecotedazur.org/data/organization/ville-de-nice
- Volumétrie : 79 jeux de données

## NIEVRE

*Département*

### Source **Plateforme territoriale**
- URL : https://www.databfc.fr/bfc/portail
- Volumétrie : 38 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59367c42c751df5751fcc925
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `csv`, `opendata.index.csv`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 06/06/2017 à 12h07
- Date de dernière mise à jour le 10/01/2018 à 16h23

## NOE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/57ea3e8b88ee3877235ff490/
- Volumétrie : 7 jeux de données et 7 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `odt`, `doc`, `geojson`, `xls`, `docx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 29/10/2016 à 11h28
- Date de dernière mise à jour le 12/04/2017 à 23h18

## NOGENT SUR MARNE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/54ca2b22c751df5273467389
- Volumétrie : 10 jeux de données et 13 ressources
- Nombre de vues (tous jeux de données confondus) : 14
- Nombre de téléchargements (toutes ressources confondues) : 9
- Types de ressources disponibles : `pdf`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 03/03/2015 à 10h09
- Date de dernière mise à jour le 19/10/2018 à 10h55

## NOUVELLE AQUITAINE

*Région*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff6aa3a7292c64a77d95
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 6
- Nombre de téléchargements (toutes ressources confondues) : 27
- Types de ressources disponibles : `xml`, `csv`
- Types de licences utilisées : `Creative Commons Attribution Share-Alike`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 07/05/2014 à 03h38
- Date de dernière mise à jour le 18/09/2013 à 08h47

## NOUVELLE AQUITAINE TOURISME

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://www.datalocale.fr/dataset?organization=sirtaqui
- Volumétrie : 87 jeux de données

## NOUVELLE-AQUITAINE MOBILITES

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : https://www.pigma.org/ckan/dataset?organization=nouvelle_aquitaine_mobilit_s
- Volumétrie : 34 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c52f0cc8b4c410915918eee/
- Volumétrie : 38 jeux de données et 71 ressources
- Nombre de vues (tous jeux de données confondus) : 14
- Nombre de téléchargements (toutes ressources confondues) : 13
- Types de ressources disponibles : `gtfs`, `netex`
- Types de licences utilisées : `Other (Attribution)`, `License Not Specified`
- Date de création : le 25/05/2019 à 02h01
- Date de dernière mise à jour le 05/10/2019 à 02h02

## NOYAL CHATILLON SUR SEICHE

*Commune*

### Source **OpenDataSoft**
- URL : https://data.rennesmetropole.fr/explore/?refine.publisher=Ville+de+Noyal-Ch%C3%A2tillon-sur-Seiche
- Volumétrie : 6 jeux de données et 124 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 2634
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 16/01/2018 à 14h33
- Date de dernière mise à jour le 28/01/2019 à 10h57

## NOYAL PONTIVY

*Commune*

### Source **OpenDataSoft**
- URL : https://www.opendata56.fr/explore/?refine.publisher=Ville+de+Noyal-Pontivy
- Volumétrie : 1 jeux de données
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 19/10/2018 à 07h46
- Date de dernière mise à jour le 17/11/2018 à 08h36


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a5cb495c751df2cdac7334e/
- Volumétrie : 1 jeux de données et 5 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `html`, `odata`, `csv`, `xml`, `json`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 15/01/2018 à 15h42
- Date de dernière mise à jour le 15/01/2018 à 15h42

## OCCITANIE

*Région*

### Source **OpenDataSoft**
- URL : https://data.laregion.fr/explore/?refine.publisher=R%C3%A9gion+Occitanie
- Volumétrie : 95 jeux de données et 375982 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 40956
- Types de licences utilisées : `Open Database License (ODbL)`, `Licence Ouverte v2.0 (Etalab)`, `Licence Ouverte (Etalab)`
- Date de création : le 19/07/2017 à 10h40
- Date de dernière mise à jour le 07/10/2019 à 00h04

## OCTEVILLE SUR MER

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.agglo-lehavre.fr
- Volumétrie : 3 jeux de données

## OFFICE DE TOURISME METROPOLITAIN NICE COTE D'AZUR

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : http://opendata.nicecotedazur.org/data/organization/office-de-tourisme-metropolitain-nice-cote-d-azur
- Volumétrie : 10 jeux de données

## OISE

*Département*

### Source **Plateforme territoriale**
- URL : http://opendata.oise.fr/donnees/
- Volumétrie : 125 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff67a3a7292c64a77d7d
- Volumétrie : 54 jeux de données et 181 ressources
- Nombre de vues (tous jeux de données confondus) : 11515
- Nombre de téléchargements (toutes ressources confondues) : 1576
- Types de ressources disponibles : `pdf`, `doc`, `xls`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `License Not Specified`
- Date de création : le 07/05/2014 à 03h38
- Date de dernière mise à jour le 30/05/2014 à 05h57

## ONET LE CHATEAU

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5aec4c3e88ee381426c8df86/
- Volumétrie : 7 jeux de données et 7 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`, `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 04/05/2018 à 14h14
- Date de dernière mise à jour le 15/11/2018 à 09h54

## ORLEANS METROPOLE

*Métropole*

### Source **OpenDataSoft**
- URL : https://data.orleans-metropole.fr/explore/?refine.publisher=Orl%C3%A9ans+M%C3%A9tropole
- Volumétrie : 44 jeux de données et 382621 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 16303
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 13/09/2018 à 09h13
- Date de dernière mise à jour le 07/10/2019 à 04h27


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/57ed2fe9c751df236179df72
- Volumétrie : 146 jeux de données et 553 ressources
- Nombre de vues (tous jeux de données confondus) : 131
- Nombre de téléchargements (toutes ressources confondues) : 41
- Types de ressources disponibles : `csv`, `json`, `shp`, `pdf`, `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`, `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence`, `Other (Public Domain)`
- Date de création : le 10/01/2019 à 18h42
- Date de dernière mise à jour le 05/10/2019 à 00h02

## ORSANCO

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5cc19b08634f415e201b269e/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `json`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 26/04/2019 à 16h38
- Date de dernière mise à jour le 07/06/2019 à 15h44

## ORVAULT

*Commune*

### Source **OpenDataSoft**
- URL : https://data.loire-atlantique.fr/explore/?refine.publisher=Orvault
- Volumétrie : 16 jeux de données et 1159 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 917
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 13/03/2018 à 00h00
- Date de dernière mise à jour le 07/10/2019 à 01h13

## OSSERAIN RIVAREYTE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5cacc0838b4c4147790eca6e/
- Volumétrie : 3 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `json`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 26/04/2019 à 16h30
- Date de dernière mise à jour le 07/06/2019 à 15h49

## OTTMARSHEIM

*Commune*

### Source **OpenDataSoft**
- URL : https://data.mulhouse-alsace.fr/explore/?refine.publisher=Comme+d%27Ottmarsheim
- Volumétrie : 1 jeux de données et 1025 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 134
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 31/01/2018 à 16h04
- Date de dernière mise à jour le 12/09/2019 à 21h48

## PARIS

*Commune*

### Source **OpenDataSoft**
- URL : https://opendata.paris.fr/explore/?sort=modified
- Volumétrie : aucun jeu de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff89a3a7292c64a77eb7
- Volumétrie : 593 jeux de données et 2254 ressources
- Nombre de vues (tous jeux de données confondus) : 12899
- Nombre de téléchargements (toutes ressources confondues) : 8927
- Types de ressources disponibles : `csv`, `json`, `shp`, `pdf`, `xlsx`, `html`, `xlb`, `zip`, `docx`, `obj`, `xls`, `doc`, `ods`, `map`, `odt`, `txt`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `License Not Specified`, `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 07/05/2014 à 03h36
- Date de dernière mise à jour le 05/10/2019 à 04h04

## PAS DE CALAIS TOURISME

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://tourisme62.opendatasoft.com/explore/?refine.publisher=Comité+départemental+de+tourisme+du+Pas-de-Calais
- Volumétrie : 40 jeux de données et 77815 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 96140
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 01/04/2015 à 11h47
- Date de dernière mise à jour le 07/10/2019 à 04h01


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fffa2a3a7292c64a7808d/
- Volumétrie : 30 jeux de données et 117 ressources
- Nombre de vues (tous jeux de données confondus) : 24
- Nombre de téléchargements (toutes ressources confondues) : 59
- Types de ressources disponibles : `csv`, `json`, `xls`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 07/05/2014 à 04h09
- Date de dernière mise à jour le 07/01/2014 à 14h54

## PAYS DE LA LOIRE

*Région*

### Source **OpenDataSoft**
- URL : https://data.paysdelaloire.fr/explore/?refine.publisher=Région+des+Pays+de+la+Loire
- Volumétrie : 56 jeux de données et 91937 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 33184
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 16/06/2016 à 00h00
- Date de dernière mise à jour le 04/10/2019 à 09h10


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fffa7a3a7292c64a780c5
- Volumétrie : 117 jeux de données et 374 ressources
- Nombre de vues (tous jeux de données confondus) : 82
- Nombre de téléchargements (toutes ressources confondues) : 65
- Types de ressources disponibles : `csv`, `json`, `shp`, `zip`, `pdf`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 16/07/2018 à 19h18
- Date de dernière mise à jour le 30/09/2019 à 22h15

## PAYS DE SAINT BRIEUC

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 25 jeux de données

## PEILLAC

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b7ea6c3634f4119e42b781b/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Other (Public Domain)`
- Date de création : le 24/08/2018 à 08h46
- Date de dernière mise à jour le 24/08/2018 à 08h47

## PEROLS

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/p%C3%A9rols
- Volumétrie : 29 jeux de données

## PESSAC

*Commune*

### Source **OpenDataSoft**
- URL : https://opendata.bordeaux-metropole.fr/explore/?disjunctive.publisher&disjunctive.frequence&disjunctive.territoire&sort=title&refine.publisher=Ville+de+Pessac
- Volumétrie : 2 jeux de données
- Types de licences utilisées : `Licence Ouverte`
- Date de création : le 04/09/2019 à 13h58
- Date de dernière mise à jour le 13/09/2019 à 13h58

## PETIT COURONNE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b90dc4d634f415369256e17/
- Volumétrie : 5 jeux de données et 5 ressources
- Nombre de vues (tous jeux de données confondus) : 11
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `jpg`, `pdf`, `docx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 06/09/2018 à 12h11
- Date de dernière mise à jour le 06/09/2018 à 15h30

## PETR PAYS MIDI QUERCY

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/594d23e688ee380a6acd18af/
- Volumétrie : 9 jeux de données et 10 ressources
- Nombre de vues (tous jeux de données confondus) : 17
- Nombre de téléchargements (toutes ressources confondues) : 4
- Types de ressources disponibles : `xls`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 23/06/2017 à 16h02
- Date de dernière mise à jour le 19/07/2017 à 16h07

## PICARDIE NATURE

*Organisme associé de collectivité territoriale*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/54e3c4f1c751df7e3e467389/
- Volumétrie : 6 jeux de données et 11 ressources
- Nombre de vues (tous jeux de données confondus) : 15
- Nombre de téléchargements (toutes ressources confondues) : 14
- Types de ressources disponibles : `csv`, `kml`, `html`
- Types de licences utilisées : `Creative Commons Attribution`, `Creative Commons Attribution Share-Alike`
- Date de création : le 21/04/2016 à 10h04
- Date de dernière mise à jour le 30/06/2016 à 17h55

## PIGNAN

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/pignan
- Volumétrie : 28 jeux de données

## PIRAE

*Commune*

### Source **Plateforme territoriale**
- URL : http://www.pirae.pf/opendata/
- Volumétrie : 3 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/56bac12ec751df6c539f9dea
- Volumétrie : 15 jeux de données et 884 ressources
- Nombre de vues (tous jeux de données confondus) : 41
- Nombre de téléchargements (toutes ressources confondues) : 34
- Types de ressources disponibles : `csv`, `pdf`, `json`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 24/10/2016 à 04h44
- Date de dernière mise à jour le 04/10/2019 à 00h37

## PLAINTEL

*Commune*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 2 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/588f0a98c751df12c6ae0a66
- Volumétrie : 2 jeux de données et 11 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `html`, `odata`, `csv`, `xml`, `json`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 31/01/2017 à 15h59
- Date de dernière mise à jour le 08/02/2017 à 11h48

## PLERIN

*Commune*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 6 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/584ed32c88ee38733fc65bb3
- Volumétrie : 6 jeux de données et 30 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `html`, `odata`, `csv`, `xml`, `json`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 20/02/2017 à 11h53
- Date de dernière mise à jour le 09/10/2018 à 17h30

## PLESSE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a1fde6d88ee384f0ff312b0
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 4
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 30/11/2017 à 13h59
- Date de dernière mise à jour le 30/11/2017 à 13h59

## PLESSIS TREVISE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bb4ae00634f4155bc71f71f/
- Volumétrie : 11 jeux de données et 11 ressources
- Nombre de vues (tous jeux de données confondus) : 11
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`, `xml`, `07.2018.09-38.xml`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`, `License Not Specified`
- Date de création : le 03/10/2018 à 14h12
- Date de dernière mise à jour le 26/07/2019 à 15h10

## PLEUMELEUC

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b1a788188ee3843648b36b6/
- Volumétrie : 6 jeux de données et 6 ressources
- Nombre de vues (tous jeux de données confondus) : 7
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 28/06/2018 à 16h58
- Date de dernière mise à jour le 11/06/2019 à 17h01

## PLOEZAL

*Commune*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 5 jeux de données

## PLOUFRAGAN

*Commune*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/donnees
- Volumétrie : 7 jeux de données

## POITIERS

*Commune*

### Source **OpenDataSoft**
- URL : https://data.grandpoitiers.fr/explore/?refine.publisher=Ville+de+Poitiers
- Volumétrie : 41 jeux de données et 124696 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 18121
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 17/08/2016 à 14h50
- Date de dernière mise à jour le 30/09/2019 à 10h02

## POLE METROPOLITAIN DE L'ESTUAIRE DE LA SEINE

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : https://data.estuairedelaseine.fr
- Volumétrie : 5 jeux de données

## POLE METROPOLITAIN DU PAYS DE BREST

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5adda58dc751df45e10ca319/
- Volumétrie : 16 jeux de données et 61 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`, `json`, `shp`, `document`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 23/04/2018 à 11h36
- Date de dernière mise à jour le 02/10/2019 à 16h05

## PONT L'ABBE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/58da32bac751df47f26f37b1
- Volumétrie : 2 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `csv`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 29/03/2017 à 16h06
- Date de dernière mise à jour le 25/04/2018 à 11h42

## PORNICHET

*Commune*

### Source **OpenDataSoft**
- URL : https://data.agglo-carene.fr/explore/?sort=modified&disjunctive.gestionnaire&disjunctive.publisher&disjunctive.theme&disjunctive.keyword&disjunctive.license&disjunctive.features&disjunctive.diffuseur&refine.dcat.spatial=CARENE&disjunctive.dcat.spatial&refine.publisher=Pornichet
- Volumétrie : 1 jeux de données et 37 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 94
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 03/10/2019 à 08h35
- Date de dernière mise à jour le 03/10/2019 à 08h35

## PRADES LE LEZ

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/prades-le-lez
- Volumétrie : 25 jeux de données

## PROVENCE TOURISME

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/provence-tourisme
- Volumétrie : 9 jeux de données

## PUGET VILLE

*Commune*

### Source **Plateforme territoriale**
- URL : https://opendata.sictiam.fr/fr/organization/mairie-de-puget-ville
- Volumétrie : 8 jeux de données

## PUY DE DOME

*Département*

### Source **Plateforme territoriale**
- URL : http://www.puy-de-dome.fr/opendata.html
- Volumétrie : 26 jeux de données

## PYRENEES ATLANTIQUES

*Département*

### Source **OpenDataSoft**
- URL : https://data.le64.fr/explore/?refine.publisher=D%C3%A9partement+des+Pyr%C3%A9n%C3%A9es-Atlantiques
- Volumétrie : aucun jeu de données et 59074 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 694

## PYRENEES ORIENTALES

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ab9f242c751df2f936031d6
- Volumétrie : 7 jeux de données et 7 ressources
- Nombre de vues (tous jeux de données confondus) : 9
- Nombre de téléchargements (toutes ressources confondues) : 7
- Types de ressources disponibles : `-signees-2eme-trim19.pdf`, `-signees-1er-trim19-a-completer-avant-31-032019-copie.ods`, `-signees-4t18.pdf`, `ods`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 27/03/2018 à 10h31
- Date de dernière mise à jour le 15/07/2019 à 12h05

## QUALITAIR CORSE

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://data-qualitaircorse.opendata.arcgis.com/datasets
- Volumétrie : 145 jeux de données

## RAMBOUILLET

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c40559d8b4c4151b275a7b0/
- Volumétrie : 6 jeux de données et 6 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 6
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 17/04/2019 à 15h24
- Date de dernière mise à jour le 26/06/2019 à 13h43

## RATP

*Délégataire de service public*

### Source **OpenDataSoft**
- URL : https://data.ratp.fr/explore/?refine.publisher=RATP
- Volumétrie : 22 jeux de données et 227419 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 109900
- Types de licences utilisées : `Licence Ouverte (Etalab)`, `Licence RATP http://data.ratp.fr/page/cgu_ratp`, `Licence ODbL Version Française`
- Date de création : le 30/01/2015 à 10h49
- Date de dernière mise à jour le 02/10/2019 à 08h39


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fffa6a3a7292c64a780b7/
- Volumétrie : 34 jeux de données et 108 ressources
- Nombre de vues (tous jeux de données confondus) : 2355
- Nombre de téléchargements (toutes ressources confondues) : 1235
- Types de ressources disponibles : `csv`, `json`, `pdf`, `html`, `zip`, `shp`, `xls`, `autre`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`, `Licence Ouverte / Open Licence`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 07/05/2014 à 03h37
- Date de dernière mise à jour le 04/10/2019 à 04h12

## REDON

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a1e73b0c751df0d9952bd2d
- Volumétrie : 5 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 6
- Nombre de téléchargements (toutes ressources confondues) : 5
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 29/11/2017 à 14h11
- Date de dernière mise à jour le 20/04/2018 à 15h50

## REGIE DES TRANSPORTS CARCASONNE AGGLO

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c17b4d7634f41734de371f7/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `License Not Specified`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 17/12/2018 à 15h45
- Date de dernière mise à jour le 01/10/2019 à 18h14

## REGIE DES TRANSPORTS CARCASSONNE AGGLO

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c17b4d7634f41734de371f7/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `License Not Specified`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 17/12/2018 à 15h45
- Date de dernière mise à jour le 01/10/2019 à 18h14

## REGIE DES TRANSPORTS DU TERRITOIRE DE BELFORT

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b11425c88ee387510d9dcfb/
- Volumétrie : 3 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `json`, `gtfs`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 01/06/2018 à 15h02
- Date de dernière mise à jour le 19/03/2019 à 17h27

## REGIE LIGNES D'AZUR

*Délégataire de service public*

### Source **Plateforme territoriale**
- URL : http://opendata.nicecotedazur.org/data/organization/regie-ligne-d-azur
- Volumétrie : 5 jeux de données

## RENNES

*Commune*

### Source **OpenDataSoft**
- URL : https://data.rennesmetropole.fr/explore/?refine.publisher=Ville+de+Rennes
- Volumétrie : 137 jeux de données et 346788 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 161399
- Types de licences utilisées : `Open Database License (ODbL)`, `Licence ODbL 1.0`
- Date de création : le 05/04/2011 à 09h22
- Date de dernière mise à jour le 07/10/2019 à 00h02


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fffb9a3a7292c64a7815d
- Volumétrie : 40 jeux de données et 82 ressources
- Nombre de vues (tous jeux de données confondus) : 173
- Nombre de téléchargements (toutes ressources confondues) : 323
- Types de ressources disponibles : `xls`, `txt`, `csv`, `api`, `html`, `rss`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Other (Attribution)`, `License Not Specified`
- Date de création : le 07/05/2014 à 03h37
- Date de dernière mise à jour le 14/11/2013 à 13h32

## RENNES METROPOLE

*Métropole*

### Source **OpenDataSoft**
- URL : https://data.rennesmetropole.fr/explore/?refine.publisher=Rennes+M%C3%A9tropole
- Volumétrie : 46 jeux de données et 528536 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 513347
- Types de licences utilisées : `Licence ODbL 1.0`, `Licence Ouverte (Etalab)`, `Open Database License (ODbL)`, `Licence Ouverte 2.0`, `Domaine public`, `Licence ODbL 1.0
http://www.data.rennes-metropole.fr/notre-demarche/licence-odbl-ville-de-rennes-et-rennes-metropole/`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 01/05/2005 à 10h00
- Date de dernière mise à jour le 07/10/2019 à 04h24


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fffaaa3a7292c64a780dd
- Volumétrie : 66 jeux de données et 142 ressources
- Nombre de vues (tous jeux de données confondus) : 80
- Nombre de téléchargements (toutes ressources confondues) : 227
- Types de ressources disponibles : `json`, `shp`, `csv`, `xls`, `txt`, `xml`, `rtf`, `api`, `html`, `rss`, `gtfs`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence version 2.0`, `Other (Attribution)`, `License Not Specified`
- Date de création : le 07/05/2014 à 03h37
- Date de dernière mise à jour le 26/04/2019 à 06h39

## RESTINCLIERES

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/restincli%C3%A8res
- Volumétrie : 14 jeux de données

## RIEDISHEIM

*Commune*

### Source **OpenDataSoft**
- URL : https://data.mulhouse-alsace.fr/explore/?refine.publisher=Ville+de+Riedisheim
- Volumétrie : 3 jeux de données et 9938 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 512
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 17/11/2017 à 08h26
- Date de dernière mise à jour le 12/09/2019 à 21h48

## RILLIEUX LA PAPE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5d1b06b46f44410e40f553ac/
- Volumétrie : 2 jeux de données et 7 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `ods`, `csv`, `shp`, `shx`, `prj`, `dbf`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 02/07/2019 à 03h49
- Date de dernière mise à jour le 02/07/2019 à 04h14

## RIXHEIM

*Commune*

### Source **OpenDataSoft**
- URL : https://data.mulhouse-alsace.fr/explore/?refine.publisher=Ville+de+Rixheim
- Volumétrie : 5 jeux de données et 128 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 917
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 19/01/2018 à 10h27
- Date de dernière mise à jour le 12/09/2019 à 21h48

## RODEZ

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/57ff946788ee384fa45ff490
- Volumétrie : 7 jeux de données et 40 ressources
- Nombre de vues (tous jeux de données confondus) : 17
- Nombre de téléchargements (toutes ressources confondues) : 40
- Types de ressources disponibles : `_a_23k.csv`, `json`, `xls`, `csv`, `pdf`, `geojson`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 13/10/2016 à 16h38
- Date de dernière mise à jour le 11/06/2018 à 15h37

## ROQUEBRUNE CAP MARTIN

*Commune*

### Source **Plateforme territoriale**
- URL : https://opendata.sictiam.fr/fr/organization/mairie-de-roquebrune-cap-martin
- Volumétrie : 3 jeux de données

## ROSCOFF

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ab12b6ec751df128414f914/
- Volumétrie : 3 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 5
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `xlsx`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 21/03/2018 à 14h44
- Date de dernière mise à jour le 21/09/2018 à 19h28

## ROSNY SOUS BOIS

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b28f5b288ee38047e62143e/
- Volumétrie : 6 jeux de données et 45 ressources
- Nombre de vues (tous jeux de données confondus) : 10
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `pdf`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 19/07/2018 à 15h16
- Date de dernière mise à jour le 03/10/2019 à 16h08

## ROUBAIX

*Commune*

### Source **OpenDataSoft**
- URL : https://opendata.roubaix.fr/explore/?refine.publisher=Ville+de+Roubaix
- Volumétrie : 90 jeux de données et 217998 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 28348
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 26/07/2016 à 12h56
- Date de dernière mise à jour le 06/10/2019 à 07h01


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5be53f55634f4119ca26ec72/
- Volumétrie : 114 jeux de données et 424 ressources
- Nombre de vues (tous jeux de données confondus) : 249
- Nombre de téléchargements (toutes ressources confondues) : 18
- Types de ressources disponibles : `csv`, `json`, `shp`, `png`, `zip`, `doc`, `pdf`, `xls`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 13/11/2018 à 12h32
- Date de dernière mise à jour le 27/09/2019 à 09h02

## SAEMES

*Délégataire de service public*

### Source **OpenDataSoft**
- URL : https://opendata.saemes.fr/explore/?refine.publisher=Saemes
- Volumétrie : 13 jeux de données et 951 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 3380597
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 20/02/2017 à 15h17
- Date de dernière mise à jour le 07/10/2019 à 04h30


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/56f54580c751df65ebc485cb/
- Volumétrie : 14 jeux de données et 52 ressources
- Nombre de vues (tous jeux de données confondus) : 13
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `csv`, `json`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`
- Date de création : le 14/03/2018 à 07h05
- Date de dernière mise à jour le 30/09/2019 à 14h08

## SAINT ANDRE DE CUBZAC

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff66a3a7292c64a77d79
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 6
- Types de ressources disponibles : `xls`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 07/05/2014 à 04h22
- Date de dernière mise à jour le 18/09/2013 à 08h47

## SAINT ANDRE DES EAUX

*Commune*

### Source **OpenDataSoft**
- URL : https://data.agglo-carene.fr/explore/?refine.publisher=Saint-André+des+Eaux
- Volumétrie : 2 jeux de données et 130 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 163
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 17/05/2019 à 07h44
- Date de dernière mise à jour le 06/10/2019 à 09h30

## SAINT APOLLINAIRE

*Commune*

### Source **Plateforme territoriale**
- URL : https://www.databfc.fr/bfc/portail
- Volumétrie : 1 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5aec1fe488ee384cbcf3befe/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 04/05/2018 à 11h11
- Date de dernière mise à jour le 04/05/2018 à 11h22

## SAINT AVE

*Commune*

### Source **OpenDataSoft**
- URL : https://www.opendata56.fr/explore/?refine.publisher=Ville+de+Saint-Av%C3%A9
- Volumétrie : 8 jeux de données et 2654 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 65
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 19/10/2018 à 07h49
- Date de dernière mise à jour le 17/12/2018 à 15h14


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5aa79c9fc751df144f78f40c/
- Volumétrie : 6 jeux de données et 17 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`, `xlsx`, `kml`, `html`, `odata`, `xml`, `json`, `geojson`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 28/03/2018 à 17h23
- Date de dernière mise à jour le 11/09/2019 à 07h56

## SAINT BONNET LE BOURG

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c59b6498b4c4157a35c3880/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 05/02/2019 à 17h25
- Date de dernière mise à jour le 05/02/2019 à 17h25

## SAINT BRES

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/saint-br%C3%A8s
- Volumétrie : 17 jeux de données

## SAINT BRIEUC

*Commune*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 9 jeux de données

## SAINT CLAUDE

*Commune*

### Source **Plateforme territoriale**
- URL : https://www.databfc.fr/bfc/portail
- Volumétrie : 7 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b06810bc751df6e997fd802/
- Volumétrie : 14 jeux de données et 8 ressources
- Nombre de vues (tous jeux de données confondus) : 7
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`, `xls`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 10/08/2018 à 09h51
- Date de dernière mise à jour le 09/04/2019 à 08h44

## SAINT CYR SUR MER 

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c4973cf8b4c410839f61188/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 06/02/2019 à 18h06
- Date de dernière mise à jour le 06/02/2019 à 18h07

## SAINT DENIS DE LA REUNION

*Commune*

### Source **Plateforme territoriale**
- URL : http://opendata-sig.saintdenis.re/datasets
- Volumétrie : 40 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c050cfb8b4c413be36cb1c3/
- Volumétrie : 54 jeux de données et 324 ressources
- Nombre de vues (tous jeux de données confondus) : 31
- Nombre de téléchargements (toutes ressources confondues) : 5
- Types de ressources disponibles : `web page`, `esri rest`, `geojson`, `kml`, `zip`, `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 08/01/2019 à 13h14
- Date de dernière mise à jour le 03/10/2019 à 02h28

## SAINT DREZERY

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/saint-dr%C3%A9z%C3%A9ry
- Volumétrie : 22 jeux de données

## SAINT EGREVE

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.metropolegrenoble.fr/ckan/dataset?organization=ville-de-saint-egreve
- Volumétrie : 5 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c002da8634f4111d7393309/
- Volumétrie : 5 jeux de données et 21 ressources
- Nombre de vues (tous jeux de données confondus) : 6
- Nombre de téléchargements (toutes ressources confondues) : 8
- Types de ressources disponibles : `shp`, `kml`, `geojson`
- Types de licences utilisées : `License Not Specified`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 26/05/2018 à 09h09
- Date de dernière mise à jour le 05/10/2019 à 02h01

## SAINT ETIENNE METROPOLE

*Métropole*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c34c63a8b4c41718c8b236f/
- Volumétrie : 2 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 7
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `gbfs`, `gtfs`
- Types de licences utilisées : `License Not Specified`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 30/08/2019 à 05h52
- Date de dernière mise à jour le 30/08/2019 à 06h28

## SAINT GENIES DES MOURGUES

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/saint-geni%C3%A8s-des-mourgues
- Volumétrie : 19 jeux de données

## SAINT GEORGES D'ORQUES

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/saint-georges-dorques
- Volumétrie : 22 jeux de données

## SAINT GEORGES DE DIDONNE 

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c8a2ec7634f411c56990637/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 14/03/2019 à 12h13
- Date de dernière mise à jour le 14/03/2019 à 12h13

## SAINT GERMAIN EN LAYE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5d123361634f4145bb4131c8/
- Volumétrie : 5 jeux de données et 5 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 26/06/2019 à 11h14
- Date de dernière mise à jour le 26/06/2019 à 11h41

## SAINT GONLAY

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b3494ef88ee382f8e126555/
- Volumétrie : 4 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 28/06/2018 à 10h41
- Date de dernière mise à jour le 28/06/2018 à 11h17

## SAINT JACQUES DE LA LANDE

*Commune*

### Source **OpenDataSoft**
- URL : https://data.rennesmetropole.fr/explore/?refine.publisher=Ville+de+Saint-Jacques+de+la+Lande
- Volumétrie : 11 jeux de données et 2003 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 6812
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 01/02/2018 à 15h23
- Date de dernière mise à jour le 17/05/2019 à 13h51

## SAINT JEAN DE VEDAS

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/saint-jean-de-vedas
- Volumétrie : 24 jeux de données

## SAINT JOACHIM

*Commune*

### Source **OpenDataSoft**
- URL : https://data.agglo-carene.fr/explore/?refine.publisher=Saint-Joachim
- Volumétrie : 2 jeux de données et 240 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 200
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`, `Licence Ouverte (Etalab)`
- Date de création : le 23/01/2019 à 10h20
- Date de dernière mise à jour le 06/10/2019 à 13h00

## SAINT JUST LUZAC

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c8a2e8e634f4118ee8aeee2/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 14/03/2019 à 12h11
- Date de dernière mise à jour le 14/03/2019 à 12h14

## SAINT LAURENT DE MURE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ae1cc2588ee385be7b19c67/
- Volumétrie : 2 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `xlsx`, `xls`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 26/04/2018 à 16h59
- Date de dernière mise à jour le 17/06/2019 à 11h26

## SAINT LOUBES

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c6686958b4c4112712e3f6c/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `-a-23k.csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 03/04/2019 à 11h29
- Date de dernière mise à jour le 03/04/2019 à 11h30

## SAINT MALO

*Commune*

### Source **OpenDataSoft**
- URL : https://data.stmalo-agglomeration.fr/explore/?refine.publisher=Ville+de+Saint-Malo
- Volumétrie : 51 jeux de données et 12780 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 33143
- Types de licences utilisées : `Licence Ouverte (Etalab)`, `Open Database License (ODbL)`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 17/10/2014 à 10h10
- Date de dernière mise à jour le 31/07/2019 à 14h30


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/53ba4599a3a729219b7beaca
- Volumétrie : 50 jeux de données et 131 ressources
- Nombre de vues (tous jeux de données confondus) : 7102
- Nombre de téléchargements (toutes ressources confondues) : 894
- Types de ressources disponibles : `csv`, `shp`, `zip`, `kml`, `shp (l93)`, `gpx`, `ods`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 08/07/2014 à 00h11
- Date de dernière mise à jour le 31/08/2016 à 10h02

## SAINT MALO DE GUERSAC

*Commune*

### Source **OpenDataSoft**
- URL : https://data.agglo-carene.fr/explore/?refine.publisher=Saint+Malo+de+Guersac
- Volumétrie : 2 jeux de données et 215 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 243
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 30/08/2018 à 14h53
- Date de dernière mise à jour le 06/10/2019 à 22h00

## SAINT MANDE

*Commune*

### Source **OpenDataSoft**
- URL : https://saintmande.opendatasoft.com/explore/?refine.publisher=Ville+de+Saint-Mand%C3%A9
- Volumétrie : 3 jeux de données et 134 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 10
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 05/10/2017 à 08h49
- Date de dernière mise à jour le 18/01/2018 à 13h01

## SAINT MARCEL

*Commune*

### Source **Plateforme territoriale**
- URL : https://www.databfc.fr/bfc/portail
- Volumétrie : 1 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bab855f634f417eb0f01da8/
- Volumétrie : 1 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xlsx`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 28/09/2018 à 11h00
- Date de dernière mise à jour le 17/09/2019 à 15h13

## SAINT MAUR DES FOSSES

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fffada3a7292c64a780f9
- Volumétrie : 1 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 9
- Types de ressources disponibles : `kml`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 07/05/2014 à 04h04
- Date de dernière mise à jour le 23/11/2013 à 10h56

## SAINT MOLF

*Commune*

### Source **OpenDataSoft**
- URL : https://capatlantique-loireatlantique.opendatasoft.com/explore/?refine.publisher=Saint+Molf
- Volumétrie : 1 jeux de données et 36 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 57
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 11/06/2019 à 13h12
- Date de dernière mise à jour le 04/10/2019 à 04h00

## SAINT NAZAIRE

*Commune*

### Source **OpenDataSoft**
- URL : https://data.agglo-carene.fr/explore/?refine.publisher=Saint-Nazaire
- Volumétrie : 10 jeux de données et 29760 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1687
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 20/08/2018 à 13h47
- Date de dernière mise à jour le 06/10/2019 à 10h30

## SAINT NICOLAS DE REDON

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a2023c388ee383e1dea3b3f
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 4
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 30/11/2017 à 16h34
- Date de dernière mise à jour le 30/11/2017 à 16h35

## SAINT PALAIS SUR MER

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c8a2e8f8b4c41248f2542f5/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 14/03/2019 à 12h12
- Date de dernière mise à jour le 14/03/2019 à 12h12

## SAINT PAUL LES DAX

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c6303468b4c414d6ae2ebcd/
- Volumétrie : 45 jeux de données et 131 ressources
- Nombre de vues (tous jeux de données confondus) : 27
- Nombre de téléchargements (toutes ressources confondues) : 7
- Types de ressources disponibles : `csv`, `xlsx`, `pdf`, `geojson`, `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 26/03/2019 à 16h17
- Date de dernière mise à jour le 01/10/2019 à 16h09

## SAINT PERREUX

*Commune*

### Source **OpenDataSoft**
- URL : https://www.opendata56.fr/explore/?refine.publisher=Ville+de+Saint-Perreux
- Volumétrie : 2 jeux de données et 267 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 8
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 19/10/2018 à 08h12
- Date de dernière mise à jour le 17/11/2018 à 07h24

## SAINT QUENTIN

*Commune*

### Source **Plateforme territoriale**
- URL : http://open-data.saint-quentin-numerique.fr/datasets
- Volumétrie : 68 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fffaea3a7292c64a780fb
- Volumétrie : 32 jeux de données et 39 ressources
- Nombre de vues (tous jeux de données confondus) : 186
- Nombre de téléchargements (toutes ressources confondues) : 238
- Types de ressources disponibles : `kml`, `kmz`, `zip`, `csv`, `shape`, `shp`, `xls`, `txt`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 07/05/2014 à 03h45
- Date de dernière mise à jour le 22/10/2015 à 18h15

## SAINT VINCENT SUR OUST

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b90de3c8b4c412516d5511f/
- Volumétrie : 3 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xls`, `csv`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 06/09/2018 à 12h51
- Date de dernière mise à jour le 13/09/2019 à 14h11

## SAINTE ADRESSE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b110a9788ee38208c6bc159/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 01/06/2018 à 11h17
- Date de dernière mise à jour le 01/06/2018 à 11h17

## SAINTE MARIE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b90e0238b4c4126f11c4e06/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 06/09/2018 à 10h33
- Date de dernière mise à jour le 06/09/2018 à 10h34

## SAONE ET LOIRE

*Département*

### Source **Plateforme territoriale**
- URL : https://www.databfc.fr/bfc/portail
- Volumétrie : 71 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff68a3a7292c64a77d8d
- Volumétrie : 81 jeux de données et 201 ressources
- Nombre de vues (tous jeux de données confondus) : 326
- Nombre de téléchargements (toutes ressources confondues) : 178
- Types de ressources disponibles : `csv`, `xlsx`, `xls`, `html`, `txt`
- Types de licences utilisées : `Licence Ouverte / Open Licence`, `Other (Attribution)`
- Date de création : le 07/05/2014 à 03h41
- Date de dernière mise à jour le 24/09/2019 à 02h27

## SARLAT LA CANEDA

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fffaea3a7292c64a780fe
- Volumétrie : 12 jeux de données et 12 ressources
- Nombre de vues (tous jeux de données confondus) : 15
- Nombre de téléchargements (toutes ressources confondues) : 22
- Types de ressources disponibles : `pdf`, `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 07/05/2014 à 04h22
- Date de dernière mise à jour le 06/01/2014 à 10h47

## SARTHE

*Département*

### Source **OpenDataSoft**
- URL : https://data.sarthe.fr/explore/?refine.publisher=Conseil+D%C3%A9partemental+de+la+Sarthe
- Volumétrie : 35 jeux de données et 11008 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 851
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`, `Licence Ouverte (Etalab)`, `Open Database License (ODbL)`
- Date de création : le 10/12/2014 à 11h35
- Date de dernière mise à jour le 07/10/2019 à 04h01

## SARTHE DEVELOPPEMENT

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://data.sarthe.fr/explore/?refine.publisher=Sarthe+D%C3%A9veloppement
- Volumétrie : 5 jeux de données et 3038 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 514
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 24/07/2015 à 11h48
- Date de dernière mise à jour le 07/10/2019 à 04h01

## SAUSSAN

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/saussan
- Volumétrie : 20 jeux de données

## SAVIGNY LE TEMPLE

*Commune*

### Source **OpenDataSoft**
- URL : https://data.savigny-le-temple.fr/explore/?refine.publisher=Ville+de+Savigny-le-Temple
- Volumétrie : 3 jeux de données et 209 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 10
- Date de création : le 04/10/2018 à 12h01
- Date de dernière mise à jour le 08/10/2018 à 13h01

## SAVOIE

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59f843bcc751df5a1b4776b6
- Volumétrie : 3 jeux de données et 4 ressources
- Nombre de vues (tous jeux de données confondus) : 5
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `csv`, `kml`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 07/11/2017 à 13h38
- Date de dernière mise à jour le 12/03/2019 à 14h00

## SCEAUX

*Commune*

### Source **OpenDataSoft**
- URL : https://opendata.hauts-de-seine.fr/explore/?sort=modified&refine.publisher=Ville+de+Sceaux
- Volumétrie : 1 jeux de données et 74 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 162
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 17/06/2019 à 08h23
- Date de dernière mise à jour le 17/06/2019 à 09h27

## SDE 04

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a2ad43988ee38214cb97308/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 03/09/2019 à 12h55
- Date de dernière mise à jour le 03/09/2019 à 12h56

## SDE 07

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/583da740c751df06e1c0bb7e/
- Volumétrie : 2 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 03/09/2019 à 12h56
- Date de dernière mise à jour le 03/09/2019 à 12h57

## SDE 18

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/56420fbac751df373faad371/
- Volumétrie : 1 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 32
- Types de ressources disponibles : `csv`, `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 09/12/2015 à 09h05
- Date de dernière mise à jour le 19/09/2018 à 16h17

## SDE 24

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/56c586aa88ee38669b585a59/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 7
- Types de ressources disponibles : `17.02.2016.data.gouv.xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 18/02/2016 à 14h01
- Date de dernière mise à jour le 18/02/2016 à 14h01

## SDEA

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a2005f888ee381180db9edc/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 30/11/2017 à 14h46
- Date de dernière mise à jour le 03/05/2018 à 18h16

## SDEC ENERGIE

*Autre groupement de collectivités territoriales*

### Source **OpenDataSoft**
- URL : https://data.fleurysurorne.fr/explore/?refine.publisher=SDEC
- Volumétrie : 1 jeux de données et 1 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 2
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 17/06/2018 à 08h52
- Date de dernière mise à jour le 30/04/2019 à 19h49

## SDEI 36

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/58c11791c751df5a3d517bbb/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 05/03/2018 à 13h58
- Date de dernière mise à jour le 05/03/2018 à 13h58

## SDESM

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/559f83e3c751df2775330fd6/
- Volumétrie : 2 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 6
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `3.xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 20/12/2016 à 10h43
- Date de dernière mise à jour le 20/12/2016 à 10h43

## SDIS 17

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://opendata.larochelle.fr/dataset/?metakey=dataset_property&metaval=134
- Volumétrie : 2 jeux de données

## SDIS 30

*Organisme associé de collectivité territoriale*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5877b3b988ee38775fa6f717/
- Volumétrie : 5 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`, `7z`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence`
- Date de création : le 19/01/2017 à 11h26
- Date de dernière mise à jour le 19/03/2019 à 14h46

## SDIS 33

*Organisme associé de collectivité territoriale*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5aa8fccac751df716fffd696/
- Volumétrie : 2 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `pdf`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 15/03/2018 à 14h30
- Date de dernière mise à jour le 15/03/2018 à 14h30

## SDIS 44

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://data.loire-atlantique.fr/explore/?refine.publisher=Service+Départemental+d%27Incendie+et+de+Secours+de+Loire-Atlantique
- Volumétrie : 4 jeux de données et 36555 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 798
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 16/04/2019 à 09h04
- Date de dernière mise à jour le 30/04/2019 à 14h34

## SDIS 49

*Organisme associé de collectivité territoriale*

### Source **Plateforme territoriale**
- URL : https://www.opendata49.fr/index.php?id=38
- Volumétrie : 2 jeux de données

## SDIS 67

*Organisme associé de collectivité territoriale*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/58738bdc88ee385e8c0bfefe/
- Volumétrie : 3 jeux de données et 9 ressources
- Nombre de vues (tous jeux de données confondus) : 8
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `document`, `csv`, `json`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 04/02/2019 à 11h36
- Date de dernière mise à jour le 11/04/2019 à 06h49

## SDIS 72

*Organisme associé de collectivité territoriale*

### Source **OpenDataSoft**
- URL : https://data.sarthe.fr/explore/?refine.publisher=SDIS72
- Volumétrie : 1 jeux de données et 75 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 13
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 01/04/2019 à 15h32
- Date de dernière mise à jour le 19/04/2019 à 07h06

## SDIS 81

*Organisme associé de collectivité territoriale*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/551959f6c751df6682057c91/
- Volumétrie : 3 jeux de données et 8 ressources
- Nombre de vues (tous jeux de données confondus) : 70
- Nombre de téléchargements (toutes ressources confondues) : 4
- Types de ressources disponibles : `json`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 08/02/2019 à 07h14
- Date de dernière mise à jour le 08/02/2019 à 07h19

## SEILH

*Commune*

### Source **OpenDataSoft**
- URL : https://data.toulouse-metropole.fr/explore/?refine.publisher=Mairie+de+Seilh
- Volumétrie : 1 jeux de données et 2 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 2815
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 14/02/2013 à 23h00
- Date de dernière mise à jour le 31/12/2017 à 22h45

## SEINE ET MARNE

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c13da1f8b4c41461f8a544c/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 17/12/2018 à 10h07
- Date de dernière mise à jour le 17/12/2018 à 10h08

## SEINE MARITIME

*Département*

### Source **Plateforme territoriale**
- URL : http://www.opendata-27-76.fr
- Volumétrie : 238 jeux de données

## SEINE SAINT DENIS

*Département*

### Source **Plateforme territoriale**
- URL : http://data.seine-saint-denis.fr/spip.php?page=data2_rubrique
- Volumétrie : 356 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5457bc26c751df7378225d2e
- Volumétrie : 151 jeux de données et 314 ressources
- Nombre de vues (tous jeux de données confondus) : 1003
- Nombre de téléchargements (toutes ressources confondues) : 348
- Types de ressources disponibles : `json`, `shp`, `csv`, `geo.csv`, `geojson`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`
- Date de création : le 25/03/2015 à 14h19
- Date de dernière mise à jour le 04/10/2019 à 06h38

## SEM DES TRANSPORTS MONTALBANAIS

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ab217b188ee387f2fb8d2c9/
- Volumétrie : 2 jeux de données et 12 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `7z`, `zip`, `txt`
- Types de licences utilisées : `License Not Specified`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 21/03/2018 à 11h44
- Date de dernière mise à jour le 28/08/2019 à 14h07

## SEMITAN

*Délégataire de service public*

### Source **OpenDataSoft**
- URL : https://data.nantesmetropole.fr/explore/?refine.publisher=Semitan
- Volumétrie : 4 jeux de données et 137 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 6168
- Types de licences utilisées : `Open Database License (ODbL)`
- Date de création : le 20/03/2018 à 14h31
- Date de dernière mise à jour le 07/10/2019 à 04h35

## SENE

*Commune*

### Source **OpenDataSoft**
- URL : https://www.opendata56.fr/explore/?disjunctive.publisher&sort=modified&refine.publisher=Ville+de+Séné
- Volumétrie : 2 jeux de données et 94 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 30
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 28/03/2019 à 09h22
- Date de dernière mise à jour le 28/03/2019 à 13h13

## SETE

*Commune*

### Source **Plateforme territoriale**
- URL : http://opendata.agglopole.fr
- Volumétrie : 11 jeux de données

## SETRAM

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5acb71b4c751df234a194bdb/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 8
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 27/08/2019 à 08h48
- Date de dernière mise à jour le 27/08/2019 à 08h55

## SEVRES

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/549971c8c751df352b04805a
- Volumétrie : 2 jeux de données
- Nombre de vues (tous jeux de données confondus) : 0
- Types de licences utilisées : `Licence Ouverte / Open Licence`

## SI TRANSPORTS URBAINS AGGLO DU CALAISIS

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bfc16c28b4c4169df72af31/
- Volumétrie : 4 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `License Not Specified`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 27/11/2018 à 11h52
- Date de dernière mise à jour le 20/09/2019 à 11h35

## SICECO

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : https://www.databfc.fr/bfc/portail
- Volumétrie : 1 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5afda2cbc751df60c8fd4696/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 03/09/2019 à 13h02
- Date de dernière mise à jour le 03/09/2019 à 13h03

## SICTIAM

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : https://opendata.sictiam.fr/fr/organization/sictiam
- Volumétrie : 22 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/58da0f55c751df04af13c109/
- Volumétrie : 24 jeux de données et 68 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`, `pdf`, `json`, `odt`, `rar`, `ods`, `xlsx`, `xls`, `zip`
- Types de licences utilisées : `License Not Specified`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 08/06/2017 à 17h33
- Date de dernière mise à jour le 04/10/2019 à 00h00

## SIEIL 37

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/58d138dd88ee387c51ab42b2/
- Volumétrie : 1 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`, `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 21/03/2017 à 15h43
- Date de dernière mise à jour le 19/02/2019 à 15h07

## SIEL 42

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5880827c88ee382b5f9b81a4/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 21/12/2018 à 10h04
- Date de dernière mise à jour le 21/12/2018 à 10h05

## SIXT SUR AFF

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5baa0e468b4c414416b6f7b2/
- Volumétrie : 23 jeux de données et 28 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xml`, `pdf`, `csv`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 25/09/2018 à 12h40
- Date de dernière mise à jour le 01/10/2019 à 11h57

## SM4CC

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5be6f4838b4c415550fc0af6/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `License Not Specified`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 05/12/2018 à 14h57
- Date de dernière mise à jour le 10/09/2019 à 12h35

## SMED

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : https://opendata.sictiam.fr/fr/organization/syndicat-mixte-d-elimination-des-dechets-menagers-du-moyen-pays-des-alpes-maritimes-smed
- Volumétrie : 1 jeux de données

## SMICA

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5963804188ee3874c56d9456/
- Volumétrie : 3 jeux de données et 8 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`, `xls`, `zip`, `json`, `shp`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 07/02/2019 à 11h51
- Date de dernière mise à jour le 30/09/2019 à 11h08

## SMICVAL

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : https://www.datalocale.fr/dataset?organization=smicval
- Volumétrie : 1 jeux de données

## SMO SUD THD

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/smo-sud-thd
- Volumétrie : 29 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59e60b8cc751df26bd5f4ceb/
- Volumétrie : 11 jeux de données et 64 ressources
- Nombre de vues (tous jeux de données confondus) : 17
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `zip`, `json`, `shp`, `image`, `document`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 08/02/2019 à 06h44
- Date de dernière mise à jour le 08/02/2019 à 06h44

## SMOYS

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b0e6478c751df08a2eb86eb/
- Volumétrie : 1 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`, `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 30/05/2018 à 12h10
- Date de dernière mise à jour le 22/08/2019 à 11h36

## SMTC

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : http://data.metropolegrenoble.fr/ckan/dataset?organization=smtc
- Volumétrie : 8 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/575fc7f0c751df0e1dac31a0/
- Volumétrie : 14 jeux de données et 27 ressources
- Nombre de vues (tous jeux de données confondus) : 33
- Nombre de téléchargements (toutes ressources confondues) : 16
- Types de ressources disponibles : `json`, `protobuf`, `geojson`, `csv`, `gtfs`
- Types de licences utilisées : `License Not Specified`, `Open Data Commons Open Database License (ODbL)`
- Date de création : le 14/06/2016 à 15h50
- Date de dernière mise à jour le 05/10/2019 à 02h03

## SOCIETE DES TRANSPORTS URBAINS DE VIERZON

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bcf27078b4c4127749e259d/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 3
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 31/10/2018 à 14h37
- Date de dernière mise à jour le 31/10/2018 à 14h38

## SOISSONS

*Commune*

### Source **OpenDataSoft**
- URL : https://data.ville-soissons.fr/explore/?disjunctive.theme&sort=explore.popularity_score&refine.publisher=Ville+de+Soissons+
- Volumétrie : 17 jeux de données
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`, `Licence Ouverte (Etalab)`
- Date de création : le 10/05/2019 à 13h06
- Date de dernière mise à jour le 17/09/2019 à 12h17

## SOLEA

*Délégataire de service public*

### Source **OpenDataSoft**
- URL : https://data.mulhouse-alsace.fr/explore/?refine.publisher=Sol%C3%A9a
- Volumétrie : 1 jeux de données et 4 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 2496
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 16/09/2019 à 10h05
- Date de dernière mise à jour le 16/09/2019 à 10h05

## SOLURIS

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/58247d5888ee380f19c65bb3/
- Volumétrie : 3 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 19/02/2018 à 14h58
- Date de dernière mise à jour le 01/04/2019 à 11h22

## SOMME

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5919a313c751df1c22411168
- Volumétrie : 1 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 17
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 29/06/2017 à 16h39
- Date de dernière mise à jour le 24/09/2019 à 10h53

## SOYAUX

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b39e30a88ee38056c569d48/
- Volumétrie : 2 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xls`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 02/07/2018 à 11h17
- Date de dernière mise à jour le 02/07/2018 à 11h26

## SUCY EN BRIE

*Commune*

### Source **Plateforme territoriale**
- URL : https://data.ville-sucy.fr/portail
- Volumétrie : 7 jeux de données

## SUD PROVENCE ALPES COTE D'AZUR

*Région*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/region-sud
- Volumétrie : 54 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fffa8a3a7292c64a780d2
- Volumétrie : 54 jeux de données et 153 ressources
- Nombre de vues (tous jeux de données confondus) : 30
- Nombre de téléchargements (toutes ressources confondues) : 12
- Types de ressources disponibles : `xlsx`, `xls`, `csv`, `zip`, `pdf`, `gtfs`, `html`, `geojson`, `shp`, `ods`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`, `Licence Ouverte / Open Licence`, `Other (Open)`
- Date de création : le 24/05/2018 à 15h54
- Date de dernière mise à jour le 05/10/2019 à 00h00

## SURESNES

*Commune*

### Source **OpenDataSoft**
- URL : https://opendata.hauts-de-seine.fr/explore/?refine.publisher=Ville+de+Suresnes
- Volumétrie : 21 jeux de données et 13914 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 4593
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 25/01/2018 à 09h04
- Date de dernière mise à jour le 13/05/2019 à 14h27

## SUSSARGUES

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/sussargues
- Volumétrie : 22 jeux de données

## SYBLE

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/syble
- Volumétrie : 12 jeux de données

## SYDESL 71

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a4e516bc751df60325980d9/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 05/01/2018 à 16h33
- Date de dernière mise à jour le 05/01/2018 à 16h34

## SYDEV

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/54b7c236c751df406d5fa5a2/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 143
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 03/09/2019 à 12h17
- Date de dernière mise à jour le 03/09/2019 à 12h18

## SYMIELEC VAR

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/symielecvar
- Volumétrie : 1 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5a2ada2f88ee382b0adbccf3/
- Volumétrie : 2 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 03/09/2019 à 12h53
- Date de dernière mise à jour le 03/09/2019 à 12h54

## SYNDICAT DES EAUX DU BASSIN DE L'ARDECHE

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5ab3c78ec751df61c594d38b/
- Volumétrie : 4 jeux de données et 10 ressources
- Nombre de vues (tous jeux de données confondus) : 4
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `7z`, `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 22/03/2018 à 16h36
- Date de dernière mise à jour le 19/09/2019 à 05h58

## SYNDICAT DES MOBILITES PAYS BASQUE-ADOUR

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b9fe153634f415dcb10db19/
- Volumétrie : 3 jeux de données et 10 ressources
- Nombre de vues (tous jeux de données confondus) : 14
- Nombre de téléchargements (toutes ressources confondues) : 7
- Types de ressources disponibles : `zip`, `gtfs.zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 26/09/2018 à 12h45
- Date de dernière mise à jour le 25/09/2019 à 09h52

## SYTRAL

*Autre groupement de collectivités territoriales*

### Source **Plateforme territoriale**
- URL : https://data.beta.grandlyon.com/fr/recherche
- Volumétrie : 17 jeux de données

## TALANT

*Commune*

### Source **Plateforme territoriale**
- URL : https://www.databfc.fr/bfc/portail
- Volumétrie : 3 jeux de données


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b3ddb3488ee3819894581db/
- Volumétrie : 3 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 8
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `pdf`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 05/07/2018 à 11h29
- Date de dernière mise à jour le 05/07/2018 à 11h41

## TALENCE

*Commune*

### Source **OpenDataSoft**
- URL : https://opendata.bordeaux-metropole.fr/explore/?disjunctive.publisher&disjunctive.frequence&disjunctive.territoire&sort=title&refine.publisher=Ville+de+Talence
- Volumétrie : 8 jeux de données
- Types de licences utilisées : `Licence Ouverte`
- Date de création : le 19/07/2019 à 09h08
- Date de dernière mise à jour le 06/10/2019 à 22h02

## TALENSAC

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b1e91b3c751df2840ab5ca5/
- Volumétrie : 6 jeux de données et 6 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 27/09/2018 à 11h31
- Date de dernière mise à jour le 18/09/2019 à 12h35

## TARN ET GARONNE NUMERIQUE 

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5cd53a6e8b4c41062df71dbc/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 10/05/2019 à 12h11
- Date de dernière mise à jour le 10/05/2019 à 12h12

## THORIGNE FOUILLARD

*Commune*

### Source **OpenDataSoft**
- URL : https://data.rennesmetropole.fr/explore/?sort=modified&refine.publisher=Ville+de+Thorigné-Fouillard
- Volumétrie : aucun jeu de données

## TISSEO

*Autre groupement de collectivités territoriales*

### Source **OpenDataSoft**
- URL : https://data.toulouse-metropole.fr/explore/?refine.publisher=Tiss%C3%A9o
- Volumétrie : 6 jeux de données et 8758 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 51489
- Types de licences utilisées : `Open Database License (ODbL)`, `ODbl`
- Date de création : le 13/02/2015 à 23h00
- Date de dernière mise à jour le 07/10/2019 à 01h01

## TOULON PROVENCE MEDITERRANEE

*Métropole*

### Source **OpenDataSoft**
- URL : https://data.metropoletpm.fr/explore/?sort=title&refine.publisher=M%C3%A9tropole+Toulon+Provence+M%C3%A9diterran%C3%A9e
- Volumétrie : 21 jeux de données et 5167 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 363
- Types de licences utilisées : `Open Database License (ODbL)`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 12/11/2018 à 08h00
- Date de dernière mise à jour le 06/10/2019 à 22h00


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5b6c128d634f41593028881d/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 9
- Nombre de téléchargements (toutes ressources confondues) : 4
- Types de ressources disponibles : `gtfs`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 09/08/2018 à 12h25
- Date de dernière mise à jour le 19/10/2018 à 15h37

## TOULOUSE

*Commune*

### Source **OpenDataSoft**
- URL : https://data.toulouse-metropole.fr/explore/?refine.publisher=Mairie+de+Toulouse
- Volumétrie : 202 jeux de données et 549722 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 306520
- Types de licences utilisées : `Open Database License (ODbL)`, `Licence Ouverte (Etalab)`, `ODbl`
- Date de création : le 14/02/2013 à 23h00
- Date de dernière mise à jour le 07/10/2019 à 04h00


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fff8aa3a7292c64a77ecd
- Volumétrie : 49 jeux de données et 49 ressources
- Nombre de vues (tous jeux de données confondus) : 2573
- Nombre de téléchargements (toutes ressources confondues) : 1168
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 07/05/2014 à 03h43
- Date de dernière mise à jour le 20/11/2013 à 14h10

## TOULOUSE METROPOLE

*Métropole*

### Source **OpenDataSoft**
- URL : https://data.toulouse-metropole.fr/explore/?refine.publisher=Toulouse+M%C3%A9tropole
- Volumétrie : 150 jeux de données et 2816433 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 246970
- Types de licences utilisées : `Open Database License (ODbL)`, `ODbl`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 03/10/2012 à 22h00
- Date de dernière mise à jour le 07/10/2019 à 04h02


### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fffb3a3a7292c64a78129
- Volumétrie : 529 jeux de données et 2393 ressources
- Nombre de vues (tous jeux de données confondus) : 2226
- Nombre de téléchargements (toutes ressources confondues) : 851
- Types de ressources disponibles : `csv`, `json`, `shp`, `ksh`, `doc`, `pdf`, `ods`, `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Licence Ouverte / Open Licence version 2.0`, `License Not Specified`
- Date de création : le 07/05/2014 à 03h39
- Date de dernière mise à jour le 05/10/2019 à 03h47

## TOURCOING

*Commune*

### Source **OpenDataSoft**
- URL : https://opendata.lillemetropole.fr/explore/?refine.publisher=Ville+de+Tourcoing
- Volumétrie : 1 jeux de données et 646 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 890
- Types de licences utilisées : `Licence Ouverte (Etalab)`
- Date de création : le 09/01/2017 à 09h37
- Date de dernière mise à jour le 05/11/2018 à 17h18

## TOURS METROPOLE VAL DE LOIRE

*Métropole*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5cb6d4f78b4c4118e18f37d1/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 8
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence version 2.0`
- Date de création : le 17/04/2019 à 09h52
- Date de dernière mise à jour le 30/09/2019 à 08h16

## TOUT'ENBUS

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bfd2c5e634f411d0ced2709/
- Volumétrie : 1 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 02/08/2019 à 10h12
- Date de dernière mise à jour le 02/08/2019 à 10h18

## TRANSDEV LE HAVRE

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5cdd6a658b4c4129c70cbc44/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 17/09/2019 à 15h59
- Date de dernière mise à jour le 17/09/2019 à 16h00

## TRANSILIEN

*Délégataire de service public*

### Source **OpenDataSoft**
- URL : https://data.sncf.com/explore/?refine.publisher=Transilien
- Volumétrie : 15 jeux de données et 59208 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 100820
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`, `Open Database License (ODbL)`
- Date de création : le 21/06/2014 à 12h29
- Date de dernière mise à jour le 04/10/2019 à 07h14

## TRANSPORTS EN COMMUN DE L'AGGLOMERATION TROYENNE

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c0932458b4c4175cacf2f77/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 01/10/2019 à 17h03
- Date de dernière mise à jour le 01/10/2019 à 17h03

## TREGUEUX

*Commune*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 1 jeux de données

## TULLINS

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c61a7448b4c410deeac953d/
- Volumétrie : 2 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 2
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 14/02/2019 à 13h37
- Date de dernière mise à jour le 14/02/2019 à 13h37

## USEDA

*Autre groupement de collectivités territoriales*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/576cfdce88ee386c6bab6512/
- Volumétrie : 1 jeux de données et 6 ressources
- Nombre de vues (tous jeux de données confondus) : 6
- Nombre de téléchargements (toutes ressources confondues) : 3
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 23/01/2017 à 08h15
- Date de dernière mise à jour le 06/08/2018 à 15h23

## VANVES

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/54884a24c751df7226a3fc16
- Volumétrie : 14 jeux de données et 14 ressources
- Nombre de vues (tous jeux de données confondus) : 38
- Nombre de téléchargements (toutes ressources confondues) : 57
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `License Not Specified`, `Licence Ouverte / Open Licence`
- Date de création : le 19/01/2015 à 15h07
- Date de dernière mise à jour le 13/10/2016 à 12h08

## VAR

*Département*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/conseil-departemental-du-var
- Volumétrie : 7 jeux de données

## VARENNES JARCY

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c2f5acc634f4124ff9f6ba2/
- Volumétrie : 1 jeux de données et 2 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `xlsx`
- Types de licences utilisées : `License Not Specified`
- Date de création : le 04/01/2019 à 14h17
- Date de dernière mise à jour le 20/06/2019 à 08h33

## VAUCLUSE

*Département*

### Source **Plateforme territoriale**
- URL : https://trouver.datasud.fr/organization/conseil-departemental-de-vaucluse
- Volumétrie : 7 jeux de données

## VECTALIA PERPIGNAN MEDITERRANEE

*Délégataire de service public*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5c18fad8634f4130048f1ea1/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 9
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 30/09/2019 à 15h28
- Date de dernière mise à jour le 30/09/2019 à 15h28

## VENDARGUES

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/vendargues
- Volumétrie : 28 jeux de données

## VIENNE

*Département*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/59bfe09988ee3809d96507cb/
- Volumétrie : 1 jeux de données et 1 ressources
- Nombre de vues (tous jeux de données confondus) : 0
- Nombre de téléchargements (toutes ressources confondues) : 1
- Types de ressources disponibles : `zip`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 25/09/2017 à 11h36
- Date de dernière mise à jour le 25/09/2017 à 11h37

## VILLE D'AVRAY

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/548b19c6c751df1e6e4120e7
- Volumétrie : 15 jeux de données et 15 ressources
- Nombre de vues (tous jeux de données confondus) : 34
- Nombre de téléchargements (toutes ressources confondues) : 73
- Types de ressources disponibles : `csv`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 17/12/2014 à 15h28
- Date de dernière mise à jour le 04/11/2015 à 13h28

## VILLEFRANCHE SUR MER 

*Commune*

### Source **Plateforme territoriale**
- URL : https://opendata.sictiam.fr/fr/dataset?_organization_limit=0&organization=mairie-de-villefranche-sur-mer
- Volumétrie : 1 jeux de données

## VILLEMOMBLE

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/534fffbaa3a7292c64a78162
- Volumétrie : 10 jeux de données et 10 ressources
- Nombre de vues (tous jeux de données confondus) : 31
- Nombre de téléchargements (toutes ressources confondues) : 112
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 07/05/2014 à 03h58
- Date de dernière mise à jour le 06/01/2014 à 10h48

## VILLENEUVE D'ASCQ

*Commune*

### Source **OpenDataSoft**
- URL : https://opendata.lillemetropole.fr/explore/?refine.publisher=Ville+de+Villeneuve+d%27Ascq
- Volumétrie : 6 jeux de données et 3873 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 4906
- Types de licences utilisées : `Licence Ouverte (Etalab)`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 21/06/2017 à 13h50
- Date de dernière mise à jour le 29/08/2019 à 07h56

## VILLENEUVE LES MAGUELONE

*Commune*

### Source **Plateforme territoriale**
- URL : http://data.montpellier3m.fr/datasets/commune/villeneuve-les-maguelone
- Volumétrie : 30 jeux de données

## VILLENEUVE LOUBET

*Commune*

### Source **Plateforme territoriale**
- URL : https://opendata.sictiam.fr/fr/organization/mairie-de-villeneuve-loubet
- Volumétrie : 2 jeux de données

## VILLIERS LE BEL

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5965d3a888ee3839fc58d95e/
- Volumétrie : 4 jeux de données et 3 ressources
- Nombre de vues (tous jeux de données confondus) : 1
- Nombre de téléchargements (toutes ressources confondues) : 8
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 14/01/2019 à 12h19
- Date de dernière mise à jour le 09/05/2019 à 14h11

## VINCENNES

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bc75fbe8b4c417d0a07c480/
- Volumétrie : 8 jeux de données et 8 ressources
- Nombre de vues (tous jeux de données confondus) : 10
- Nombre de téléchargements (toutes ressources confondues) : 0
- Types de ressources disponibles : `csv`
- Types de licences utilisées : `Open Data Commons Open Database License (ODbL)`
- Date de création : le 13/12/2018 à 14h11
- Date de dernière mise à jour le 21/12/2018 à 16h28

## VITALIS

*Délégataire de service public*

### Source **OpenDataSoft**
- URL : https://data.grandpoitiers.fr/explore/?refine.publisher=R%C3%A9gie+des+transports+Grand+Poitiers+(VITALIS)
- Volumétrie : 2 jeux de données et 1 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1900
- Types de licences utilisées : `Open Database License (ODbL)`, `Licence Ouverte (Etalab)`
- Date de création : le 19/08/2019 à 12h29
- Date de dernière mise à jour le 06/10/2019 à 22h00

## WAMBRECHIES

*Commune*

### Source **DataGouv**
- URL : https://www.data.gouv.fr/fr/organizations/5bd875ac634f41794e19695f/
- Volumétrie : 5 jeux de données et 10 ressources
- Nombre de vues (tous jeux de données confondus) : 5
- Nombre de téléchargements (toutes ressources confondues) : 2
- Types de ressources disponibles : `pdf`, `csv`, `xlsx`
- Types de licences utilisées : `Licence Ouverte / Open Licence`
- Date de création : le 31/10/2018 à 08h45
- Date de dernière mise à jour le 25/01/2019 à 10h31

## WITTELSHEIM

*Commune*

### Source **OpenDataSoft**
- URL : https://data.mulhouse-alsace.fr/explore/?refine.publisher=Ville+de+Wittelsheim
- Volumétrie : 7 jeux de données et 1647 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 1309
- Types de licences utilisées : `Licence Ouverte (Etalab)`, `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 25/10/2017 à 16h50
- Date de dernière mise à jour le 12/09/2019 à 21h48

## YFFINIAC

*Commune*

### Source **Plateforme territoriale**
- URL : http://datarmor.cotesdarmor.fr/web/guest/donnees
- Volumétrie : 8 jeux de données

## ZILLISHEIM

*Commune*

### Source **OpenDataSoft**
- URL : https://data.mulhouse-alsace.fr/explore/?refine.publisher=Commune+de+Zillisheim
- Volumétrie : 3 jeux de données et 205 enregistrements
- Nombre de téléchargements (tous jeux de données confondus) : 494
- Types de licences utilisées : `Licence Ouverte v2.0 (Etalab)`
- Date de création : le 25/10/2017 à 16h51
- Date de dernière mise à jour le 12/09/2019 à 21h48

